import sys

try:
    from skbuild import setup
except ImportError:
    print(
        "Please update pip, you need pip 10 or greater,\n"
        " or you need to install the PEP 518 requirements in pyproject.toml yourself",
        file=sys.stderr,
    )
    raise

from setuptools import find_packages

setup(
    name="pycgrt",
    version="0.0.1",
    description="Python bindings for CGRT",
    author="Chris Guiney",
    license="GPLv2",
    packages=find_packages(where="src/python-cgrt"),
    package_dir={"": "src/python-cgrt"},
    cmake_install_dir="src/python-cgrt/pycgrt",
    cmake_languages=("C", "CXX", "CUDA"),
    include_package_data=True,
    extras_require={"test": ["pytest"]},
    python_requires=">=3.6",
)
# CGRT
_Chris Guiney's Ray Tracer_

## What is this?

This is a repository for building ray tracers and other utilities to work with 3d rendering.

It is for hobby development and learning. Anything and everything is likely to be incomplete or a work in progress.


## Current Renderers

`cgrt-cpu` - The cgrt renderer using a cpu runtime
`cgrt-cuda` - The cgrt renderer using a cuda runtime
`cgrt-c` - A C implementation of rendering kernel.  Incomplete, and will likely be abandoned.
`python-cgrt` - Experimenting with exposing cuda kernels to python



## Dependencies
- Reasonably new version of cmake
- For GPU support: CUDA 11.8 - other versions may work, ymmv
- OpenGL
- MSVC 17.0 (Visual Studio 2022)
- GCC 11.3

## References
- Most implementation details are based on [Physically Based Rendering](https://pbr-book.org/)
- Many tests and some implementation details are based on [The Ray Tracer Challenge](http://raytracerchallenge.com/)
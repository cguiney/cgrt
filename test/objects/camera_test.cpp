//
// Created by chris on 1/23/21.
//
#include <gtest/gtest.h>

#include <cgrt/objects/cameras.hpp>
#include <cgrt/img/canvas.hpp>

using namespace cgrt;
using namespace cgrt::math;
using namespace cgrt::objects;


TEST(test_camera, pixel_size_for_horizontal_canvas)
{
    auto c = cgrt::objects::camera(200, 125, CGRT_PI_2, identity<4>());
    ASSERT_EQ(c.pixel_size, 0.01f);
}

TEST(test_camera, pixel_size_for_vertical_canvas)
{
    auto c = cgrt::objects::camera(125, 200, CGRT_PI_2, identity<4>());
    ASSERT_EQ(c.pixel_size, 0.01f);
}

TEST(test_camera, generate_ray_through_center_of_canvas)
{
    auto c = cgrt::objects::camera(201, 101, CGRT_PI_2, identity<4>());
    auto r = generate_ray(c, 100, 50);
    ASSERT_EQ(r.origin, math::point<3>(0));
    ASSERT_EQ(r.direction, math::vector<3>(0, 0, -1));
}

TEST(test_camera, generate_ray_through_corner_of_canvas)
{
    auto c = cgrt::objects::camera(201, 101, CGRT_PI_2, identity<4>());
    auto r = generate_ray(c, 0, 0);
    ASSERT_EQ(r.origin, math::point<3>(0));
    ASSERT_EQ(r.direction, math::vector<3>(0.66519, 0.33259, -0.66851));
}

TEST(test_camera, generate_ray_when_camera_is_transformed)
{
    auto c = cgrt::objects::camera(201, 101, CGRT_PI_2, identity<4>());
    c.transform = math::transform::rotate_y(CGRT_PI_4) * math::transform::translation(0, -2, 5);

    auto r = generate_ray(c, 100, 50);
    ASSERT_EQ(r.origin, math::point<3>(0, 2, -5));
    ASSERT_EQ(r.direction, math::vector<3>(CGRT_SQRT2 / 2, 0, -CGRT_SQRT2 / 2));
}

/*
TEST(test_camera, rendering_world_with_camera)
{
    struct test_kernel
    {
        struct frame
        {
            cgrt::objects::intersections xs;
        };

        void operator()(kernel::frame &global, frame& local, int x, int y)
        {
            auto r = cgrt::objects::generate_ray(global.state.camera, x, y);

            local.xs.reset();
            cgrt::objects::intersections(r, global.state.world, local.xs);

            auto hit = local.xs.hit();
            if (hit) {
                auto interaction = cgrt::objects::interact(global.state.world, *hit, r);
                auto contribution = shading::phong::lighting(interaction, global.state.world.light_points[0]);
                auto pixel = img::canvas::point_type{x, y};
                global.canvas[pixel] = img::color(contribution);
            }
        }
    };


    auto w = 11;
    auto h = 11;
    auto b = default_state();
    b.camera()
        .field_of_view(CGRT_PI_2)
        .from(math::point<3>(0, 0, -5))
        .to(math::point<3>(0, 0, 0))
        .up(math::vector<3>(0, 1, 0))
        .finish()
        .set_film(w, h);


    auto st = b.build();
    auto c = img::canvas(w, h);
    auto kernel = kernel::serial<test_kernel>();

    auto settings = cgrt::kernel::settings();

    auto global = kernel::frame{
        .state = st,
        .settings = settings,
        .canvas = c,
    };

    kernel(global);

    auto p = img::canvas::point_type{5, 5};
    auto have = c[p];
    auto want = img::color(0.38066f, 0.47583f, 0.2855f);

    ASSERT_EQ(have, want);
}
 */
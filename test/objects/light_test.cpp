//
// Created by chris on 1/16/21.
//

#include <gtest/gtest.h>

#include <cgrt/objects/lights.hpp>

using namespace cgrt;

TEST(test_lights, light_has_position_and_intensity)
{
    cgrt::objects::light_point p = {math::point<3>(0), math::spectrum(1)};

    ASSERT_EQ(p.position, math::point<3>(0));
    ASSERT_EQ(p.energy, math::spectrum(1));
}

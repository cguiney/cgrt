//
// Created by chris on 1/16/21.
//

#include <gtest/gtest.h>

#include <cgrt/objects/materials.hpp>

using namespace cgrt::math;
using namespace cgrt::objects;

TEST(materials_test, default_material)
{
    auto m = default_material();
//    ASSERT_EQ(m.color, spectrum(1.f));
    ASSERT_EQ(m.ambient, 0.1f);
    ASSERT_EQ(m.diffuse, 0.9f);
    ASSERT_EQ(m.specular, 0.9f);
    ASSERT_EQ(m.shininess, 200.f);

}
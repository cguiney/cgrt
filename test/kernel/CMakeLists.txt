add_executable(
    kernel_test
    intersection_test.cpp
    math_test.cpp
)

target_include_directories(
        kernel_test
        PRIVATE
        ../../src/libcgrt
)

target_link_libraries(kernel_test gtest_main)

add_test(NAME cgrt_kernel_test COMMAND $<TARGET_FILE:kernel_test>)
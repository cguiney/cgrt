//
// Created by chris on 2/14/21.
//

/*
#include "../../src/cgrt-c/algebra.h"
#include "../../src/cgrt-c/kernel.h"

#include <cgrt/math/vector.hpp>
#include <gtest/gtest.h>

using namespace cgrt;


TEST(kernel_vector_test, construct3D)
{
    struct vector v =  INIT_VECTOR(1, 2, 3);
    struct vector v2 = INIT_VECTOR(1, 2, 3);

    ASSERT_TRUE(vector_eq(&v, &v2));
}

TEST(kernel_vector_test, addition)
{
    struct vector v1 = INIT_VECTOR(3, -2, 5);
    struct vector v2 = INIT_VECTOR(-2, 3, 1);

    struct vector have = vector_add(&v1, &v2);

    struct vector want = INIT_VECTOR(1, 1, 6);

    ASSERT_TRUE(vector_eq(&have, &want));
}

TEST(kernel_vector_test, reflection_45deg)
{
    struct vector v = INIT_VECTOR(1, -1, 0);
    struct vector n = INIT_VECTOR(0, 1, 0);

    struct vector want = INIT_VECTOR(1, 1, 0);
    struct vector have = vector_reflect(&v, &n);
    ASSERT_TRUE(vector_eq(&have, &want));
}

TEST(kernel_vector_test, reflecting_off_slanted_surface)
{
    struct vector v = INIT_VECTOR(0, -1.f, 0);
    struct vector n = INIT_VECTOR(CGRT_SQRT2 / 2.f, CGRT_SQRT2 / 2.f, 0);

    struct vector want = INIT_VECTOR(1.f, 0, 0);
    struct vector have = vector_reflect(&v, &n);

    auto x = vector_eq(&want, &have);
    ASSERT_TRUE(x) << "have " << math::vector<3>(have.value.x, have.value.y, have.value.z);
}

TEST(kernel_vector_test, dot_product)
{
    struct vector x = INIT_VECTOR(1, 2, 3);
    struct vector y = INIT_VECTOR(2, 3, 4);
    float want = 20.0f;
    ASSERT_EQ(dot(&x, &y), want);
}

TEST(kernel_vector_test, normalize)
{
    struct vector x = INIT_VECTOR(1, 2, 3);

    struct vector want = INIT_VECTOR(0.26726, 0.53452, 0.80178);
    struct vector have = vector_normalize(&x);

    ASSERT_TRUE(vector_eq(&have, &want));
}



TEST(kernel_point_test, construction3D) {
    struct point p = INIT_POINT(1, 2, 3);
    struct point want = INIT_POINT(1, 2, 3);
    ASSERT_TRUE(point_eq(&p, &want));
}

TEST(kernel_point_test, addition)
{
    struct point p1 = INIT_POINT(3, -2, 5);
    struct vector v1 = INIT_VECTOR(-2, 3, 1);

    struct point want = INIT_POINT(1.f,1.f, 6.f);
    struct point have = point_add(&p1, &v1);

    ASSERT_TRUE(point_eq(&have, &want));
}

TEST(kernel_point_test, point_point_subtraction)
{
    struct point p1 = INIT_POINT(3, 2, 1);
    struct point p2 = INIT_POINT(5, 6, 7);

    struct vector want = INIT_VECTOR(-2, -4, -6);
    struct vector have = point_sub(&p1, &p2);

    ASSERT_TRUE(vector_eq(&want, &have));
}

TEST(kernel_point_test, point_vector_subtraction)
{
    struct point p1 = INIT_POINT(3, 2, 1);
    struct point p2 = INIT_POINT(5, 6, 7);

    struct vector want = INIT_VECTOR(-2, -4, -6);
    struct vector have = point_sub(&p1, &p2);

    ASSERT_TRUE(vector_eq(&want, &have));
}
*/
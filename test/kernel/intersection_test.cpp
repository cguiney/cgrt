//
// Created by chris on 2/13/21.
//

#include <../cgrt-cpu/cpu.hpp>
#include <cgrt/kernel/estimators.hpp>
#include <cgrt/kernel/interactions.hpp>
#include <cgrt/kernel/intersections.hpp>
#include <cgrt/kernel/surfaces.hpp>
#include <cgrt/math/point.hpp>
#include <cgrt/math/ray.hpp>
#include <cgrt/math/vector.hpp>
#include <cgrt/objects/objects.hpp>
#include <cgrt/state.hpp>
#include <gtest/gtest.h>

using namespace cgrt;

using ray = math::ray<3>;
using point = math::point<3>;
using vector = math::vector<3>;
using object = objects::object;

struct intersection_test {
    std::string name;
    ray input;
    std::optional<math::scalar> output;
};

TEST(test_diffuse, disk_against_sphere_area_light) {
    auto state = spheres_example(200, 200).build();
    auto ds = dense_state<cgrt::runtime::cpu>(state);
    auto st = dense_state_view(ds);
    auto r = math::ray<3>(
        math::point<3>(-0.000000343969589f, 2.99999928f, -4.99999952),
        math::vector<3>(-0.365185857f, -0.657598615f, 0.65894109)
    );
    auto xs = kernel::intersections::find_closest(r, st.objects(), st.transforms());
    ASSERT_TRUE(xs);
    ASSERT_NE(xs->time, 0.f);
    ASSERT_EQ(xs->obj.type, objects::DISK);
    ASSERT_NEAR(xs->time.value, 4.56205f, math::EPSILON());
    // auto xs = kernel::intersections::intersection{1u, st.objects()[1], math::scalar{0.638602912f}};
    auto ia = kernel::interactions::interact(st, *xs, r);

    auto xi =  math::continuous_random_variable<4>::join<2, 2>(
        math::continuous_random_variable<2>::join<1, 1>(
            math::continuous_random_variable<1>{0.72748804f},
            math::continuous_random_variable<1>{0.493847817f}
        ),
        math::continuous_random_variable<2>::join<1, 1>(
            math::continuous_random_variable<1>{0.104709394f},
            math::continuous_random_variable<1>{0.329331279f}
        )
    );

    auto estimated = kernel::illumination::estimate_direct(
        ia,
        st.lights()[0],
        st.light_points(),
        st.light_objects(),
        st.objects(),
        st.transforms(),
        xi
    );

    ASSERT_NE(estimated, math::spectrum{});

}

TEST(test_surface, disk_partial_derivatives) {
    auto p = point{1.66599751f, 0.000000087155044f, -1.99387503f};
    auto t = math::transform::scale(100.f, 100.f, 100.f) *
             math::transform::rotate_x(90.f);
    auto surface = kernel::surfaces::surface_query_disk(inverse(t)(p));

    auto want = vector{12.5278864f, 0.000000457560816f, -10.4677715f};
    auto have = t(surface.geometry.du);
    ASSERT_EQ(have, want);
}

TEST(test_intersections, spheres) {
    auto tests = std::vector<intersection_test> {
        {"sphere intersection", ray{point{0.f, 0.f, -5.f}, vector{0.f, 0.f, 1.f}}, 4.0f},
        {"sphere at tangent", ray{point{0.f, 1.f, -5.f}, vector{0.f, 0.f, 1.f}}, 5.0f},
        {"ray misses spehre", ray{point{0.f, 2.f, -5.f}, vector{0.f, 0.f, 1.f}}, {}},
        {"ray inside sphere", ray{point{0.f, 0.f, 0.f}, vector{0.f, 0.f, 1.f}}, 1.f}, // -1.f, 1.f
        {"sphere behind ray", ray{point{0.f, 0.f, 5.f}, vector{0.f, 0.f, 1.f}}, {}} // -4.f, -6.f
    };
    for(auto&& t : tests) {
        ASSERT_EQ(kernel::intersections::intersection_test<objects::SPHERE>{}(t.input), t.output);
    }
}

TEST(test_intersections, planes) {
    auto tests = std::vector<intersection_test>{
        {"intersect with ray parallel to plane", ray{point{0.f, 0.f, 10.f}, vector{0.f, 1.f, 0.f}}, {}},
        {"intersect with ray coplanar to plane", ray{point{0.f, 0.f, 0.f}, vector{0.f, 1.f, 0.f}}, {}},
        {"intersect with plane from above", ray{point{0.f, 0.f, 1.f}, vector{0.f, 0.f, -1.f}}, 1.f},
        {"intersect with plane from below", ray{point{0.f, 0.f, -1.f}, vector{0.f, 0.f, 1.f}}, 1.f}
    };
    for(auto&& t : tests) {
        auto x = kernel::intersections::intersection_test<objects::PLANE>{}(t.input);
        ASSERT_EQ(x, t.output) << t.name << " failed" << std::endl;
    }
}

/*
TEST(test_kernel_intersections, hit_when_all_intersections_have_positive_time) {
    struct object obj[2] = {
            {.object_id = 1},
            {.object_id = 2}
    };

    struct intersections_t xs = INIT_INTERSECTIONS;
    intersection_push(&xs, &obj[0], 1.f);
    intersection_push(&xs, &obj[1], 2.f);

    intersection_t x = {};
    auto hit = intersection_hit(&xs, &x);

    ASSERT_TRUE(hit);
    ASSERT_EQ(x.obj.object_id, 1);
    ASSERT_EQ(x.time, 1.f);
}

TEST(test_kernel_intersections, hit_when_some_have_negative_time) {
    struct object obj[2] = {
            {.object_id = 1},
            {.object_id = 2}
    };

    struct intersections_t xs = INIT_INTERSECTIONS;
    intersection_push(&xs, &obj[0], -1.f);
    intersection_push(&xs, &obj[1], 1.f);

    struct intersection_t x = {};
    auto hit = intersection_hit(&xs, &x);

    ASSERT_TRUE(hit);
    ASSERT_EQ(x.obj.object_id, 2);
    ASSERT_EQ(x.time, 1.f);
}

TEST(test_kernel_intersections, hit_when_all_have_negative_time) {
    struct object obj[2] = {
            {.object_id = 1},
            {.object_id = 2}
    };

    struct intersections_t xs = INIT_INTERSECTIONS;
    intersection_push(&xs, &obj[0], -2.f);
    intersection_push(&xs, &obj[1], -1.f);

    struct intersection_t x = {};
    auto hit = intersection_hit(&xs, &x);
    ASSERT_FALSE(hit);
}

TEST(test_kernel_intersections, hit_is_lowest_nonnegative_intersection) {
    struct object obj[4] = {
            {.object_id = 1},
            {.object_id = 2},
            {.object_id = 3},
            {.object_id = 4}
    };

    struct intersections_t xs = INIT_INTERSECTIONS;
    intersection_push(&xs, &obj[0], 5.f);
    intersection_push(&xs, &obj[1], 7.f);
    intersection_push(&xs, &obj[2], -3.f);
    intersection_push(&xs, &obj[3], 2.f);

    struct intersection_t x = {};
    auto hit = intersection_hit(&xs, &x);

    ASSERT_TRUE(hit);
    ASSERT_EQ(x.obj.object_id, 4);
    ASSERT_EQ(x.time, 2.f);
}

TEST(test_kernel_intersections, scaled_sphere_with_ray) {
    auto t = math::transform::scale(2, 2, 2);
    cgrt::builder b;

    auto s = b.objects().create(cgrt::objects::SPHERE)
            .use_transform(t)
            .finish()
            .finish()
            .build();

    auto ds = dense_state(s);

    struct ray r{};
    r.origin = INIT_POINT(0, 0, -5);
    r.dir = INIT_VECTOR(0, 0, 1);

    struct intersections_t xs = INIT_INTERSECTIONS;

    intersection_query_objects(&r, ds.get(), &xs);

    ASSERT_EQ(xs.count, 2);
    ASSERT_EQ(xs.times[0], 3.f);
    ASSERT_EQ(xs.times[1], 7.f);
}

TEST(test_kernel_intersections, translated_sphere_with_ray) {
    struct ray r{};
    r.origin = INIT_POINT(0, 0, -5);
    r.dir = INIT_VECTOR(0, 0, 1);

    auto t = math::transform::translation(5, 0, 0);

    cgrt::builder b;
    auto s = b.objects().create(cgrt::objects::SPHERE)
            .use_transform(t)
            .finish()
            .finish()
            .build();

    auto ds = dense_state(s);

    struct intersections_t xs = INIT_INTERSECTIONS;
    intersection_query_objects(&r, ds.get(), &xs);

    ASSERT_EQ(xs.count, 0);
}

TEST(test_kernel_intersections, interaction_should_offset_points) {
    struct ray r{};
    r.origin = INIT_POINT(0, 0, -5);
    r.dir = INIT_VECTOR(0, 0, 1);

    auto t = math::transform::translation(0, 0, 1);

    cgrt::builder b;
    auto s = b.objects().create(cgrt::objects::SPHERE)
            .use_transform(t)
            .finish()
            .finish()
            .build();

    auto ds = dense_state(s);

    struct intersection_t x = {};
    x.time = 5.f;

    struct interaction_t ia = {};

    interact(ds.get(), &x, &r, &ia);

    ASSERT_LT(ia.above.value.z, -math::epsilon / 2.f);
    ASSERT_GT(ia.surface.position.value.z, ia.above.value.z);
}

//todo: move to surface test
TEST(test_kernel_intersection, test_sphere_derivatives) {
    struct ray r{};
    r.origin = INIT_POINT(0, 0, -5);
    r.dir = INIT_VECTOR(0, 0, 1);

    auto t = (transform)(math::transform::matrix());

    struct object obj = { .type = sphere, .object_id = 1};
    struct intersections_t xs = INIT_INTERSECTIONS;

    auto intersects = intersection_query_sphere(&r, &obj, &xs);
    ASSERT_TRUE(intersects);

    struct intersection_t hit{};
    intersection_hit(&xs, &hit);

    ASSERT_EQ(hit.obj.object_id, 1);
    ASSERT_EQ(hit.time, 4);

    struct point pos = ray_position(&r, hit.time);

    struct surface_t surf = {};

    surface_query(&obj, &t, &pos, &surf);


    std::cerr << "geometric normal" << math::vector<3>(surf.geometry.normal) << std::endl;
    std::cerr << "shading normal" << math::vector<3>(surf.shading.normal) << std::endl;
    std::cerr << "dpdu" << math::vector<3>(surf.geometry.du) << std::endl;
    std::cerr << "dpdv" << math::vector<3>(surf.geometry.dv) << std::endl;

    ASSERT_TRUE(scalar_eq(surf.geometry.du.value.x, 0.f));
    ASSERT_TRUE(scalar_eq(surf.geometry.du.value.y, 6.28319e-05));
    ASSERT_TRUE(scalar_eq(surf.geometry.du.value.z, 0.f));

    ASSERT_TRUE(scalar_eq(surf.geometry.dv.value.x, 3.14159274f));
    ASSERT_TRUE(scalar_eq(surf.geometry.dv.value.y, 0.f));
    ASSERT_TRUE(scalar_eq(surf.geometry.dv.value.z, -2.74646766e-07f));
}
*/

TEST(test_intersections, disk_intersection) {
    {
        auto r = ray{
            point{0.f, 0.f, -5.f},
            vector{0.25f, 0.25f, 1.5f},
        };

        auto t = math::transform::scale(2, 2, 2);

        objects::object obj = {.type = objects::DISK, .id = objects::object_id{1}};

        auto time = kernel::intersections::intersects(r, obj, t);
        ASSERT_TRUE(time);
        ASSERT_EQ(*time, 3.33333f);

        auto pos = math::position(r, *time);
        auto surf = kernel::surfaces::surface_query(obj, t, pos);

        std::cerr << "geometric normal" << surf.geometry.normal << std::endl;
        std::cerr << "shading normal" << surf.shading.normal << std::endl;
        std::cerr << "dpdu" << surf.geometry.du << std::endl;
        std::cerr << "dpdv" << surf.geometry.dv << std::endl;

        ASSERT_EQ(surf.geometry.du.x(), -5.23598766f);
        ASSERT_EQ(surf.geometry.du.y(), 5.23598766f);
        ASSERT_EQ(surf.geometry.du.z(), 0.f);

        ASSERT_EQ(surf.geometry.dv.x(), -1.414211354f);
        ASSERT_EQ(surf.geometry.dv.y(), -1.414211354f);
        ASSERT_EQ(surf.geometry.dv.z(), 0.f);
    }

    {
        auto r = ray{
            point{0.f, 0.f, -5.f},
            vector{0.25f, 0.25f, 1.5f}
        };

        auto t = math::transform::translation(-50.f, 0.f, 50.f)
                 * math::transform::scale(1000.f, 1000.f, 1000.f)
                 * math::transform::rotate_x(math::radians(90.f));

        auto obj = objects::object{.type = objects::DISK, .id = objects::object_id{1}};
        auto time = kernel::intersections::intersects(r, obj, t);
        ASSERT_TRUE(time);
        ASSERT_GT(*time, 0.f);

        auto pos = math::position(r, *time);
        auto surf = kernel::surfaces::surface_query(obj, t, pos);

        std::cerr << "geometric normal" << surf.geometry.normal << std::endl;
        std::cerr << "shading normal" << surf.shading.normal << std::endl;
        std::cerr << "dpdu" << surf.geometry.du << std::endl;
        std::cerr << "dpdv" << surf.geometry.dv << std::endl;

        ASSERT_EQ(surf.geometry.du.x(), 345.575104f);
        ASSERT_EQ(surf.geometry.du.y(), -1.37323377e-05f);
        ASSERT_EQ(surf.geometry.du.z(), 314.159302f);

        ASSERT_EQ(surf.geometry.dv.x(), math::scalar{-672.6737f});
        ASSERT_EQ(surf.geometry.dv.y(), -3.23438035e-05f);
        ASSERT_EQ(surf.geometry.dv.z(), 739.939941f);
    }
}

/*
TEST(kernel_test_intersection, point_transform) {
    struct point p = INIT_POINT(5, 4, -5);
    transform t = (transform)(math::transform::scale(0.5, 0.5, 0.5));

    struct transform_view view = INIT_TRANSFORM(&t);

    view = transform_inverse(&view);

    struct point ip = transform_view_apply_point(&view, &p);
    view = transform_inverse(&view);

    struct point tp = transform_view_apply_point(&view, &ip);

    ASSERT_EQ(p.value.x, tp.value.x);
    ASSERT_EQ(p.value.y, tp.value.y);
    ASSERT_EQ(p.value.z, tp.value.z);

    std::cerr << "inverted point" << math::point<3>(ip) << std::endl;
    std::cerr << "inverted inverted point" << math::point<3>(tp) << std::endl;
}
*/
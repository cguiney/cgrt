//
// Created by chris on 9/19/20.
//

#include <gtest/gtest.h>

#include <cgrt/math/constants.h>
#include <cgrt/math/vector.hpp>
#include <cgrt/math/point.hpp>
#include <cgrt/math/scalar.hpp>

using cgrt::math::scalar;

using namespace cgrt;

TEST(vector_test, construct3D)
{
    math::vector<3> v =  math::vector<3>(1, 2, 3);
    math::vector<3> v2 = math::vector<3>(1, 2, 3);

    ASSERT_TRUE(v == v2);

    ASSERT_EQ(v, math::vector<3>(1, 2, 3));
    ASSERT_NE(v, math::vector<3>(1, 2, 4));

    ASSERT_EQ(v, math::vector<3>(1, 2, 3));
    ASSERT_NE(v, math::vector<3>(1, 2, 4));
}

TEST(vector_test, assign_2d_to_3d)
{
    auto v2 = math::vector<2>(1, 2);
    auto v3 = math::vector<3>::make(v2, 4);

    ASSERT_EQ(v2[0], v3[0]);
    ASSERT_EQ(v2[1], v3[1]);
    ASSERT_EQ(v3[2], scalar{4.0});
}

TEST(vector_test, addition)
{
    auto v1 = math::vector<3>(3, -2, 5);
    auto v2 = math::vector<3>(-2, 3, 1);

    ASSERT_EQ(v1 + v2, math::vector<3>(1, 1, 6));
}

TEST(vector_test, reflection_45deg)
{
    auto v = math::vector<3>(1, -1, 0);
    auto n = math::vector<3>(0, 1, 0);
    auto have = reflect(v, n);
    ASSERT_EQ(have, math::vector<3>(1, 1, 0));
}

/*
TEST(vector_test, reflecting_off_slanted_surface)
{
    auto v = math::vector<3>(0.f, -1.f, 0.f);
    auto n = math::vector<3>(math::SQRT2/2.f, math::SQRT2/2.f, 0.f);
    auto have = reflect(v, n);
    ASSERT_EQ(have, math::vector<3>(1.f, 0.f, 0.f));
}
*/
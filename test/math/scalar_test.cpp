//
// Created by chris on 9/20/20.
//

#include <gtest/gtest.h>

#include <cgrt/math/scalar.hpp>
#include <cgrt/math/random.hpp>

using namespace cgrt;
using cgrt::math::scalar;

TEST(test_scalar, equals_epsilon)
{
    scalar s = scalar{0.000001f};

    ASSERT_EQ(s, 0.f);
    ASSERT_NE(s, 1.f);
}

TEST(test_scalar, test_cmp_with_zero) {
    ASSERT_GE(math::epsilon, 0.f);
    ASSERT_FALSE(0.f > math::epsilon);

    auto s = math::scalar{0.f};
    ASSERT_FALSE(s > math::epsilon);
}

/*
TEST(test_scalar, operators_with_random_var) {
	auto s = scalar{0.1f};
	auto crv = math::continuous_random_variable<1>{0.9};

	static_assert(std::is_same_v<decltype(s+crv), scalar>);
	static_assert(!std::is_same_v<decltype(s+crv), math::continuous_random_variable<1>>);

	ASSERT_EQ(s + crv, scalar{1});
}*/
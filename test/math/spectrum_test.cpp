//
// Created by chris on 11/30/2022.
//

#include <gtest/gtest.h>
#include <cgrt/math/radiosity.hpp>

using namespace cgrt::math;

TEST(test_spectrum, spectrum_add) {
    auto x = spectrum{1.f, 2.f, 3.f};
    auto y = spectrum{2.f, 3.f, 4.f};
    auto z = spectrum{3.f, 5.f, 7.f};
    ASSERT_EQ(x+y, z);
}

TEST(test_spectrum, spectrum_inc) {
    auto x = spectrum{1.f, 2.f, 3.f};
    auto y = spectrum{2.f, 3.f, 4.f};
    auto z = spectrum{3.f, 5.f, 7.f};
    x += y;
    ASSERT_EQ(x, z);
}

TEST(test_spectrum, spectrum_add_assign) {
    auto x = spectrum{1.f, 2.f, 3.f};
    auto y = spectrum{2.f, 3.f, 4.f};
    auto z = spectrum{3.f, 5.f, 7.f};
    x = x + y;
    ASSERT_EQ(x, z);
}

TEST(test_spectrum, spectrum_scale) {
    auto x = spectrum{2.f, 2.f, 2.f};
    auto y = 0.5f;
    auto z = spectrum{1.f, 1.f, 1.f};
    ASSERT_EQ(x*y, z);
}


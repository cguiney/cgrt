//
// Created by chris on 9/19/20.
//

#include <gtest/gtest.h>

#include <cgrt/math/point.hpp>

using cgrt::math::scalar;

using namespace cgrt;

TEST(point_test, construction3D) {
    auto p = math::point<3>(1.f, 2.f, 3.f);

    ASSERT_EQ(p, math::point<3>(1, 2, 3));
    ASSERT_NE(p, math::point<3>(1, 2, 4));
}

TEST(point_test, addition)
{
    auto p1 = math::point<3>(3, -2, 5);
    auto p2 = math::point<3>(-2, 3, 1);

    ASSERT_EQ(p1 + p2, math::point<3>(1, 1, 6));
}

TEST(point_test, point_point_subtraction)
{
    auto p1 = math::point<3>(3, 2, 1);
    auto p2 = math::point<3>(5, 6, 7);
    ASSERT_EQ(p1 - p2, math::vector<3>(-2, -4, -6));
}

TEST(point_test, point_vector_subtraction)
{
    auto p = math::point<3>(3, 2, 1);
    auto v = math::vector<3>(5, 6, 7);
    ASSERT_EQ(p - v, math::point<3>(-2, -4, -6));
}

TEST(point_test, preincrement)
{
    for(int i = 0; i < 10; ++i) {
        std::cout << i << std::endl;
    }
}
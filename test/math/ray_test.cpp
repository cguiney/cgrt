//
// Created by chris on 1/8/21.
//

#include <gtest/gtest.h>

#include <cgrt/math/ray.hpp>
#include <cgrt/math/scalar.hpp>
#include <cgrt/math/transform.hpp>

using namespace cgrt;
using namespace cgrt::math;

TEST(test_ray, create_ray)
{
    auto origin = math::point<3>(1, 2, 3);
    auto direction = math::vector<3>(4, 5, 6);

    auto r = math::ray<3>{origin, direction};
    ASSERT_EQ(r.origin, origin);
    ASSERT_EQ(r.direction, direction);
}

TEST(test_ray, position)
{
    auto r = math::ray<3>{
        math::point<3>(2, 3, 4),
        math::vector<3>(1, 0, 0)
    };

    struct test
    {
        scalar t{};
        math::point<3> want{};
    };

    auto tests = std::vector<test>{
        {0.f,   math::point<3>(2, 3, 4)},
        {1.f,   math::point<3>(3, 3, 4)},
        {-1.f,  math::point<3>(1, 3, 4)},
        {2.5f,  math::point<3>(4.5, 3, 4)},
    };

    for (auto t : tests) {
        ASSERT_EQ(position(r, t.t), t.want) << "failed for " << (float) (t.t);
    }
}
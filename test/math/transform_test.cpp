//
// Created by chris on 11/29/20.
//

#include <cgrt/math/transform.hpp>
#include <cgrt/math/point.hpp>

#include <gtest/gtest.h>
#include <cgrt/objects/state.hpp>

using namespace cgrt;

TEST(test_transform, translation_mult)
{
    auto transform = math::transform::translation(5.f, -3.f, 2.f);

    auto p = math::point<3>(-3.f, 4.f, 5.f);
    auto have = transform(p);

    auto want = math::point<3>(2.f, 1.f, 7.f);
    ASSERT_EQ(have, want);
}

TEST(test_transform, inverse_translation_mult)
{
    auto transform = math::transform::translation(5.f, -3.f, 2.f);
    auto inverse = math::transform::inverse(transform);
    auto p = math::point<3>(-3.f, 4.f, 5.f);

    auto have = inverse(p);
    auto want = math::point<3>(-8.f, 7.f, 3.f);
    ASSERT_EQ(have, want);
}

#if 0
TEST(test_transform, translation_vector_failure)
{
    auto transform = math::transform::translation(5, -3, 2.f);
    auto v = math::vector<3>(-3, 4, 5.f);

    transform(v);
}
#endif

TEST(test_transform, scale_point)
{
    auto transform = math::transform::scale(2.f, 3.f, 4.f);
    auto p = math::point<3>(-4.f, 6.f, 8.f);
    auto have = transform(p);
    auto want = math::point<3>(-8.f, 18.f, 32.f);
    ASSERT_EQ(have, want);
}

TEST(test_transform, scale_vector)
{
    auto transform = math::transform::scale(2.f, 3.f, 4.f);
    auto v = math::vector<3>(-4.f, 6.f, 8.f);
    auto have = transform(v);
    auto want = math::vector<3>(-8.f, 18.f, 32.f);
    ASSERT_EQ(have, want);
}

TEST(test_transform, scale_vector_inv)
{
    auto transform = math::transform::scale(2.f, 3.f, 4.f);
    auto inv = math::transform::inverse(transform);
    auto v = math::vector<3>(-4.f, 6.f, 8.f);
    auto have = inv(v);
    auto want = math::vector<3>(-2.f, 2.f, 2.f);
    ASSERT_EQ(have, want);
}

TEST(test_transform, reflection_is_scaling_by_negative)
{
    auto transform = math::transform::scale(-1.f, 1.f, 1.f);
    auto p = math::point<3>(2.f, 3.f, 4.f);
    ASSERT_EQ(transform(p), math::point<3>(-2.f, 3.f, 4.f));
}

TEST(test_transform, rotation_x)
{
    auto p = math::point<3>(0.f, 1.f, 0.f);
    auto half_quarter = math::transform::rotate_x(CGRT_PI_4);
    auto full_quarter = math::transform::rotate_x(CGRT_PI_2);

    ASSERT_EQ(half_quarter(p), math::point<3>(0.f, CGRT_SQRT2 / 2.f, CGRT_SQRT2 / 2.f));
    ASSERT_EQ(full_quarter(p), math::point<3>(0.f, 0.f, 1.f));
}

TEST(test_transform, rotation_x_inv)
{
    auto p = math::point<3>(0.f, 1.f, 0.f);
    auto half_quarter = math::transform::rotate_x(CGRT_PI_4);
    auto inv = math::transform::inverse(half_quarter);

    ASSERT_EQ(inv(p), math::point<3>(0.f, CGRT_SQRT2 / 2.f, -CGRT_SQRT2 / 2.f));
}

TEST(test_transform, rotation_y)
{
    auto p = math::point<3>(0.f, 0.f, 1.f);
    auto half_quarter = math::transform::rotate_y(CGRT_PI_4);
    auto full_quarter = math::transform::rotate_y(CGRT_PI_2);

    ASSERT_EQ(half_quarter(p), math::point<3>(CGRT_SQRT2 / 2.f, 0.f, CGRT_SQRT2 / 2.f));
    ASSERT_EQ(full_quarter(p), math::point<3>(1.f, 0.f, 0.f));
}

TEST(test_transform, rotation_z)
{
    auto p = math::point<3>(0.f, 1.f, 0.f);
    auto half_quarter = math::transform::rotate_z(CGRT_PI_4);
    auto full_quarter = math::transform::rotate_z(CGRT_PI_2);

    ASSERT_EQ(half_quarter(p), math::point<3>(-CGRT_SQRT2 / 2.f, CGRT_SQRT2 / 2.f, 0.f));
    ASSERT_EQ(full_quarter(p), math::point<3>(-1.f, 0.f, 0.f));
}

TEST(test_transform, shearing)
{
    struct test
    {
        math::transform::matrix t;
        math::point<3> p;
        math::point<3> want;
    };

    using math::transform::shear;

    auto tests = std::vector<test>{
        {
            shear(0.f, 1.f, 0.f, 0.f, 0.f, 0.f),
            math::point<3>(2.f, 3.f, 4.f),
            math::point<3>(6.f, 3.f, 4.f),
        },
        {
            shear(0.f, 0.f, 1.f, 0.f, 0.f, 0.f),
            math::point<3>(2.f, 3.f, 4.f),
            math::point<3>(2.f, 5.f, 4.f),
        },
        {
            shear(0.f, 0.f, 0.f, 1.f, 0.f, 0.f),
            math::point<3>(2.f, 3.f, 4.f),
            math::point<3>(2.f, 7.f, 4.f),
        },
        {
            shear(0.f, 0.f, 0.f, 0.f, 1.f, 0.f),
            math::point<3>(2.f, 3.f, 4.f),
            math::point<3>(2.f, 3.f, 6.f),
        },
        {
            shear(0.f, 0.f, 0.f, 0.f, 0.f, 1.f),
            math::point<3>(2.f, 3.f, 4.f),
            math::point<3>(2.f, 3.f, 7.f),
        }
    };


    for(auto& t : tests)
    {
        ASSERT_EQ(t.t(t.p), t.want);
    }
}

TEST(test_transform, applied_in_squence)
{
    auto p = math::point<3>(1.f, 0.f, 1.f);

    auto A = math::transform::rotate_x(CGRT_PI_2);
    auto B = math::transform::scale(5.f, 5.f, 5.f);
    auto C = math::transform::translation(10.f, 5.f, 7.f);

    auto p2 = A(p);
    ASSERT_EQ(p2, math::point<3>(1.f, -1.f, 0.f));

    auto p3 = B(p2);
    ASSERT_EQ(p3, math::point<3>(5.f, -5.f, 0.f));

    auto p4 = C(p3);
    ASSERT_EQ(p4, math::point<3>(15.f, 0.f, 7.f));

    auto T = C * B * A;
    auto p5 = T(p);
    ASSERT_EQ(p5, math::point<3>(15.f, 0.f, 7.f));
}

TEST(test_transform, translating_ray)
{
    auto r = math::ray<3>{
        math::point<3>(1.f, 2.f, 3.f),
        math::vector<3>(0.f, 1.f, 0.f)
    };

    auto m = math::transform::translation(3.f, 4.f, 5.f);
    auto r2 = m(r);

    ASSERT_EQ(r2.origin, math::point<3>(4.f, 6.f, 8.f));
    ASSERT_EQ(r2.direction, math::vector<3>(0.f, 1.f, 0.f));
}

TEST(test_transform, scaling_ray)
{
    auto r = math::ray<3>{
        math::point<3>(1.f, 2.f, 3.f),
        math::vector<3>(0.f, 1.f, 0.f)
    };

    auto m = math::transform::scale(2.f, 3.f, 4.f);
    auto r2 = m(r);

    ASSERT_EQ(r2.origin, math::point<3>(2.f, 6.f, 12.f));
    ASSERT_EQ(r2.direction, math::vector<3>(0.f, 3.f, 0.f));
}

TEST(test_transform, default_transformation)
{
    cgrt::objects::builder b;
    cgrt::objects::object_id id;

    b.create(cgrt::objects::SPHERE).tag(id).finish();

    auto s = b.build();

    auto have = s.transforms[id];
    auto want = math::identity<4>();
    ASSERT_EQ(have, want);
}

TEST(test_transform, camera_view_default)
{
    auto m = math::transform::camera_view(
        math::point<3>(0.f),
        math::point<3>(0.f, 0.f, -1.f),
        math::vector<3>(0.f, 1.f, 0.f)
    );

    if(m.mtx() != math::identity<4>()) {
        throw std::runtime_error("not equal");
    }
    // ASSERT_EQ(m.mtx(), math::identity<4>());
}

TEST(test_transform, camera_view_in_positive_z)
{
    auto m = math::transform::camera_view(
        math::point<3>(0.f),
        math::point<3>(0.f, 0.f, 1.f),
        math::vector<3>(0.f, 1.f, 0.f)
    );

    auto s = math::transform::scale(-1.f, 1.f, -1.f);

    ASSERT_EQ(m.mtx(), s.mtx());
}

TEST(test_transform, camera_transform_moves_world)
{
    auto m = math::transform::camera_view(
        math::point<3>(0.f, 0.f, 8.f),
        math::point<3>(0.f, 0.f, 0.f),
        math::vector<3>(0.f, 1.f, 0.f)
    );

    auto s = math::transform::translation(0.f, 0.f, -8.f);

    ASSERT_EQ(m.mtx(), s.mtx());
}

TEST(test_transform, camera_transform_arbitrary_view)
{
    auto m = math::transform::camera_view(
        math::point<3>(1.f, 3.f, 2.f),
        math::point<3>(4.f, -2.f, 8.f),
        math::vector<3>(1.f, 1.f, 0.f)
    );

    auto want = math::transform::matrix::matrix_type{
        {-0.50709f , 0.50709f ,  0.67612f , -2.36643f },
        { 0.76772f , 0.60609f ,  0.12122f , -2.82843f },
        {-0.35857f , 0.59761f , -0.71714f ,  0.00000f },
        { 0.00000f , 0.00000f ,  0.00000f ,  1.00000f },
    };

    ASSERT_EQ(m.mtx(), want);
}

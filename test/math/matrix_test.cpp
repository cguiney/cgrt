//
// Created by chris on 11/26/20.
//

#include <cgrt/math/matrix.hpp>
#include <gtest/gtest.h>

using namespace cgrt::math;

TEST(test_matrix, equality) {
	auto m = matrix<4, 4>();
	auto n = matrix<4, 4>();

	ASSERT_TRUE(n == m);
	ASSERT_FALSE(n != m);
}

TEST(test_matrix, inequality) {
	auto m = matrix<4, 4>();

	matrix<4, 4> n = {{2.f, 2.f, 2.f, 2.f}, {2.f, 2.f, 2.f, 2.f}, {2.f, 2.f, 2.f, 2.f}, {2.f, 2.f, 2.f, 2.f}};

	ASSERT_FALSE(n == m);
	ASSERT_TRUE(n != m);
}

TEST(test_matrix, multiply) {
	auto A = matrix<4, 4>({{1.f, 2.f, 3.f, 4.f}, {5.f, 6.f, 7.f, 8.f}, {9.f, 8.f, 7.f, 6.f}, {5.f, 4.f, 3.f, 2.f}});

	auto B = matrix<4, 4>({{-2.f, 1.f, 2.f, 3.f}, {3.f, 2.f, 1.f, -1.f}, {4.f, 3.f, 6.f, 5.f}, {1.f, 2.f, 7.f, 8.f}});

	auto C = matrix<4, 4>(
	    {{20.f, 22.f, 50.f, 48.f}, {44.f, 54.f, 114.f, 108.f}, {40.f, 58.f, 110.f, 102.f}, {16.f, 26.f, 46.f, 42.f}});

	auto have = A * B;

	ASSERT_EQ(have, C);
	ASSERT_TRUE(have == C);
}

TEST(test_matrix, vector_mul) {
	auto A = matrix<4, 4>{
	    {1.f, 2.f, 3.f, 4.f},
	    {2.f, 4.f, 4.f, 2.f},
	    {8.f, 6.f, 4.f, 1.f},
	    {0.f, 0.f, 0.f, 1.f},
	};

	auto b = matrix<4, 4>::vector_type{1.f, 2.f, 3.f, 1.f};
	auto have = A * b;
	auto want = matrix<4, 4>::vector_type{18.f, 24.f, 33.f, 1.f};

	ASSERT_EQ(have, want);
}

TEST(test_matrix, mul_by_identity) {
	auto A = matrix<4, 4>{
	    {0.f, 1.f, 2.f, 4.f},
	    {1.f, 2.f, 4.f, 8.f},
	    {2.f, 4.f, 8.f, 16.f},
	    {2.f, 8.f, 16.f, 32.f},
	};

	auto i = cgrt::math::identity<4>();

	auto have = A * i;

	ASSERT_EQ(have, A);
}

TEST(test_matrix, transpose) {
	auto A = matrix<4, 4>{
	    {0.f, 9.f, 3.f, 0.f},
	    {9.f, 8.f, 0.f, 8.f},
	    {1.f, 8.f, 5.f, 3.f},
	    {0.f, 0.f, 5.f, 8.f},
	};

	auto B = transpose(A);

	auto want = matrix<4, 4>{
	    {0.f, 9.f, 1.f, 0.f},
	    {9.f, 8.f, 8.f, 0.f},
	    {3.f, 0.f, 5.f, 5.f},
	    {0.f, 8.f, 3.f, 8.f},
	};

	ASSERT_EQ(B, want);
}

TEST(test_matrix, transpose_n_by_m) {
	auto A = matrix<3, 2>{
	    {0.f, 9.f, 3.f},
	    {9.f, 8.f, 0.f},
	};
	auto want = matrix<2, 3>{
	    {0.f, 9.f},
	    {9.f, 8.f},
	    {3.f, 0.f},
	};

	auto have = transpose(A);

	ASSERT_EQ(have, want);
}

TEST(test_matrix, transpose_identity) {
	auto A = matrix<4, 4>();

	ASSERT_EQ(A, transpose(A));
}

TEST(test_matrix, determinant_2x2) {
	auto A = matrix<2, 2>{
	    { 1.f, 5.f},
	    {-3.f, 2.f},
	};

	auto have = determinant(A);

	ASSERT_EQ(have, 17.f);
}

TEST(test_matrix, submatrix_3x3) {
	auto A = matrix<3, 3>{
	    { 1.f, 5.f,  0.f},
	    {-3.f, 2.f,  7.f},
	    { 0.f, 6.f, -3.f},
	};

	auto want = matrix<2, 2>{
	    {-3.f, 2.f},
	    { 0.f, 6.f},
	};

	auto have = submatrix(A, 0, 2);

	ASSERT_EQ(have, want);
}

TEST(test_matrix, submatrix4x4) {
	auto A = matrix<4, 4>{
        {-6.f, 1.f,  1.f, 6.f},
        {-8.f, 5.f,  8.f, 6.f},
        {-1.f, 0.f,  8.f, 2.f},
        {-7.f, 1.f, -1.f, 1.f}
    };

	auto have = submatrix(A, 2, 1);
	auto want = matrix<3, 3>{
	    {-6.f, 1.f, 6.f},
	    {-8.f, 8.f, 6.f},
	    {-7.f, -1.f, 1.f},
	};
	ASSERT_EQ(have, want);
}

TEST(test_matrix, minor3x3) {
	auto A = matrix<3, 3>{
	    {3.f,  5.f,  0.f},
	    {2.f, -1.f, -7.f},
	    {-6.f, -1.f, 5.f},
	};

	auto B = submatrix(A, 1, 0);
	auto have_determinant = determinant(B);
	auto have_minor = minor(A, 1, 0);
	ASSERT_EQ(have_determinant, 25.f);
	ASSERT_EQ(have_minor, 25.f);
}

TEST(test_matrix, cofactor3x3) {
	auto A = matrix<3, 3>{
	    { 3.f,  5.f,  0.f},
	    { 2.f, -1.f, -7.f},
	    {-6.f, -1.f,  5.f},
	};

	ASSERT_EQ(minor(A, 0, 0), -12.f);
	ASSERT_EQ(cofactor(A, 0, 0), -12.f);

	ASSERT_EQ(minor(A, 1, 0), 25.f);
	ASSERT_EQ(cofactor(A, 1, 0), -25.f);
}

TEST(test_matrix, determinant3x3) {
	auto A = matrix<3, 3>{
	    { 1.f, 2.f,  6.f},
	    {-5.f, 8.f, -4.f},
	    { 2.f, 6.f,  4.f},
	};

	ASSERT_EQ(cofactor(A, 0, 0), 56.f);
	ASSERT_EQ(cofactor(A, 0, 1), 12.f);
	ASSERT_EQ(cofactor(A, 0, 2), -46.f);
	ASSERT_EQ(determinant<3>(A), -196.f);
}

TEST(test_matrix, determinant4x4) {
	auto A = matrix<4, 4>{
	    {-2.f, -8.f,  3.f,  5.f},
	    {-3.f,  1.f,  7.f,  3.f},
	    { 1.f,  2.f, -9.f,  6.f},
	    {-6.f,  7.f,  7.f, -9.f},
	};

	ASSERT_EQ(cofactor(A, 0, 0), 690.f);
	ASSERT_EQ(cofactor(A, 0, 1), 447.f);
	ASSERT_EQ(cofactor(A, 0, 3), 51.f);
	ASSERT_EQ(determinant(A), -4071.f);
}

TEST(test_matrix, inverse4x4) {
	auto A = matrix<4, 4>{
	    {-5.f, 2.f, 6.f, -8.f},
	    {1.f, -5.f, 1.f, 8.f},
	    {7.f, 7.f, -6.f, -7.f},
	    {1.f, -3.f, 7.f, 4.f},
	};

	auto B = inverse(A);

	auto want = matrix<4, 4>{
	    {0.21805f, 0.45113f, 0.24060f, -0.04511f},
	    {-0.80827f, -1.45677f, -0.44361f, 0.52068f},
	    {-0.07895f, -0.22368f, -0.05263f, 0.19737f},
	    {-0.52256f, -0.81391f, -0.30075f, 0.30639f},
	};

	ASSERT_EQ(B[3][0], -0.52256f);
	ASSERT_EQ(B[3][1], -0.81391f);
	ASSERT_EQ(B[3][2], -0.30075f);
	ASSERT_EQ(B[3][3], 0.30639f);
	ASSERT_EQ(determinant(A), 532.f);
	ASSERT_EQ(cofactor(A, 2, 3), -160.f);
	ASSERT_EQ(B[3][2], -160.f / 532.f);
	ASSERT_EQ(B, want);
}

TEST(test_matrix, inverse_2) {
	{
		auto A = matrix<4, 4>{
		    { 8.f, -5.f,  9.f,  2.f},
		    { 7.f,  5.f,  6.f,  1.f},
		    {-6.f,  0.f,  9.f,  6.f},
		    {-3.f,  0.f, -9.f, -4.f},
		};

		auto have = inverse(A);

		auto want = matrix<4, 4>{
		    {-0.15385f, -0.15385f, -0.28205f, -0.53846f},
		    {-0.07692f,  0.12308f,  0.02564f,  0.03077f},
		    { 0.35897f,  0.35897f,  0.43590f,  0.92308f},
		    {-0.69231f, -0.69231f, -0.76923f, -1.92308f},
		};

		ASSERT_EQ(have, want);
	}

	{
		auto A = matrix<4, 4>{
		    { 9.f,  3.f,  0.f,  9.f},
		    {-5.f, -2.f, -6.f, -3.f},
		    {-4.f,  9.f,  6.f,  4.f},
		    {-7.f,  6.f,  6.f,  2.f},
		};

		auto have = inverse(A);
		auto want = matrix<4, 4>{
		    {-0.04074f, -0.07778f, 0.14444f, -0.22222f},
		    {-0.07778f, 0.03333f, 0.36667f, -0.33333f},
		    {-0.02901f, -0.14630f, -0.10926f, 0.12963f},
		    {0.17778f, 0.06667f, -0.26667f, 0.33333f},
		};

		ASSERT_EQ(have, want);
	}
}

TEST(test_matrix, inverse_mul) {
	auto A = matrix<4, 4>{
	    { 3.f, -9.f,  7.f,  3.f},
	    { 3.f, -8.f,  2.f, -9.f},
	    {-4.f,  4.f,  4.f,  1.f},
	    {-6.f,  5.f, -1.f,  1.f},
	};

	auto B = matrix<4, 4>{
	    {8.f, 2.f, 2.f, 2.f},
	    {3.f, -1.f, 7.f, 0.f},
	    {7.f, 0.f, 5.f, 4.f},
	    {6.f, -2.f, 0.f, 5.f},
	};

	auto C = A * B;

	ASSERT_EQ(C * inverse(B), A);
}
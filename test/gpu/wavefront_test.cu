//
// Created by chris on 11/13/2021.
//

#include <cgrt/math/random.hpp>
#include <cgrt/math/vector.hpp>
#include <cgrt/math/point.hpp>
#include <cgrt/math/ray.hpp>
#include <cgrt/objects/cameras.hpp>

#include <cuda_runtime.h>
#include <gtest/gtest.h>

#include "../../src/cgrt-cuda/kernel.cuh"

using namespace cgrt;
using namespace cgrt::objects;
using namespace cgrt::containers;

using ray = cgrt::math::ray<3>;
using point = cgrt::math::point<3>;
using vector = cgrt::math::vector<3>;
using continuous_random_variable_2d = math::continuous_random_variable<2>;

template<typename T>
using cuda_storage_t = typename storage<runtime::cuda, T>::type;

__device__ __host__ bool is_even(unsigned x) {
	return x % 2 == 0;
}

CGRT_KERNEL void rng_initialize(span<math::rng> rngs, unsigned seed) {
    auto i = cgrt::detail::index();
    if(i >= rngs.size()) return;
    rngs[i] = math::rng(i, seed + i);
}

template<size_t N>
CGRT_KERNEL void rng_sample(span<math::rng> rngs, span<math::continuous_random_variable<N>> random_samples) {
    assert(rngs.size() == random_samples.size());

    auto i = cgrt::detail::index();
    if(i >= rngs.size()) return;
    random_samples[i] = math::continuous_random_variable<N>(rngs[i]);
}

CGRT_KERNEL void generate_all_rays(span<ray> rs, span<continuous_random_variable_2d> random_samples, span<camera> cs) {
    assert(rs.size() == rngs.size());
    auto& c = cs[0];

    auto pixel = cgrt::detail::coordinates();
    auto id = cgrt::detail::index();
    if(id >= rs.size()) return;

    auto xi = random_samples[id];
    auto x = xi.x() + static_cast<math::scalar>(pixel.x());
    auto y = xi.y() + static_cast<math::scalar>(pixel.y());
    rs[id] = objects::generate_ray(c, x, y);
}

template<typename T>
auto check_cuda_value(T fn, const char* msg) {
    cudaError code = cudaSuccess;
    auto r = fn();
    code = cudaDeviceSynchronize();
    EXPECT_EQ(code, cudaSuccess) << msg << ": " << cudaGetErrorString(code);
    return std::move(r);
}

template<typename T>
void check_cuda_action(T fn, const char* msg) {
    cudaError code = cudaSuccess;
    fn();
    code = cudaDeviceSynchronize();
    ASSERT_EQ(code, cudaSuccess) << msg << ": " << cudaGetErrorString(code);
}

TEST(test_gpu, can_generate_all_rays) {
    cudaError code = cudaSuccess;
    ASSERT_EQ(code, cudaSuccess);

    cudaDeviceSetLimit(cudaLimitStackSize, 32*1024);

    auto c = default_camera();
    int work_units_x = c.width;
    int work_units_y = c.height;
    int threads_per_block_x = 64; /* n x m thread counts */
    int threads_per_block_y = 64;
    dim3 threads(threads_per_block_x, threads_per_block_y);
    dim3 blocks(work_units_x / threads_per_block_x +1, work_units_y / threads_per_block_y +1);

    auto cs = check_cuda_value([&]() {
        auto cs = cuda_storage_t<camera>(1);
        cs[0] = c;
        return std::move(cs);
    }, "failed creating camera storage");


    auto rs = check_cuda_value([&]() {
        return std::move(cuda_storage_t<ray>(c.height * c.width));
    }, "failed creating ray storage");

    auto rngs = check_cuda_value([&]() {
        return std::move(cuda_storage_t<math::rng>(c.height * c.width));
    }, "failed creating rng storage");

    check_cuda_action([&]() -> void {
        rng_initialize<<<threads, blocks>>>(rngs, 0);
    }, "failed initializing random number generators");


    auto random_samples = check_cuda_value([&]() {
        return std::move(cuda_storage_t<math::continuous_random_variable<2>>(c.height * c.width));
    }, "failed creating random number storage");

    check_cuda_action([&]() {
        rng_sample<2><<<threads, blocks>>>(rngs, random_samples);
    }, "failed collecting random number samples");

    check_cuda_action([&](){
        generate_all_rays<<<threads, blocks>>>(rs, random_samples, cs);
    }, "failed generating rays");

    auto host_storage = check_cuda_value([&]() {
        return rs.to_host();
    }, "failed reading rays from device");

    for(auto&& r : host_storage) {
        ASSERT_EQ(r.origin, point{});
        ASSERT_NE(r.direction, vector{});
    }
}

/*
template<typename T>
__global__ void queue_random_reals(typename cuda::device_queue<T> q, math::rng* rng, int n) {
	auto x = threadIdx.x + blockIdx.x * blockDim.x;
	auto y = threadIdx.y + blockIdx.y * blockDim.y;
	auto i = y * (gridDim.x * blockDim.x) + x;

	if(i > n)
		return;

	auto v = 0.0f;
	for(int j = 0; j < 100; j++) {
		v = rng[i].real();
	}
	q.push_back(v);
}

__global__ void print_matricies(containers::span<math::transform::matrix> transforms) {
	for(const auto& transform : transforms) {
		printf("[[%f %f %f %f][%f %f %f %f][%f %f %f %f][%f %f %f %f]]\n", (float) transform[0][0],
		       (float) transform[0][1], (float) transform[0][2], (float) transform[0][3], (float) transform[1][0],
		       (float) transform[1][1], (float) transform[1][2], (float) transform[1][3], (float) transform[2][0],
		       (float) transform[2][1], (float) transform[2][2], (float) transform[2][3], (float) transform[3][0],
		       (float) transform[3][1], (float) transform[3][2], (float) transform[3][3]);

		auto inv = math::transform::inverse(transform);
		printf("[[%f %f %f %f][%f %f %f %f][%f %f %f %f][%f %f %f %f]]\n\n", (float) inv[0][0], (float) inv[0][1],
		       (float) inv[0][2], (float) inv[0][3], (float) inv[1][0], (float) inv[1][1], (float) inv[1][2],
		       (float) inv[1][3], (float) inv[2][0], (float) inv[2][1], (float) inv[2][2], (float) inv[2][3],
		       (float) inv[3][0], (float) inv[3][1], (float) inv[3][2], (float) inv[3][3]);
	}
}

TEST(test_camera, can_generate_rays) {
	dim3 grid(10, 10);
	dim3 block(10, 10);

	auto size = grid.x * grid.y * block.x * block.y;
	auto b = cgrt::scene::builder{};
	b.set_film(grid.x * block.x, grid.y * block.y);

	auto st = spheres_example(100, 100);
	auto camera = cuda::make_device<cgrt::objects::camera>(st.camera);

	auto dst = dense_state{st};
	auto gst = kernel::wavefront::state_owner{dst};

	auto pipeline = kernel::wavefront::pipeline{grid, block, std::move(gst)};
	pipeline.init();

	cudaError_t code = cudaSuccess;
	code = cudaDeviceSynchronize();
	ASSERT_EQ(code, cudaSuccess);

	std::vector<kernel::path_segment> results;
	pipeline.path_segments.copy(results);

	auto j = 0;
	for(const auto& i : results) {
		std::cerr << std::setw(5) << i.path;
		if(++j == 10) {
			std::cerr << std::endl;
			j = 0;
		}
	}

	// try finding intersections
	pipeline.trace();

	auto xs = std::vector<kernel::shading_eval>{};
	pipeline.shading_evals.copy(xs);

	auto k = 0;
	for(const auto& x : xs) {
		std::cerr << std::setw(5) << x.intersection.id << " at " << x.intersection.time;
		if(++k == 10) {
			std::cerr << std::endl;
			k = 0;
		}
	}
}
*/
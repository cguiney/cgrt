#pragma once
#ifndef CGRT_WHITTED_CLH
#define CGRT_WHITTED_CLH

#include "kernel.h"
#include "shading.h"
#include "camera.h"
#include "intersect.h"
#include "interaction.h"
#include "sampling.h"

KERNEL struct spectrum rtc_shade(
    const struct context *ctx,
    const struct interaction_t *ia
);

KERNEL struct spectrum integrate_unidirectional(
    const struct context *ctx,
    const struct ray *init,
    struct spectrum *contribution
);

KERNEL bool rtc_obstruction_test(
    const struct context *ctx,
    const struct interaction_t *ia
);

KERNEL struct spectrum rtc_shade(
    const struct context *ctx,
    const struct interaction_t *ia
)
{
    /*
     * Select a random light!
     * Keep it to point lights for now.
     */
    float n_lights = ctx->world->lights.n_lights;
    if(n_lights == 0)
    {
        return CGRT_COLOR_BLACK;
    }

    unsigned random_light = pcg32_boundedrand_r(ctx->rng, ctx->world->lights.n_lights);

    struct light light = ctx->world->lights.lights[random_light];

    struct spectrum estimate = shade_direct(ctx, ia, &light);
    return spectrum_scale(&estimate, n_lights);
}

#define TRACE_DEPTH 7

enum rtc_trace_state
{
    TRACING,
    CONVERGED,
};

struct integration_frame
{
    enum rtc_trace_state state;

    struct ray input;
    struct intersection_t hit;
    struct interaction_t ia;

    struct sample sample;
    struct spectrum weight;

    struct spectrum direct;
    struct spectrum indirect;

    struct spectrum *contribution;
};

KERNEL struct spectrum integrate_unidirectional(
    const struct context *ctx,
    const struct ray *init,
    struct spectrum *contribution
)
{
    /* handy to have these around */
    *contribution = CGRT_COLOR_BLACK; /* be sure to default the contribution */

    struct integration_frame stack[TRACE_DEPTH] = {};
    struct integration_frame *top = &stack[TRACE_DEPTH];
    struct integration_frame *bottom = &stack[0];
    struct integration_frame *sp = bottom;

    /* initialize the frame */
    sp->state = TRACING;
    sp->input = *init;
    sp->contribution = contribution;
    sp->weight = FILL_SPECTRUM(1.f);

    while (sp >= bottom) {
        /* based on the current ray state, decide what to do next*/
        switch (sp->state) {
            case TRACING: {
                /* base case - we're at stack depth */
                if (sp >= (top - 1)) {
                    *sp->contribution = CGRT_COLOR_BLACK;
                    sp->state = CONVERGED;
                    continue;
                }

                /* trace the input */
                intersection_clear(ctx->xs);

                /* query for intersections */
                intersection_query_objects(&sp->input, ctx->world, ctx->xs);

                /* no intersections, converge on black */
                if (!intersection_hit(ctx->xs, &sp->hit)) {
                    *sp->contribution = CGRT_COLOR_BLACK;
                    sp->state = CONVERGED;
                    continue;
                }


                /* precompute what's happening at the intersection */
                interact(ctx->world, &sp->hit, &sp->input, &sp->ia);

                /* account for any emission from the object */
                if (sp == bottom) {
                    if(sp->ia.intersection.obj.is_light) {
                        struct light_object *li = &ctx->world->lights.objects[sp->ia.intersection.obj.light_id];
                        struct vector wi = vector_negate(&sp->input.dir);
                        struct vector n  = sp->ia.surface.shading.normal;
                        struct spectrum emission = lighting_object_emit(ctx, li, &n, &wi);
                        emission = spectrum_mul(&emission, &sp->weight);
                        *sp->contribution = spectrum_add(sp->contribution, &emission);
                    }
                }

                /*
                 * - initialize the bsdf
                 * - compute direct contribution
                 * - sample bsdf to determine next bounce direction
                 */
                sp->direct = rtc_shade(ctx, &sp->ia);

                /* adjust the direct contribution by the weight for this ray */
                sp->direct = spectrum_mul(&sp->direct, &sp->weight);

                /*
                 * Start setting up the next frame based on a sampling from the current intersection
                 * that is, we're testing the bsdf
                 */
                struct scattering_sample sample = {};
                if (!bsdf_sample(ctx, &sp->ia.bsdf, &sp->ia.input.dir, &sample)) {
                    sp->state = CONVERGED;
                    continue;
                }

                if (spectrum_zero(&sample.reading.energy) || sample.reading.pdf <= 0)
                {
                    sp->state = CONVERGED;
                    continue;
                }

                /*
                 * everything completed with this ray. cast off it's children to collect their information
                 * and sum it during convergence.
                 */
                sp->state = CONVERGED;

                /*
                 * calculate the weight of the next ray based off the energy carried by it, and decayed by
                 * it's depth in the path
                 */
                float angle = abs(dot(&sample.reading.direction, &sp->ia.surface.shading.normal));
                struct spectrum weight = {};
                weight = spectrum_scale(&sample.reading.energy, angle);
                weight = spectrum_scale(&weight, 1.f/sample.reading.pdf);
                weight = spectrum_mul(&weight, &sp->weight);

                struct integration_frame next = {};
                next.state = TRACING;
                next.input.origin = sp->ia.above;
                next.input.dir = sample.reading.direction;
                next.weight = weight;
                next.contribution = &sp->indirect;

                *(++sp) = next;

                continue;
            }
            case CONVERGED:
                /*
                 * all of a rays children have been processed:
                 * all different paths contributions are ready, add the reflection and contribution together
                 * combine them, and then
                 */

                *sp->contribution = spectrum_add(sp->contribution, &sp->direct);
                *sp->contribution = spectrum_add(sp->contribution, &sp->indirect);

                /* pop the stack */
                --sp;
                break;
            default:
                goto exit;
        }
    }
    exit:
    return *contribution;
}

KERNEL struct spectrum rtc_trace(const struct context *ctx)
{
    struct spectrum contribution = CGRT_COLOR_BLACK;

    for (int i = 0; i < ctx->frame->spp; i++) {
        float rx = sample_random_real(ctx);
        float ry = sample_random_real(ctx);
        float x = ctx->frame->x + rx;
        float y = ctx->frame->y + ry;
        struct ray r = camera_generate_ray(&ctx->world->camera, x, y);
        struct spectrum sample = integrate_unidirectional(ctx, &r, &sample);
        contribution = spectrum_add(&contribution, &sample);
    }

    return contribution;
}


#endif
#ifndef CGRT_SCHEMA_CLH
#define CGRT_SCHEMA_CLH

#include "algebra.h"
#include "interaction.h"
#include "kernel.h"
#include "sampling.h"

struct surface_t
{
    /*
     * point in world space
     * this is equivalent to surface.position (which is in object space)
     */
    struct point position;

    /*
     * Details about the surface as it needs to be shown
     */
    struct
    {
        /* 2d mapping coordinates */
        float u;
        float v;

        struct vector normal;
    } shading;

    /*
     * Details about the physical surface area
     */
    struct
    {
        struct vector normal;

        /* partial derivatives used for creating an orthonormal basis with the surface normal */
        struct vector du;
        struct vector dv;
    } geometry;

};

KERNEL float surface_area_sphere()
{
    float phi_max = radians(360),
            radius = 1.f,
            z_min = -radius,
            z_max = radius;

    return phi_max * radius * (z_max - z_min);
}

KERNEL float surface_area(
    const struct object *obj
)
{
    switch (obj->type) {
        case sphere:
            return surface_area_sphere();
            break;
        default:
            return 0;
    }
}

KERNEL void surface_query_sphere(
    const struct point *op,
    struct surface_t *surf
)
{
    float radius = 1.f;
    struct point origin = FILL_POINT(0);
    struct vector path = point_sub(op, &origin);
    float distance = vector_magnitude(&path);

    float px = op->value.x * (radius/distance);
    float py = op->value.y * (radius/distance);
    float pz = op->value.z * (radius/distance);

   if(scalar_eq(px, 0.f) && scalar_eq(py, 0.f))
       px = 1e-5f * radius;

    float phi = atan2f(py, px);
    if(phi < 0.f)
        phi += 2.f * CGRT_PI;

    float phi_max = 2 * CGRT_PI; // radians(360.f);
    float theta = acosf(clamp(pz / radius, -1.f, 1.f));
    float theta_min = CGRT_PI; // acosf(-1f);
    float theta_max = 0; // acosf(1f);
    float theta_diff = (theta_max - theta_min);

    surf->shading.u = (phi/phi_max); // (1.0f - ((phi / phi_max) + 0.5f));
    surf->shading.v = (theta - theta_min) / theta_diff;

    // float inv_z_radius = rsqrt(px * px + py * py);
    float inv_z_radius = 1/ sqrt(px*px + py*py);
    float cos_phi = px * inv_z_radius;
    float sin_phi = py * inv_z_radius;

    surf->position = INIT_POINT(px, py, pz);
    surf->geometry.normal = path;
    surf->geometry.du = INIT_VECTOR(-phi_max * py, phi_max * px, 0.f);
    surf->geometry.dv = INIT_VECTOR(theta_diff * (pz * cos_phi),
                                    theta_diff * (pz * sin_phi),
                                    theta_diff * (-radius * sinf(theta)));
}

KERNEL void surface_query_disk(
    const struct point *op,
    struct surface_t *surf
)
{
    float px = op->value.x;
    float py = op->value.y;
    float distsq = px * px + py * py;
    float r = sqrt(distsq);
    float phi_max = 2*CGRT_PI;
    float phi = atan2f(py, px);
    if(phi < 0)
        phi += 2 * CGRT_PI;

    surf->position = INIT_POINT(px, py, 0);
    surf->shading.u = phi / phi_max;
    surf->shading.v = (1 - r);
    surf->geometry.normal = INIT_VECTOR(0, 0, 1);
    surf->geometry.du = INIT_VECTOR(-phi_max * py, phi_max * px, 0);
    surf->geometry.dv = INIT_VECTOR(px * -1 / r, py * -1 / r, 0);
}

KERNEL void surface_query_plane(
    const struct point *op,
    struct surface_t *surf
)
{
    surf->position = INIT_POINT(op->value.x, op->value.y, 0);
    surf->geometry.normal = INIT_VECTOR(0.f, 0.f, 1.f);
    // vector_coordinate_system(&surf->geometry.normal, &surf->geometry.du, &surf->geometry.dv);
    //surf->geometry.du = INIT_VECTOR(-1, 1, 0);
    //surf->geometry.dv = INIT_VECTOR(1, 1, 0);
    // surf->geometry.du = vector_negate(&surf->geometry.du);
    // surf->geometry.du = vector_negate(&surf->geometry.du);
    surf->geometry.du = INIT_VECTOR(-op->value.y, op->value.x, 0);
    surf->geometry.dv = INIT_VECTOR(op->value.x, op->value.y, 0);

    surf->shading.u = 1.f;
    surf->shading.v = 1.f;
}

KERNEL void surface_query(
    const struct object *obj,
    const struct transform* transforms,
    const struct point *wp,
    struct surface_t *surf
)
{
    struct transform_view view = INIT_TRANSFORM(transforms);
    view = transform_inverse(&view);

    struct point op = transform_view_apply_point(&view, wp);

    switch (obj->type) {
        case sphere:
            surface_query_sphere(&op, surf);
            break;
        case plane:
            surface_query_plane(&op, surf);
            break;
        case disk:
            surface_query_disk(&op, surf);
            break;
        default:
            break;
    }

    view = transform_inverse(&view);

    surf->geometry.normal = vector_cross(&surf->geometry.du, &surf->geometry.dv);
    surf->geometry.normal = vector_normalize(&surf->geometry.normal);

    /* transform normal to world space, normalize & capture */

    surf->position = transform_view_apply_point(&view, &surf->position);
    surf->geometry.normal = transform_view_apply_normal(&view, &surf->geometry.normal);
    surf->geometry.du = transform_view_apply_vector(&view, &surf->geometry.du);
    surf->geometry.dv = transform_view_apply_vector(&view, &surf->geometry.dv);

    surf->shading.normal = surf->geometry.normal;
}


KERNEL void surface_sample_sphere(
    const struct context *ctx,
    const struct object *obj,
    struct sample *sample
)
{
    float radius = 1.f;
    struct point origin = INIT_POINT(0, 0, 0);
    struct vector random_direction = sample_sphere_uniform(ctx);
    struct vector direction = vector_scale(&random_direction, radius);
    struct point position = point_add(&origin, &direction);


    sample->pos = position; // point_scale(&position, 1 / distance(&position, &origin));
    sample->direction = direction;
    sample->normal = INIT_VECTOR(position.value.x, position.value.y, position.value.z);
    sample->pdf = 1 / surface_area_sphere();
}

KERNEL float surface_pdf_sphere(
    const struct object* obj,
    const struct vector *wi,
    const struct vector *wo
)
{
    return 1 / surface_area_sphere();
}

KERNEL float surface_pdf(
        const object *obj,
        const transform *transform,
        const point *from,
        const vector *wi
)
{
    struct ray r = { *from, *wi };

    intersections_t xs = INIT_INTERSECTIONS;
    intersection_query_object(&r, obj, transform, &xs);

    struct intersection_t hit = {};
    if(!intersection_hit(&xs, &hit)) {
        return 0.f;
    }

    struct point p = ray_position(&r, hit.time);

    struct surface_t surf = {};
    surface_query(obj, transform, &p, &surf);

    struct vector dir = vector_negate(wi);
    float angle = abs(dot(&surf.geometry.normal, &dir));
    float dist = distance_sq(from, &surf.position);
    float area = surface_area(obj);
    float pdf =  dist / (angle * area);

    if(isinf(pdf)) {
        return 0.f;
    }

    return pdf;
}

KERNEL void surface_sample(
    const struct context *ctx,
    const struct object *obj,
    const struct point *from,
    struct sample *sample
)
{
    const struct transform *transform = &ctx->world->objects.transforms[obj->object_id];
    struct transform_view view = INIT_TRANSFORM(transform);
    view = transform_inverse(&view);

    switch (obj->type) {
        case sphere:
            surface_sample_sphere(ctx, obj, sample);
        case cube:
            break;
        case plane:
            break;
    }

    view = transform_inverse(&view);
    sample->pos = transform_view_apply_point(&view, &sample->pos);
    sample->normal = transform_view_apply_normal(&view, &sample->normal);

    // adjust pdf from probability to sample point in area
    // to probability of sampling the angle
    // struct vector wi = point_sub(&sample->pos, &from->position);
    if(scalar_eq(distance_sq(&sample->pos, from), 0)) {
        sample->pdf = 0;
        return;
    }

    sample->direction = point_sub(&sample->pos, from);
    sample->direction = vector_normalize(&sample->direction);

    struct vector wi = vector_negate(&sample->direction);
    sample->pdf *= distance_sq(from, &sample->pos) * abs(dot(&sample->normal, &wi));
    if(isinf(sample->pdf)) {
        sample->pdf = 0;
    }
}

#endif
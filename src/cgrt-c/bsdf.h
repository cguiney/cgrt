#pragma once

#include "distribution-functions/brdf_lambertian.h"
#include "distribution-functions/brdf_specular.h"
#include "distribution-functions/trig.h"
#include "interaction.h"
#include "kernel.h"
#include "sampling.h"

#define MAX_BXDF 16

enum bxdf_func
{
    FUNC_LAMBERTIAN_REFLECTION = 0,
    FUNC_SPECULAR_REFLECTION = 1,
};

/*
 * discriminated union of all b[rt]df functions
 */
struct bxdf
{
    enum bxdf_func func;
    /*
     * ideally this would be a union - it doesn't appear to work though.
     * it _might_ be due to an OpenCL restriction, but it seems like
     * vendors also have broken implementations as well.
     *
     * todo: rethink the memory access pattern.  it might have been
     * tolerable with a sum type.  a product type is undesirable.
     * for now, just making multiple bxdf functions play nicely together
     */
    struct
    {
        struct brdf_lambertian lambertian;
        struct brdf_specular specular;
    } input;
};

KERNEL float bxdf_pdf(
    const struct frame *frame,
    const struct bxdf *bxdf,
    const struct vector* wo,
    const struct vector* wi
)
{
    switch (bxdf->func) {
        case FUNC_LAMBERTIAN_REFLECTION:
            return brdf_lambertian_pdf(wo, wi);
        case FUNC_SPECULAR_REFLECTION:
            return brdf_specular_pdf(wo, wi);
    }
    return 0.f;
}


struct bsdf
{
    count_t count;
    struct vector ng;
    struct transform transform;
    struct bxdf bxdfs[MAX_BXDF];
};

KERNEL void bsdf_init(
    struct bsdf *init,
    const struct surface_t *surface,
    const material* material
)
{
    struct bsdf df = {};
    const struct vector ns = surface->shading.normal;
    const struct vector ss = vector_normalize(&surface->geometry.du);
    const struct vector ts = vector_cross(&ns, &ss);
    // const struct vector ts = surface->geometry.dv;

    df.ng = surface->geometry.normal;

    df.transform.matrices[0][0] = ss.value;
    df.transform.matrices[0][1] = ts.value;
    df.transform.matrices[0][2] = ns.value;
    df.transform.matrices[0][3] = {0.f, 0.f, 0.f, 0.f};

    df.transform.matrices[1][0] = { ss.value.x, ts.value.x, ns.value.x, 0};
    df.transform.matrices[1][1] = { ss.value.y, ts.value.y, ns.value.y, 0};
    df.transform.matrices[1][2] = { ss.value.z, ts.value.z, ns.value.z, 0};
    df.transform.matrices[1][3] = { 0, 0, 0, 0};

    switch (material->type) {
        case MATERIAL_MIRROR:
            df.bxdfs[df.count].func = FUNC_SPECULAR_REFLECTION;
            df.bxdfs[df.count].input.specular.f.type = FRESNEL_NOOP;
            df.bxdfs[df.count].input.specular.color = material->primary;
            break;

        case MATERIAL_LAMBERTIAN:
        case MATERIAL_DEFAULT:
        default:
            df.bxdfs[df.count].func = FUNC_LAMBERTIAN_REFLECTION;
            df.bxdfs[df.count].input.lambertian.color = material->primary;
    }

    df.count = 1;

    *init = df;
}

/*
KERNEL const struct bxdf* bsdf_select_random(
    const struct context *ctx,
    const struct bsdf *bsdf
)
{
    const struct bxdf* df = &bsdf->bxdfs[pcg32_boundedrand_r(ctx->rng, bsdf->count)];
    for(int i = 0; i < bsdf->count; i++)
    {

    }
    return 0;
}
*/

/*
 * bsdf_sample collects a random sampling from the combined
 * reflection and transmission distribution functions
 *
 * returns false if a sample was unable to be returned due things like
 * mathematical impossibility or undefinededness.
 */
KERNEL bool bsdf_sample(
    const struct context *ctx,
    const struct bsdf *bsdf,
    const struct vector *wo,
    struct scattering_sample *sample
)
{
    /*
     * transform wi into a bsdf local point using the bsdf transform matrix
     */
    struct vector xo = transform_matrix_apply_vector(&bsdf->transform.matrices[0], wo);

    if(xo.value.z == 0.f) {
        *sample = {};
        return false;
    }


    const struct bxdf* df = &bsdf->bxdfs[0];

    switch (df->func) {
        case FUNC_LAMBERTIAN_REFLECTION:
            brdf_lambertian_sample(ctx, &df->input.lambertian, &xo, sample);
            break;
        case FUNC_SPECULAR_REFLECTION:
            brdf_specular_sample(ctx, &df->input.specular, &xo, sample);
            break;
    }

    struct vector wi = transform_matrix_apply_vector(&bsdf->transform.matrices[1], &sample->reading.direction);

    // todo: randomly select a match df to use to get a direction and then re-sample all the matching ones
    // for now there's only one type, and it's reflective...so no need
    bool reflect = dot(&wi, &bsdf->ng) * dot(wo, &bsdf->ng) > 0;
    if(!reflect) {
        *sample = {};
        return false;
    }

    if(sample->reading.pdf == 0.f)
        return false;

    // if(sample->type != SPECULAR)
    // {
    //     for(size_t i = 0; i < bsdf->count; i++)
    //     {
    //         const bxdf* x = &bsdf->bsdfs[i];
    //         if(x == df)
    //             continue;
    //         sample->reading.pdf += bxdf_pdf(frame, x, &wi, &sample->reading.direction)
    //     }
    // }

    /*
     * because it's a unit vector, the matrix can be used as the inverse transposed, as well.
     * use it to transform the surface sample's output vector back to world space
     */
    sample->reading.direction = transform_matrix_apply_vector(&bsdf->transform.matrices[1], &sample->reading.direction);
    sample->reading.pos = transform_matrix_apply_point(&bsdf->transform.matrices[1], &sample->reading.pos);
    return true;
}

KERNEL struct spectrum bsdf_eval(
    const struct context *ctx,
    const struct bsdf *bsdf,
    const struct vector *wo,
    const struct vector *wi
)
{
    struct spectrum energy = INIT_SPECTRUM(0, 0, 0);

    struct vector xo = transform_matrix_apply_vector(&bsdf->transform.matrix, wo);
    if(xo.value.z == 0)
        return CGRT_COLOR_BLACK;

    bool reflect = dot(wi, &bsdf->ng) * dot(wo, &bsdf->ng) > 0;
    for (int i = 0; i < bsdf->count; i++) {
        const struct bxdf *df = &bsdf->bxdfs[i];

        if(reflect) {
            /*
             * todo, sum and average or something
             */
            switch (df->func) {
                case FUNC_LAMBERTIAN_REFLECTION:
                    energy = brdf_lambertian_eval(ctx, &df->input.lambertian);
                    break;
                case FUNC_SPECULAR_REFLECTION:
                    energy = brdf_specular_eval(ctx, &df->input.specular);
                    break;
                default:
                    continue;
            }
        }
    }

    return energy;
}

KERNEL float bsdf_pdf(
    const struct context *ctx,
    const struct bsdf *bsdf,
    const struct vector *wo,
    const struct vector *wi
) {
    struct vector xo = transform_matrix_apply_vector(&bsdf->transform.matrices[0], wo);
    struct vector xi = transform_matrix_apply_vector(&bsdf->transform.matrices[0], wi);

    if (xo.value.z == 0.f)
        return 0;

    float pdf = 0.f;
    int matches = 0;
    for (int i = 0; i < bsdf->count; i++) {
        const struct bxdf *df = &bsdf->bxdfs[i];
        switch (df->func) {
            case FUNC_LAMBERTIAN_REFLECTION:
                matches++;
                pdf += brdf_lambertian_pdf(&xo, &xi);
                break;
            case FUNC_SPECULAR_REFLECTION:
                matches++;
                pdf += brdf_specular_pdf(&xo, &xi);
                break;
        }
    }

    if(pdf == 0 || matches == 0) {
        return 0;
    }

    return pdf / (float)(matches);
}
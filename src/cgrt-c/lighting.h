#pragma once

#include "interaction.h"
#include "kernel.h"
#include "surface.h"

struct lighting_sample
{
    struct vector normal;
    struct sample reading;
    bool delta;
};

KERNEL struct spectrum lighting_object_emit(
    const struct context* ctx,
    const struct light_object *light,
    const struct vector* normal,
    const struct vector* wi
)
{
    if(dot(normal, wi) <= 0)
        return CGRT_COLOR_BLACK;
    return light->energy;
}


KERNEL void lighting_sample_point(
    const struct frame *frame,
    const struct interaction_t *ia,
    const struct light_point *light,
    struct lighting_sample *sample
)
{
    struct vector path = point_sub(&light->position, &ia->surface.position);
    float decay = 1 / vector_magnitude(&path);

    sample->reading.pos = light->position;
    sample->reading.direction = vector_normalize(&path);
    sample->reading.energy = spectrum_scale(&light->energy, decay);
    sample->reading.pdf = 1.f;
    sample->delta = true;
}

KERNEL void lighting_sample_environment(
    const struct frame *frame,
    const struct interaction_t* ia,
    const struct light_environment *light,
    struct lighting_sample *sample
)
{
    sample->delta = true;
}

KERNEL void lighting_sample_object(
    const struct context *ctx,
    const struct interaction_t *ia,
    const struct light_object *light,
    struct lighting_sample *sample
)
{
    struct object *obj = &ctx->world->objects.objects[light->object_id];
    surface_sample(ctx, obj, &ia->above, &sample->reading);

    struct vector direction = point_sub(&sample->reading.pos, &ia->above);
    if(vector_length(&direction) == 0) {
        sample->reading.pdf = 0;
        return;
    }
    sample->reading.direction = vector_normalize(&direction);

    struct vector wi = vector_negate(&sample->reading.direction);
    sample->reading.energy = lighting_object_emit(ctx, light, &sample->normal, &wi);
}

KERNEL void lighting_sample(
    const struct context *ctx,
    const struct interaction_t *ia,
    const struct light *light,
    struct lighting_sample *sample
)
{
    switch (light->type) {
        case OBJECT_LIGHT: {
            struct light_object obj = ctx->world->lights.objects[light->light_id];
            lighting_sample_object(ctx, ia, &obj, sample);
            break;
        }
        case POINT_LIGHT: {
            struct light_point point = ctx->world->lights.points[light->light_id];
            lighting_sample_point(ctx->frame, ia, &point, sample);
            break;
        }
        case ENVIRONMENT_LIGHT:
            break;
    }
}

KERNEL float lighting_pdf_object(
    const struct context *ctx,
    const struct light_object *obj,
    const struct point *from,
    const struct vector *wi
)
{
    const struct object* surface = &ctx->world->objects.objects[obj->object_id];
    const struct transform* transform = &ctx->world->objects.transforms[obj->object_id];
    switch (surface->type) {
        case sphere:
            return surface_pdf(surface, transform, from, wi);
            break;
        case cube:
            break;
        case plane:
            break;
    }
    return 0.f;
}

KERNEL float lighting_pdf(
    const struct context *ctx,
    const struct light *light,
    const struct point *from,
    const struct vector *wo
)
{
    switch (light->type) {
        case OBJECT_LIGHT: {
            struct light_object obj = ctx->world->lights.objects[light->light_id];
            return lighting_pdf_object(ctx, &obj, from, wo);
        }

        case POINT_LIGHT:
        case ENVIRONMENT_LIGHT:
            return 0.f;
    }
    return 0.f;
}

KERNEL bool lighting_is_singularity(const struct light *light)
{
    return light->type == POINT_LIGHT;
}
#include "kernel.h"

#include "intersect.h"
#include "interaction.h"
#include "shading.h"
#include "integration.h"

#ifdef __cplusplus
extern "C" {
#endif

KERNEL void unidirectional_trace(
    struct scene scene,

    // output & options
    struct spectrum *sums,
    int *samples,
    struct srgb *averages
)
{

    if (scene.frame.x >= scene.frame.width || scene.frame.y >= scene.frame.height) {
        return;
    }

    struct stats stats = {};
    scene.frame.stats = &stats;

    struct intersections_t xs = INIT_INTERSECTIONS;

    pcg32_random_t rng;
    pcg32_srandom_r(&rng, scene.frame.time * scene.frame.id * scene.frame.x * scene.frame.y, 0xda3e39cb94b95bdbUL + scene.frame.time);

    struct context ctx = {};
    ctx.frame = &scene.frame;
    ctx.world = &scene.world;
    ctx.xs = &xs;
    ctx.rng = &rng;

    spectrum sample = rtc_trace(&ctx);

    if (scene.frame.redraw) {
        sums[scene.frame.id] = sample;
        samples[scene.frame.id] = scene.frame.spp;
    } else {
        sums[scene.frame.id] = spectrum_add(&sums[scene.frame.id], &sample);
        samples[scene.frame.id] += scene.frame.spp;
    }

    float factor = 1.f / (float) (samples[scene.frame.id]);
    struct spectrum avg = spectrum_scale(&sums[scene.frame.id], factor);

    /*
     * convert from linear rgb to srgb
     */
    struct srgb v = spectrum_to_srgb(&avg);

    /*
     * clamp and convert for output
     */
    averages[scene.frame.id] = srgb_as_integers(&v);
}


#ifdef __cplusplus
};
#endif
#pragma once

#include "cgrt/kernel/pcg_basic.h"
#include "kernel.h"

struct sample
{
    struct point  pos;
    struct vector direction;
    struct vector normal;
    struct spectrum energy;
    float pdf;
};

enum scatter_type
{
    DIFFUSE,
    SPECULAR,
    REFRACTION
};

struct scattering_sample
{
    enum scatter_type type;
    struct sample reading;
};

KERNEL float sample_random_real(const struct context *ctx)
{
    ctx->frame->stats->rng_samples++;
    return ldexp((float) pcg32_random_r(ctx->rng), -32.0f);
}

KERNEL uvpoint sample_random_2d(const struct context *ctx)
{
    float u = sample_random_real(ctx);
    float v = sample_random_real(ctx);
    return uvpoint{u, v};
}

KERNEL struct uvpoint sample_disk_concentric(
    struct uvpoint rnd
)
{
    float u[2] = {
        (2*rnd.u) - 1,
        (2*rnd.v) - 1
    };

    if (u[0] == 0 && u[1] == 0) {
        return uvpoint{0, 0};
    }

    float theta, weight;
    if (fabs(u[0]) > fabs(u[1])) {
        weight = u[0];
        theta = CGRT_PI_4 * (u[1] / u[0]);
    } else {
        weight = u[1];
        theta = CGRT_PI_2 - CGRT_PI_4 * (u[0] / u[1]);
    }

    return uvpoint{
        weight * cosf(theta),
        weight * sinf(theta)
    };
}

KERNEL void sample_hemisphere_cosine(
    struct uvpoint rnd,
    struct vector *out
)
{
    struct uvpoint d = sample_disk_concentric(rnd);

    *out = INIT_VECTOR(
        d.u,
        d.v,
        sqrtf(fmaxf(0.f, 1.f - (d.u*d.u) - (d.v*d.v))));
}

KERNEL struct vector sample_hemisphere_uniform(const struct context  *ctx)
{
    float u[2] = {
        sample_random_real(ctx),
        sample_random_real(ctx),
    };

    float z = u[0];
    float r = sqrt(fmax(0.f, 1.f - (z * z)));
    float phi = 2 * CGRT_PI * u[1];
    return INIT_VECTOR(r * cos(phi), r * sin(phi), z);
}

KERNEL struct vector sample_sphere_uniform(const struct context *ctx)
{
    float u[2] = {
            sample_random_real(ctx),
            sample_random_real(ctx),
    };

    float z = 1 - 2 * u[0];
    float r = sqrt(fmax(0.f, 1.f - (z * z)));
    float phi = 2 * CGRT_PI * u[1];

    return INIT_VECTOR(r * cos(phi), r * sin(phi), z);
}
#pragma once
#ifndef CGRT_CAMERA_H
#define CGRT_CAMERA_H

#	include "algebra.h"
#	include "kernel.h"

#	include "cgrt/math/ray.hpp"
#	include "cgrt/cuda/cuda.hpp"
#	include "cgrt/objects/cameras.hpp"

KERNEL struct ray camera_generate_ray(const struct camera* c, float px, float py)
{
    float xoffset = c->pixel_size * (px + 0.5f);
    float yoffset = c->pixel_size * (py + 0.5f);

    float worldx = c->half_width - xoffset;
    float worldy = c->half_height - yoffset;

    const matrix4_t* xform = &c->transform.inverted;

    struct point target = INIT_POINT(worldx, worldy, -1.f);
    struct point origin = FILL_POINT(0);

    target = transform_matrix_apply_point(xform, &target);
    origin = transform_matrix_apply_point(xform, &origin);

    struct ray ray = {
        origin,
        point_sub(&target, &origin),
    };

    ray.dir = vector_normalize(&ray.dir);

    return ray;
}

namespace cgrt::kernel {
CGRT_KERNEL_FUNC inline math::ray<3> generate_ray(const objects::camera& c, math::scalar px, math::scalar py) {
	auto xoffset = c.pixel_size * (px + 0.5f);
	auto yoffset = c.pixel_size * (py + 0.5f);

	auto worldx = c.half_width - xoffset;
	auto worldy = c.half_height - yoffset;

	using math::transform::inverse;
	using math::normalize;
	using math::point;

	auto target = inverse(c.transform)(point<3>(worldx, worldy, -1.f));
	auto origin = inverse(c.transform)(point<3>(0.f));
	auto direction = normalize(target - origin);

	return {origin, direction};
}
};
#endif // CGRT_CAMERA_H
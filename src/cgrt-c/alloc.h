//
// Created by chris on 9/10/2022.
//

#ifndef CGRT_C_ALLOC_H
#define CGRT_C_ALLOC_H

#include <stdlib.h>

#ifdef CGRT_CUDA
inline void cgrt_alloc(void **ptr, size_t size)
{
	if(cudaMalloc(ptr, size))
		abort();
}
#else
inline void cgrt_alloc(void **ptr, size_t size)
{
	*ptr = calloc(sizeof(**ptr), size);
	if(!*ptr)
		abort();
}
#endif

#ifdef CGRT_CUDA
inline void cgrt_alloc(void **ptr, size_t size)
{
	if(cudaMalloc(ptr, size))
		abort();
}
#else
inline void cgrt_alloc(void **ptr, size_t size)
{
	*ptr = calloc(sizeof(**ptr), size);
	if(!*ptr)
		abort();
}
#endif



#endif //CGRT_C_ALLOC_H

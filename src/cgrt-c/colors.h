//
// Created by chris on 2/7/21.
//

#ifndef CGRT_COLORS_H
#define CGRT_COLORS_H

#include "kernel.h"

#if !CGRT_CUDA
static const struct spectrum CGRT_COLOR_RED   = INIT_SPECTRUM(1.f, 0.f, 0.f);
static const struct spectrum CGRT_COLOR_GREEN = INIT_SPECTRUM(0.f, 1.f, 0.f);
static const struct spectrum CGRT_COLOR_BLUE  = INIT_SPECTRUM(0.f, 0.f, 1.f);
static const struct spectrum CGRT_COLOR_BLACK = INIT_SPECTRUM(0.f, 0.f, 0.f);
static const struct spectrum CGRT_COLOR_WHITE = INIT_SPECTRUM(1.f, 1.f, 1.f);
#else
__device__ static const struct spectrum CGRT_COLOR_RED   = INIT_SPECTRUM(1.f, 0.f, 0.f);
__device__ static const struct spectrum CGRT_COLOR_GREEN = INIT_SPECTRUM(0.f, 1.f, 0.f);
__device__ static const struct spectrum CGRT_COLOR_BLUE  = INIT_SPECTRUM(0.f, 0.f, 1.f);
__device__ static const struct spectrum CGRT_COLOR_BLACK = INIT_SPECTRUM(0.f, 0.f, 0.f);
__device__ static const struct spectrum CGRT_COLOR_WHITE = INIT_SPECTRUM(1.f, 1.f, 1.f);
#endif

#define red   = CGRT_COLOR_RED;
#define green = CGRT_COLOR_GREEN;
#define blue  = CGRT_COLOR_BLUE;
#define black = CGRT_COLOR_BLACK;
#define white = CGRT_COLOR_WHITE;

KERNEL float gamma_correct(float linear_rgb)
{
    if(linear_rgb <= 0.0031308f)
        return 12.92f * linear_rgb;
    return 1.055f * powf(linear_rgb, 1.f/2.4f) - 0.055f;
}

KERNEL struct srgb spectrum_to_srgb(const struct spectrum* v) {
    return srgb{
        gamma_correct(v->channel.r),
        gamma_correct(v->channel.g),
        gamma_correct(v->channel.b),
        1.f,
    };
}

KERNEL struct srgb srgb_as_integers(const struct srgb *v) {
    return srgb{
        clamp(ceilf(v->r * 255.f), 0.f, 255.f),
        clamp(ceilf(v->g * 255.f), 0.f, 255.f),
        clamp(ceilf(v->b * 255.f), 0.f, 255.f),
        255.f
    };
}

#endif //CGRT_COLORS_H

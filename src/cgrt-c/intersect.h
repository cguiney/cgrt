#pragma once
#ifndef CGRT_KERNEL_INTERSECT_H
#define CGRT_KERNEL_INTERSECT_H

#	include "algebra.h"
#	include "kernel.h"

#ifdef __cplusplus
extern "C" {
#endif

#define INIT_INTERSECTIONS {0, 0, INTERSECTION_DEPTH, {}, {}}

struct intersections_t {
    count_t hits;
    count_t count;
    size_t cap;
    struct object objects[INTERSECTION_DEPTH];
    float times[INTERSECTION_DEPTH];
};

struct intersection_t {
    struct object obj;
    float time;
};

KERNEL void intersection_push(
        struct intersections_t *xs,
        const struct object *obj,
        float t
) {
    if ((size_t)(xs->count) >= xs->cap)
        return; // dont overflow stack!

    if (t > 0)
        xs->hits++;

    unsigned n = xs->count++;
    xs->objects[n] = *obj;
    xs->times[n] = t;
}

KERNEL void intersection_clear(
        struct intersections_t *xs
) {
    xs->count = 0;
    xs->hits = 0;
}

KERNEL bool intersection_hit(
        const struct intersections_t *xs,
        struct intersection_t *hit
) {
    int min = 0;
    bool found = false;
    for (int i = 0; i < xs->count; i++) {
        if (xs->times[i] < 0)
            continue;

        if (!found) {
            min = i;
            found = true;
            continue;
        }

        if (xs->times[i] < xs->times[min])
            min = i;
    }

    hit->obj = xs->objects[min];
    hit->time = xs->times[min];

    return found;
}

KERNEL void check_axis(float origin, float direction, float lower, float upper, float *min, float *max) {
    float tmin_numerator = lower - origin;
    float tmax_numerator = upper - origin;

    float a = fabsf(direction);
    if (a >= EPSILON) {
        float denom = 1 / direction;
        *min = tmin_numerator * denom;
        *max = tmax_numerator * denom;
    } else {
        *min = tmin_numerator * INFINITY;
        *max = tmax_numerator * INFINITY;
    }

    if (min > max) {
        float tmp = *min;
        *min = *max;
        *max = tmp;
    }
}

KERNEL bool intersection_query_cube(
        const struct point *low,
        const struct point *high,
        const struct ray *ray,
        float *t
) {
    float minx, miny, minz;
    float maxx, maxy, maxz;
    check_axis(ray->origin.value.x, ray->dir.value.x, low->value.x, high->value.x, &minx, &maxx);
    check_axis(ray->origin.value.y, ray->dir.value.y, low->value.y, high->value.y, &miny, &maxy);
    check_axis(ray->origin.value.z, ray->dir.value.z, low->value.z, high->value.z, &minz, &maxz);

    float tmin = fmaxf(fmaxf(minx, miny), minz);
    float tmax = fminf(fmaxf(maxx, maxy), maxz);

    if (tmin > tmax) {
        return false;
    }

    *t = tmin;
    return true;
}

KERNEL bool intersection_query_plane(
        const struct ray *r,
        const struct object *obj,
        struct intersections_t *xs
) {
    /*
     * ignore rays that're coplanar with the plane
     */
    if (fabsf(r->dir.value.z) < EPSILON) {
        return false;
    }

    float n = -r->origin.value.z;
    float d = r->dir.value.z;
    float t = n / d;

    intersection_push(xs, obj, t);
    return true;
}

KERNEL bool intersection_query_disk(
        const struct ray *r,
        const struct object *obj,
        struct intersections_t *xs
) {
    if (r->dir.value.z == 0) {
        return false;
    }

    float t = -r->origin.value.z / r->dir.value.z;
    if (t <= 0)
        return false;

    struct point p = ray_position(r, t);
    float distsq = p.value.x * p.value.x + p.value.y * p.value.y;
    if (distsq > 1 || distsq < 0)
        return false;

    float phi_max = 2 * CGRT_PI;
    float phi = atan2f(p.value.y, p.value.x);
    if (phi < 0)
        phi += 2 * CGRT_PI;
    if (phi > phi_max)
        return false;

    intersection_push(xs, obj, t);

    return true;
}


KERNEL bool intersection_query_sphere(
        const struct ray *r,
        const struct object *obj,
        struct intersections_t *xs
) {
    struct point origin = INIT_POINT(0.f, 0.f, 0.f);
    struct vector path = point_sub(&r->origin, &origin);

    float a = dot(&r->dir, &r->dir);
    float b = 2.f * dot(&r->dir, &path);
    float c = dot(&path, &path) - 1;

    float b2 = (b * b);
    float ac4 = (4.f * a * c);
    float disc = (b2) - (ac4);

    if (disc < 0.f)
        return false;

    intersection_push(xs, obj, (-b - sqrtf(disc)) / (2.f * a));
    intersection_push(xs, obj, (-b + sqrtf(disc)) / (2.f * a));

    return true;
}

KERNEL void intersection_query_object(
    const struct ray *r,
    const struct object * obj,
    const struct transform *transform,
    struct intersections_t *xs
)
{
    struct transform_view view = INIT_TRANSFORM(transform);
    view = transform_inverse(&view);

    struct ray xray = transform_view_apply_ray(&view, r);

    switch(obj->type)
    {
        case sphere:
            intersection_query_sphere(&xray, obj, xs);
            break;
        case plane:
            intersection_query_plane(&xray, obj, xs);
            break;
        case disk:
            intersection_query_disk(&xray, obj, xs);
            break;
        default: break;
    }
}

KERNEL bool intersection_query_objects(
    const struct ray*            r,
    const struct world*          world,
          struct intersections_t* xs
)
{
    for(
        int i = 0;
            i < world->objects.n_objects;
            i++
    )
    {
        const struct object *obj = &world->objects.objects[i];
        const struct transform *transform = &world->objects.transforms[i];
        intersection_query_object(r, obj, transform, xs);
    }

    return xs->hits > 0;
}

KERNEL bool intersection_query_bvh_object(
    const struct object* object,
    const struct bvh_leaf* node,
    const struct ray* ray,
    float *t
)
{
    float tt = 1e20;
    bool  intersect = false;

    switch(object->type)
    {
        case sphere:
        {
            // intersections = intersection_query_sphere(ray, &tt);
        }
        break;
        case cube:
        {
            struct point low  = INIT_POINT(-1, -1, -1);
            struct point high = INIT_POINT(-1, -1, -1);
            intersect = intersection_query_cube(&low, &high, ray, &tt);
        }
        break;
        case plane:
        break;
    }

    *t = tt;

    return intersect;
}

KERNEL bool intersection_query_bvh_node(
    const struct bvh_node* node,
    const struct ray* ray
)
{
    float tt = 1e20;
    struct bound bounds = node->bounds;
    return intersection_query_cube(&bounds.min, &bounds.max, ray, &tt);
}

struct bvh_frame {
    int node_id;
    int child_id;
};

KERNEL bool intersection_query_bvh(
    const struct world* world,
    const struct ray* ray,
    float* t,
    struct bvh_node* hit
)
{

    struct bvh_frame stack[64];         // traversal stack
    struct bvh_frame* sp = &stack[0];   // current stack frame

    struct bvh_frame* top = &stack[0] + 64; // book keeping -- address of top of stack
    bool found = false;                         // track if there's been an intersection

    const struct bvh* bvh = &world->bvh;

    sp->node_id = bvh->root; // start execution off from the tree root
    do {
        // guard against overflow
        if(sp >= top) return false;

        // intersects - examine node to either delve or do a final intersection test of an object
        switch(bvh->nodes[sp->node_id].type)
        {
            case root:
            {
                struct bvh_vertex* v = &bvh->vertices[bvh->nodes[sp->node_id].id];

                // first iteration - intersections with the bounding box
                // no need to do this on the leaf nodes, since they can just test against the primitive
                //
                // also ensure that this is only run for each parent node once, rather than each time the child's stack pops back to the parent
                if(sp->child_id == 0)
                {
                    // more likely case - not intersected pop stack and continue
                    if(!intersection_query_bvh_node(&bvh->nodes[sp->node_id], ray))
                    {
                        sp--; // pop the sack
                        continue;
                    }
                }

                if(sp->child_id >= v->edges.n) // base case - visited all children nodes, pop out
                {
                    sp--;
                    break;
                }

                // delve into child
                struct bvh_edge* e = &bvh->edges[v->edges.idx + sp->child_id++];

                // add to stack
                sp++;

                // initialize stack frame
                // be sure to initialize to zero - could have left over junk from previous iteration
                sp->node_id  = e->child;
                sp->child_id = 0;
            }
            break;

            case leaf:
            {
                struct bvh_leaf* v = &bvh->leafs[bvh->nodes[sp->node_id].id];
                struct object* object = &world->objects.objects[sp->node_id];
                // dispatch to the appropriate intersection test for the primitive
                // capture the intersection time in tt to determine if the intersection
                // is closer than any previously found intersection
                float tt = 0;
                if(intersection_query_bvh_object(object, v, ray, &tt))
                {
                    found = true;
                    if(tt < *t) {
                        *hit = bvh->nodes[sp->node_id];
                        *t = tt;
                    }
                }
                sp--;
            }
            break;
        }
    }
    /* continue as long as the topmost node is on the stack.
     *once that pops,the entire tree has been searched */
    while(sp >= &stack[0]);

    return found;
}

KERNEL bool intersection_query_scene(
    const struct context *ctx,
    const struct ray *ray
)
{
    /*
     * determine the distance between the interaction and the light.
     * if the distance of any intersection is less than this, the interaction is obscured by
     * something
     */
    float distance = vector_magnitude(&ray->dir);

    /*
     * Compute information for shadows
     * - fire a ray from the intersection point to the light
     * - if the ray intersects with an object before it reaches the light, it is shadowed by the light
     * - pass the shadowing info to the shader
     */
    struct ray path = *ray; /* declare shadow ray */

    /* use the normalized path as the ray direction */
    path.dir = vector_normalize(&path.dir);

    /*
     * clear the intersection buffer to ensure no past query results are used
     */
    intersection_clear(ctx->xs);

    /*
     * cast the shadow query
     */
    intersection_query_objects(&path, ctx->world, ctx->xs);

    /*
     * If there was an intersection, test if it was obstructive
     */
    bool obstructed = false;

    struct intersection_t hit = {};
    if(intersection_hit(ctx->xs, &hit)) {
        obstructed = hit.time < distance;
    }

    return obstructed;
}

#ifdef __cplusplus
};
#endif

#endif // CGRT_KERNEL_INTERSECT_H
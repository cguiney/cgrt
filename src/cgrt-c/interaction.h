#ifndef CGRT_INTERACTION_CLH
#define CGRT_INTERACTION_CLH

#include "algebra.h"
#include "bsdf.h"
#include "intersect.h"
#include "surface.h"

struct interaction_t
{
    struct ray input;
    struct intersection_t intersection;
    struct surface_t surface;

    /*
     * nudges above/below the interaction position
     * as determined by the surface normal
     */
    struct point above;
    struct point below;

    struct bsdf bsdf;
};

KERNEL void interact(
    const struct world* world,
    const struct intersection_t* x,
    const struct ray* input,
    struct interaction_t* ia
)
{
    ia->input = *input;
    ia->intersection = *x;
    /*
     * get position of the interaction
     */
    struct point position = ray_position(&ia->input, ia->intersection.time);

    /*
     * negate the input ray, to turn it from a camera ray to an eye ray
     * correctly orient normal with respect to eye (input) direction
     */
    ia->input.dir = vector_negate(&ia->input.dir);
    ia->input.dir = vector_normalize(&ia->input.dir);

    /*
     * with the interaction position, query the surface to get local geometry
     */
    const struct transform *t = &world->objects.transforms[x->obj.object_id];
    surface_query(&x->obj, t, &position, &ia->surface);

    /*
     * Set the surface normal to be the same direction as the eye ray
     */
    // ia->surface.geometry.normal = vector_forward(&ia->surface.geometry.normal, &ia->input.dir);
    // ia->surface.shading.normal = vector_forward(&ia->surface.shading.normal, &ia->input.dir);

    /*
     * with the normal computed, above/below points can be pre-computed as well
     */
    struct vector positive_nudge = vector_scale(&ia->surface.geometry.normal, EPSILON),
                  negative_nudge = vector_scale(&ia->surface.geometry.normal, -EPSILON);

    ia->above = point_add(&ia->surface.position, &positive_nudge);
    ia->below = point_add(&ia->surface.position, &negative_nudge);

    bsdf_init(&ia->bsdf, &ia->surface, &world->objects.materials[x->obj.object_id]);
}


#endif
#pragma once

#include "../kernel.h"
#include "trig.h"

struct brdf_microfacet {

};

inline float brdf_microfacet_pdf(
    const struct vector *wi,
    const struct vector *wo
)
{
    return 0.f;
}

inline void brdf_microfacet_eval(
    const struct frame *frame,
    const struct brdf_lambertian *lambertian,
    struct spectrum *energy
)
{

}

inline void brdf_microfacet_sample(
    const struct frame *frame,
    const struct brdf_microfacet *microfacet,
    const struct vector *wi,
    struct scattering_sample *sample
)
{

}
#pragma once
#ifndef CGRT_BRDF_LAMBERTIAN_H
#define CGRT_BRDF_LAMBERTIAN_H

#	include "../bsdf.h"
#	include "../kernel.h"
#	include "../sampling.h"

/*
 * reflection distribution function inputs
 */
struct brdf_lambertian
{
    struct spectrum color;
};

KERNEL float brdf_lambertian_pdf(
    const struct vector *wo,
    const struct vector *wi
)
{
    if (wo->value.z * wi->value.z > 0.f) {
        return bsdf_abs_cos_theta(wi) * CGRT_1_PI;
    }
    return 0.f;
}

KERNEL struct spectrum brdf_lambertian_eval(
    const struct context *ctx,
    const struct brdf_lambertian *lambertian
)
{
    return spectrum_scale(&lambertian->color, CGRT_1_PI);
}

KERNEL void brdf_lambertian_sample(
    const struct context *ctx,
    const struct brdf_lambertian *lambertian,
    const struct vector *wo,
    struct scattering_sample *sample
)
{
    struct vector wi = FILL_VECTOR(0);
    sample_hemisphere_cosine(sample_random_2d(ctx), &wi);

    if (wo->value.z < 0)
        wi.value.z = -wi.value.z;

    sample->reading.energy = brdf_lambertian_eval(ctx, lambertian);
    sample->reading.direction = wi;
    sample->reading.pdf = brdf_lambertian_pdf(wo, &wi);
}

#endif  // CGRT_BRDF_LAMBERTIAN_H

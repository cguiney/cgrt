#pragma once

#include "../algebra.h"
#include "../colors.h"
#include "../kernel.h"
#include "trig.h"

enum fresnel_type {
    FRESNEL_NOOP = 0,
    FRESNEL_SCHLICK = 1,
    FRESNEL_DIALECTRIC = 1
};

struct fresnel_noop {
    struct spectrum color;
};

struct fresnel {
     enum fresnel_type type;
};

KERNEL void fresnel_eval(const struct fresnel* f, float cos_theta_i, struct spectrum* color)
{
    switch (f->type) {
        case FRESNEL_NOOP:
            *color = CGRT_COLOR_WHITE;
        case FRESNEL_SCHLICK:
            break;
    }
}


struct brdf_specular {
    struct fresnel f;
    struct spectrum color;
};

KERNEL float brdf_specular_pdf(
    const struct vector *wi,
    const struct vector *wo
)
{
    return 0.f;
}

KERNEL struct spectrum brdf_specular_eval(
    const struct context *ctx,
    const struct brdf_specular *specular
)
{
    return CGRT_COLOR_BLACK;
}

KERNEL void brdf_specular_sample(
    const struct context *ctx,
    const struct brdf_specular *specular,
    const struct vector *wo,
    struct scattering_sample *sample
)
{
    sample->type = SPECULAR;
    sample->reading.direction = INIT_VECTOR(-wo->value.x, -wo->value.y, wo->value.z);
    sample->reading.pdf = 1.f;

    struct spectrum f = {};
    float cos_theta_o = bsdf_cos_theta(&sample->reading.direction);
    float abs_cos_theta_o = bsdf_abs_cos_theta(&sample->reading.direction);
    fresnel_eval(&specular->f, cos_theta_o, &f);
    sample->reading.energy = spectrum_scale(&specular->color, 1.f / abs_cos_theta_o);
}
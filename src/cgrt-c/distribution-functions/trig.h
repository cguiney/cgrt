#pragma once

#include "../kernel.h"

/*
 * accessors for bsdf coordinate system
 */

KERNEL float bsdf_cos_theta(const struct vector *w)
{
    return w->value.z;
}

KERNEL float bsdf_cos2_theta(const struct vector *w)
{
    return w->value.z * w->value.z;
}

KERNEL float bsdf_abs_cos_theta(const struct vector *w)
{
    return fabs(bsdf_cos_theta(w));
}


//
// Created by chris on 1/17/21.
//

#ifndef CGRT_SHADING_CLH
#define CGRT_SHADING_CLH

#include "algebra.h"
#include "bsdf.h"
#include "kernel.h"
#include "lighting.h"

KERNEL float power_heuristic(int nf, float fpdf, int ng, float gpdf)
{
    float f = (float)(nf) * fpdf;
    float g = (float)(ng)  * gpdf;
    return (f*f) / ((f * f) + (g * g));
}

KERNEL struct spectrum shade_lighting(
    const struct context* ctx,
    const struct interaction_t* ia,
    const struct light* light
)
{
    struct spectrum contribution = CGRT_COLOR_BLACK;
    struct lighting_sample ls = {};
    struct spectrum f = {};
    float weight = 0.f;
    float scattering_pdf = 0.f;

    lighting_sample(ctx, ia, light, &ls);
    if (spectrum_zero(&ls.reading.energy) || ls.reading.pdf <= 0.f) {
        return CGRT_COLOR_BLACK;
    }

    /* evaluate bsdf using the sampled path to the light's surface */
    f = bsdf_eval(ctx, &ia->bsdf, &ia->input.dir, &ls.reading.direction);
    scattering_pdf = bsdf_pdf(ctx, &ia->bsdf, &ia->input.dir, &ls.reading.direction);

    /* scale by the angle of the light from the surface normal */
    f = spectrum_scale(&f, abs(dot(&ls.reading.direction, &ia->surface.shading.normal)));

    /* if there is no contribution, early exit before the occlusion test */
    if(spectrum_zero(&f) || scattering_pdf <= 0) {
        return CGRT_COLOR_BLACK;
    }

    /* make sure that the light isn't actually occluded by another object */
    struct ray path = {
        ia->above,
        point_sub(&ls.reading.pos, &ia->above)
    };

    if (intersection_query_scene(ctx, &path)) {
        intersection_t hit = {};
        intersection_hit(ctx->xs, &hit);
        if(!hit.obj.is_light) {
            return CGRT_COLOR_BLACK;
        }
    }

    if (lighting_is_singularity(light)) {  /* incidence += light energy * scattered energy / pdf */
        contribution = spectrum_mul(&ls.reading.energy, &f);
        contribution = spectrum_scale(&contribution, 1 / ls.reading.pdf);
    } else { /* direct += light energy * scattered energy * weight / pdf */
        weight = power_heuristic(1, ls.reading.pdf, 1, scattering_pdf);
        contribution = spectrum_mul(&ls.reading.energy, &f);
        contribution = spectrum_scale(&contribution, weight);
        contribution = spectrum_scale(&contribution, (1 / ls.reading.pdf));
    }

    return contribution;
}

KERNEL struct spectrum shade_bsdf(
        const struct context* ctx,
        const struct interaction_t* ia,
        const struct light* light
)
{
    bool is_specular_sample = false;

    /* if it's a singularity, there's no real sampling to be done */
    if (lighting_is_singularity(light)) {
        return CGRT_COLOR_BLACK;
    }

    struct scattering_sample bs = {};
    bsdf_sample(ctx, &ia->bsdf, &ia->input.dir, &bs);

    struct spectrum f = bs.reading.energy;
    f = spectrum_scale(&f, abs(dot(&bs.reading.direction, &ia->surface.shading.normal)));

    if(spectrum_zero(&f) || bs.reading.pdf == 0.f) {
        return CGRT_COLOR_BLACK;
    }

    float weight = 1;

    /* there's no chance of the light sampling in the specular direction */
    is_specular_sample = !!(bs.type & SPECULAR);
    if(!is_specular_sample) {
        /* get the probability of the sampled surface direction being hit by the lighting object */
        float light_pdf = lighting_pdf(ctx, light, &ia->above, &bs.reading.direction);
        if(light_pdf <= 0) {
            /*
             * if there's no possibility that light could hit the sampled surface location,
             * there's no contribution to be had
             */
            return CGRT_COLOR_BLACK;
        }

        weight = power_heuristic(1, bs.reading.pdf, 1, light_pdf);
    }

    /*
     * do an intersection test to ensure that the path from the surface sample location to the light isn't
     * obstructed by any other object
     */
    struct ray path = {
        ia->above,
        bs.reading.direction
    };

    intersection_t hit = {};
    if(!intersection_query_objects(&path, ctx->world, ctx->xs)) {
        return CGRT_COLOR_BLACK;
    }

    intersection_hit(ctx->xs, &hit);
    if(hit.time > ia->intersection.time) {
        return CGRT_COLOR_BLACK;
    }

    /*
     * if the intersected object is not a light, there's no contribution to be had
     */
    light_object *li = &ctx->world->lights.objects[light->light_id];
    if(hit.obj.object_id != li->object_id) {
        return CGRT_COLOR_BLACK;
    }

    struct spectrum lighting = CGRT_COLOR_BLACK;
    switch (light->type) {
        case POINT_LIGHT:
            break;
        case OBJECT_LIGHT: {
            lighting = lighting_object_emit(ctx, li, &ia->surface.shading.normal, &bs.reading.direction);
            break;
        }
        case ENVIRONMENT_LIGHT:
            break;
    }

    if(spectrum_zero(&lighting)) {
        return CGRT_COLOR_BLACK;
    }

    struct spectrum contribution = CGRT_COLOR_BLACK;
    contribution = spectrum_mul(&f, &lighting);
    contribution = spectrum_scale(&contribution, weight);
    contribution = spectrum_scale(&contribution, 1 / bs.reading.pdf);

    return contribution;
}

KERNEL struct spectrum shade_direct(
    const struct context* ctx,
    const struct interaction_t* ia,
    const struct light* light
)
{
    struct spectrum lighting = CGRT_COLOR_BLACK, bsdf = CGRT_COLOR_BLACK;
    lighting = shade_lighting(ctx, ia, light);
    bsdf = shade_bsdf(ctx, ia, light);
    return spectrum_add(&lighting, &bsdf);
}




#endif //CGRT_SHADING_CLH

//
// Created by chris on 11/17/2020.
//
#pragma once
#ifndef CGRT_KERNEL_H
#define CGRT_KERNEL_H

#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

/*
 * Interrogate about the environment a bit
 */
#ifndef CGRT_CUDA
#ifdef __CUDACC__
#define CGRT_CUDA 1
#else
#define CGRT_CUDA 0
#endif
#endif

/*
 * Nvidia CUDA integration
 */
#if CGRT_CUDA
#define CGRT_CUDA_DEVICE_FUNC __device__
#else
#define CGRT_CUDA_DEVICE_FUNC
#endif

#if CGRT_CUDA
#define CGRT_CUDA_HOST_FUNC __host__
#else
#define CGRT_CUDA_HOST_FUNC
#endif

#define CGRT_GPU (CGRT_CUDA)
#define CGRT_CPU (!CGRT_GPU)

#define KERNEL CGRT_CUDA_DEVICE_FUNC inline


#define MAKE_POINT(p) (struct point){(p)}
#define INIT_POINT(x, y, z) (struct point){{(x), (y), (z), (1.f)}}
#define FILL_POINT(n) INIT_POINT(n, n, n);

#define MAKE_VECTOR(v) (struct vector){(v)}
#define INIT_VECTOR(x, y, z) (struct vector){{(x), (y), (z), 0.f}}
#define FILL_VECTOR(n) INIT_VECTOR(n, n, n)

#define MAKE_SPECTRUM(v) (struct spectrum){(v)}
#define INIT_SPECTRUM(x, y, z) (struct spectrum){{(x), (y), (z), 1.f}}
#define FILL_SPECTRUM(n) INIT_SPECTRUM(n, n, n)


/*
 * Math constants
 */
#define EPSILON 0.0001f
#define CGRT_E        2.7182818284590452354f   /* e */
#define CGRT_LOG2E    1.4426950408889634074f   /* log_2 e */
#define CGRT_LOG10E   0.43429448190325182765f  /* log_10 e */
#define CGRT_LN2      0.69314718055994530942f  /* log_e 2 */
#define CGRT_LN10     2.30258509299404568402f  /* log_e 10 */
#define CGRT_PI       3.14159265358979323846f  /* pi */
#define CGRT_PI_2     1.57079632679489661923f  /* pi/2 */
#define CGRT_PI_4     0.78539816339744830962f  /* pi/4 */
#define CGRT_1_PI     0.31830988618379067154f  /* 1/pi */
#define CGRT_2_PI     0.63661977236758134308f  /* 2/pi */
#define CGRT_2_SQRTPI 1.12837916709551257390f  /* 2/sqrt(pi) */
#define CGRT_SQRT2    1.41421356237309504880f  /* sqrt(2) */
#define CGRT_SQRT1_2  0.70710678118654752440f  /* 1/sqrt(2) */


/*
 * Rendering Constants
 */
#define INTERSECTION_DEPTH 32

typedef struct {
    union{
        float v[4];
        struct {
            float x;
            float y;
            float z;
            float w;
        };
    };
} tuple_t;

typedef tuple_t  matrix4_t[4]; /* dense 4x4 matrix type */
typedef float    matrix3_t[3][3]; /* dense 3x3 matrix type */
typedef float    matrix2_t[2][2]; /* dense 2x2 matrix type */
typedef unsigned identifier_t; /* auto incrementing identifier type */
typedef unsigned index_t;      /* index into an array */
typedef int      count_t;      /* count of objects */


struct stats {
    int rng_samples;
};

typedef struct pcg_state_setseq_64 pcg32_random_t;

struct frame {
    index_t id;
    index_t time;
    bool shadowing;
    bool redraw;
    int x;
    int y;
    int width;
    int height;
    int depth;
    int spp;

    struct stats* stats;
};

/*
 * index and count of objects.
 * used to point to a contiguous region of an array
 */

struct span {
    index_t idx;
    count_t n;
};

/*
 * safe vector3 types
 * help avoid incorrect math with the help of the compiler
 */

struct spectrum
{
    union
	{
        tuple_t value; // rgb
        struct {
            float r, g, b, a;
        } channel;
    };
};

struct srgb
{
    float r, g, b, a;
};

struct vector
{
    tuple_t value;
};

struct point
{
    tuple_t value;
};

/*
 * dense representation of a transformation matrix
 * transposed/inverted/inverted+transposed pre-calculated
 *
 * use transform_view to swizzle pointers when transposing/inverting a matrix
 */
struct transform
{
    union {
        struct {
            matrix4_t matrix;
            matrix4_t inverted;
            matrix4_t transposed;
            matrix4_t inv_transposed;
        };

        matrix4_t matrices[4];
    };
};

struct transform_view
{
    const struct transform* transform;
    index_t matrix;
    index_t inverted;
    index_t transposed;
    index_t inv_transposed;
};

struct bound
{
    struct point min;
    struct point max;
};

enum axis {
    x_axis = 0,
    y_axis = 1,
    z_axis = 2
};

struct ray {
    struct point  origin;
    struct vector dir;
};

enum object_type {
    sphere = 0,
    cube   = 1,
    plane  = 2,
    mesh   = 3,
    disk   = 4,
};

enum bvh_node_type {
    leaf,
    root
};

struct bvh_leaf {
    // index into object list
    index_t index;
};

struct bvh_edge {
    // indexes into node table
    index_t parent;
    index_t child;
};

struct bvh_vertex {
    identifier_t id;   // identity of this vertex
    struct span edges; // span into edges table
};

struct bvh_node {
    // type of bvh_node
    // either vertex or leaf
    enum bvh_node_type type;

    // this node's index in either the leaf or vertex list
    // i,e.
    // if type is root: state->bvh_vertices[node->id]
    // if type is leaf: state->bvh_tree_[node->id]
    index_t id;

    // AABB
    struct bound bounds;
};

struct bvh {
    index_t               root;       // root node of the tree tree
    count_t               n_nodes;    // count of current number of nodes
    struct bvh_node*      nodes;    // collection of all bvh_nodes
    count_t               n_vertices; // count of current number of root nodes
    struct bvh_vertex*    vertices; // collection of all roots/intermediaries
    count_t               n_edges;    // count of current number of root nodes
    struct bvh_edge*      edges;    // collection of all roots/intermediaries
    count_t               n_leafs;    // total number of leaf nodes
    struct bvh_leaf*      leafs;    // collection of all leaves
};

enum material_pattern {
    SOLID    = 0,
    CHECKERS = 1,
    THROB    = 2,
};

enum material_type {
    MATERIAL_DEFAULT = 0,
    MATERIAL_LAMBERTIAN = 1,
    MATERIAL_MIRROR = 2,
};

struct material {
    enum material_type type;
    enum material_pattern pattern;
    struct spectrum primary;
    struct spectrum secondary;
    float ambient;
    float diffuse;
    float specular;
    float shininess;
    float reflectiveness;
    float transparency;
    float refractive_index;
};

struct object {
    enum object_type type;
    identifier_t object_id;

    bool is_light;
    identifier_t light_id;
};

struct uvpoint {
    float u, v;
};

struct mesh_group {
    identifier_t object_id;  /* index into materials */
    index_t idx;             /* start triangle */
    count_t n;               /* number of triangles in group */
};

struct mesh {
    struct point*       vertices;
    struct vector*      normals;
    struct uvpoint*     textures;
    struct mesh_groups* groups;
};

struct objects {
    count_t           n_objects;
    struct object*    objects;    // collection of all spheres
    struct mesh*      meshes;
    struct transform* transforms;
    struct material*  materials;
};

struct light_point
{
    struct point    position;
    struct spectrum energy;
};

struct light_object
{
    identifier_t object_id;
    struct spectrum energy;
};

struct light_environment
{
    struct transform transform;
};

enum light_type {
    POINT_LIGHT,
    OBJECT_LIGHT,
    ENVIRONMENT_LIGHT
};

struct light {
    enum light_type type;
    identifier_t light_id;
};

struct lights {
    count_t n_lights;
    struct light* lights;

    count_t n_objects;
    struct light_object* objects;

    count_t n_points;
    struct light_point* points;
};

struct camera
{
    int width;
    int height;
    float fov;
    struct transform transform;

    // derived
    float half_view;
    float aspect;
    float half_width;
    float half_height;
    float pixel_size;
};

struct world {
    struct camera camera;
    struct bvh bvh;
    struct objects objects;
    struct lights lights;
};

struct scene {
    struct frame frame;
    struct camera camera;
    struct world world;
};

struct context
{
    const struct frame *frame;
    const struct world *world;

    struct intersections_t *xs;
    pcg32_random_t *rng;
};

#endif //CGRT_KERNEL_H
#ifndef CGRT_KERNEL_ALGEBRA_H
#define CGRT_KERNEL_ALGEBRA_H

#include "kernel.h"

#define INIT_TRANSFORM(p) {p, 0, 1, 2, 3}

/*
#if !CGRT_CUDA
#define rsqrt(x) (1.f/sqrtf(x))
#endif
*/

KERNEL float clamp(float x, float low, float high)
{
    if (x < low)
        return low;
    if (x > high)
        return high;
    return x;
}

KERNEL float radians(float d)
{
    return (CGRT_PI / 180.f) * clamp(d, 0.f, 360.f);
}

KERNEL float tdot(tuple_t lhs, tuple_t rhs)
{
    return (lhs.x * rhs.x) + (lhs.y * rhs.y) + (lhs.z * rhs.z) + (lhs.w * rhs.w);
}

KERNEL float vdot(const struct vector *lhs, const struct vector *rhs)
{
    return tdot(lhs->value, rhs->value);
}

KERNEL float pdot(const struct point *lhs, const struct point *rhs)
{
    return tdot(lhs->value, rhs->value);
}

KERNEL float dot(const struct vector *lhs, const struct vector *rhs)
{
    return vdot(lhs, rhs);
}

KERNEL bool scalar_eq(float x, float y)
{
    return fabs(x) - fabs(y) < EPSILON;
}

KERNEL struct vector vector_cross(
    const struct vector *lhs,
    const struct vector *rhs
)
{
    const float *lhs_ = lhs->value.v;
    const float *rhs_ = rhs->value.v;
    struct vector out = INIT_VECTOR(
            (lhs_[1] * rhs_[2]) - (lhs_[2] * rhs_[1]),
            (lhs_[2] * rhs_[0]) - (lhs_[0] * rhs_[2]),
            (lhs_[0] * rhs_[1]) - (lhs_[1] * rhs_[0])
    );

    return out;
}

KERNEL bool vector_eq(struct vector *lhs, struct vector *rhs)
{
    return (fabsf(lhs->value.v[0] - rhs->value.v[0]) < EPSILON &&
            fabsf(lhs->value.v[1] - rhs->value.v[1]) < EPSILON &&
            fabsf(lhs->value.v[2] - rhs->value.v[2]) < EPSILON);
}

KERNEL void vector_set(struct vector *out, tuple_t in)
{
    out->value.v[0] = in.v[0];
    out->value.v[1] = in.v[1];
    out->value.v[2] = in.v[2];
    out->value.v[3] = 0;
}

KERNEL struct vector vector_add(
    const struct vector *lhs,
    const struct vector *rhs
)
{
    struct vector out = INIT_VECTOR(
        lhs->value.v[0] + rhs->value.v[0],
        lhs->value.v[1] + rhs->value.v[1],
        lhs->value.v[2] + rhs->value.v[2]
    );

    return out;
}

KERNEL struct vector vector_sub(
    const struct vector *lhs,
    const struct vector *rhs
)
{
    struct vector out = INIT_VECTOR(
        lhs->value.v[0] - rhs->value.v[0],
        lhs->value.v[1] - rhs->value.v[1],
        lhs->value.v[2] - rhs->value.v[2]
    );

    return out;
}

KERNEL struct vector vector_negate(const struct vector *in)
{
    struct vector out = INIT_VECTOR(
        -in->value.v[0],
        -in->value.v[1],
        -in->value.v[2]
    );

    return out;
}

KERNEL struct vector vector_scale(
    const struct vector *lhs,
    float rhs
)
{
    struct vector out = INIT_VECTOR(
        lhs->value.v[0] * rhs,
        lhs->value.v[1] * rhs,
        lhs->value.v[2] * rhs
    );

    return out;
}

KERNEL struct vector vector_reflect(const struct vector *in, const struct vector *n)
{
    struct vector rhs = vector_scale(n, dot(in, n) * 2.f);
    return vector_sub(in, &rhs);
}

KERNEL struct vector vector_forward(const struct vector *in, const struct vector *n)
{
    if (dot(in, n) < 0)
        return vector_negate(in);
    return *in;
}

KERNEL float vector_length(const struct vector *in)
{
    return (in->value.x * in->value.x) + (in->value.y * in->value.y) +
           (in->value.z * in->value.z);
}

KERNEL float vector_magnitude(const struct vector *in)
{
    return sqrtf(vector_length(in));
}

KERNEL struct vector vector_normalize(const struct vector *in)
{
    return vector_scale(in, 1.f / vector_magnitude(in));
}

KERNEL void vector_coordinate_system(
    const struct vector *base,
    struct vector *x,
    struct vector *y
)
{
    float f = 0.f;
    if (fabsf(base->value.x) > fabsf(base->value.y)) {
        f = sqrtf((base->value.x * base->value.x) + (base->value.z * base->value.z));
        *x = INIT_VECTOR(-base->value.z, 0.f, base->value.x);
    } else {
        f = sqrtf((base->value.y * base->value.y) + (base->value.z * base->value.z));
        *x = INIT_VECTOR(0, base->value.z, -base->value.y);
    }

    *x = vector_scale(x, 1.f / f);
    *y = vector_cross(base, x);
}

KERNEL void point_set(struct point *p, tuple_t r)
{
    p->value.x = r.x;
    p->value.y = r.y;
    p->value.z = r.z;
    p->value.w = 1.f;
}

KERNEL bool point_eq(struct point *lhs, struct point *rhs)
{
    return (fabsf(lhs->value.v[0] - rhs->value.v[0]) < EPSILON &&
            fabsf(lhs->value.v[1] - rhs->value.v[1]) < EPSILON &&
            fabsf(lhs->value.v[2] - rhs->value.v[2]) < EPSILON);
}

KERNEL struct point point_add(
    const struct point *lhs,
    const struct vector *rhs
)
{
    struct point out = INIT_POINT(
        lhs->value.v[0] + rhs->value.v[0],
        lhs->value.v[1] + rhs->value.v[1],
        lhs->value.v[2] + rhs->value.v[2]);
    return out;
}

KERNEL struct vector point_sub(
    const struct point *lhs,
    const struct point *rhs
)
{
    struct vector out = INIT_VECTOR(
        lhs->value.v[0] - rhs->value.v[0],
        lhs->value.v[1] - rhs->value.v[1],
        lhs->value.v[2] - rhs->value.v[2]);
    return out;
}

KERNEL struct point point_scale(
        const struct point *lhs,
        float rhs
)
{
    struct point out = INIT_POINT(
            lhs->value.v[0] * rhs,
            lhs->value.v[1] * rhs,
            lhs->value.v[2] * rhs);
    return out;
}

KERNEL float distance(
    const struct point *from,
    const struct point *to
)
{
    struct vector x = point_sub(from, to);
    return vector_magnitude(&x);
}

KERNEL float distance_sq(
        const struct point *from,
        const struct point *to
)
{
    struct vector x = point_sub(from, to);
    return vector_length(&x);
}

KERNEL void spectrum_set(struct spectrum *s, tuple_t r)
{
    s->value.x = r.x;
    s->value.y = r.y;
    s->value.z = r.z;
    s->value.w = 1.f;
}

KERNEL struct spectrum spectrum_add(
    const struct spectrum *lhs,
    const struct spectrum *rhs
)
{
    struct spectrum out = INIT_SPECTRUM(
        lhs->value.v[0] + rhs->value.v[0],
        lhs->value.v[1] + rhs->value.v[1],
        lhs->value.v[2] + rhs->value.v[2]);

    return out;
}

KERNEL struct spectrum spectrum_scale(
    const struct spectrum *lhs,
    float f
)
{
    struct spectrum out = INIT_SPECTRUM(
        lhs->value.v[0] * f,
        lhs->value.v[1] * f,
        lhs->value.v[2] * f
    );

    return out;
}

KERNEL struct spectrum spectrum_mul(
    const struct spectrum *lhs,
    const struct spectrum *rhs
)
{
    struct spectrum out = INIT_SPECTRUM(
        lhs->value.v[0] * rhs->value.v[0],
        lhs->value.v[1] * rhs->value.v[1],
        lhs->value.v[2] * rhs->value.v[2]
    );

    return out;
}

KERNEL bool spectrum_eq(
    const struct spectrum *lhs,
    const struct spectrum *rhs
)
{
    return
        scalar_eq(lhs->value.v[0], rhs->value.v[0]) &&
        scalar_eq(lhs->value.v[1], rhs->value.v[1]) &&
        scalar_eq(lhs->value.v[2], rhs->value.v[2]);
}

KERNEL bool spectrum_zero(
    const struct spectrum *x
)
{
    struct spectrum zero = INIT_SPECTRUM(0, 0, 0);
    return spectrum_eq(x, &zero);
}

KERNEL bool spectrum_valid(
        const struct spectrum *x
)
{
    return !isinf(x->value.v[0])
           && !isinf(x->value.v[1])
           && !isinf(x->value.v[2]);
}


KERNEL struct transform_view transform_inverse(struct transform_view *v)
{
    struct transform_view inv = {
        v->transform,
        v->inverted,
        v->matrix,
        v->inv_transposed,
        v->transposed
    };

    return inv;
}

KERNEL struct transform_view transform_transpose(struct transform_view *v)
{
    struct transform_view inv = {
        v->transform,
        v->transposed,
        v->inv_transposed,
        v->matrix,
        v->inverted
    };

    return inv;
}

KERNEL struct point transform_matrix_apply_point(
    const matrix4_t *xform,
    const struct point *src
)
{
    const struct point *rhs = src;
    const struct point lhs[3] = {
        {(*xform)[0]},
        {(*xform)[1]},
        {(*xform)[2]}
    };

    struct point out = INIT_POINT(
        pdot(&lhs[0], rhs),
        pdot(&lhs[1], rhs),
        pdot(&lhs[2], rhs));

    return out;
}

KERNEL struct vector transform_matrix_apply_vector(
    const matrix4_t *xform,
    const struct vector *src
)
{
    const struct vector *rhs = src;
    const struct vector lhs[3] = {
        {(*xform)[0]},
        {(*xform)[1]},
        {(*xform)[2]}
    };

    struct vector out = INIT_VECTOR(
        vdot(&lhs[0], rhs),
        vdot(&lhs[1], rhs),
        vdot(&lhs[2], rhs));

    return out;
}

KERNEL struct point transform_view_apply_point(
    const struct transform_view *view,
    const struct point *src
)
{
    const matrix4_t *xform = &view->transform->matrices[view->matrix];

    const struct point *rhs = src;
    const struct point lhs[3] = {
        {(*xform)[0]},
        {(*xform)[1]},
        {(*xform)[2]}};

    struct point out = INIT_POINT(
        pdot(&lhs[0], rhs),
        pdot(&lhs[1], rhs),
        pdot(&lhs[2], rhs));

    return out;
}


KERNEL struct vector transform_view_apply_vector(
    const struct transform_view *view,
    const struct vector *src
)
{
    const matrix4_t *xform = &view->transform->matrices[view->matrix];

    const struct vector *rhs = src;
    const struct vector lhs[3] = {
        {(*xform)[0]},
        {(*xform)[1]},
        {(*xform)[2]}
    };

    struct vector out = INIT_VECTOR(
        vdot(&lhs[0], rhs),
        vdot(&lhs[1], rhs),
        vdot(&lhs[2], rhs));

    return out;
}

KERNEL struct vector transform_view_apply_normal(
        const struct transform_view *view,
        const struct vector *src
)
{
    const matrix4_t *xform = &view->transform->matrices[view->inv_transposed];

    const struct vector *rhs = src;
    const struct vector lhs[3] = {
            {(*xform)[0]},
            {(*xform)[1]},
            {(*xform)[2]}
    };

    struct vector out = INIT_VECTOR(
            vdot(&lhs[0], rhs),
            vdot(&lhs[1], rhs),
            vdot(&lhs[2], rhs));

    return vector_normalize(&out);
}

KERNEL struct ray transform_view_apply_ray(
    const struct transform_view *view,
    const struct ray *src
)
{
    struct ray out = {
        transform_view_apply_point(view, &src->origin),
        transform_view_apply_vector(view, &src->dir)
    };

    return out;
}

KERNEL struct point ray_position(const struct ray *r, float time)
{
    struct point out = INIT_POINT(
           (r->dir.value.v[0] * time) + r->origin.value.v[0],
           (r->dir.value.v[1] * time) + r->origin.value.v[1],
           (r->dir.value.v[2] * time) + r->origin.value.v[2]
    );

    return out;
}

#endif
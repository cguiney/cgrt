//
// Created by chris on 11/23/2020.
//

#ifndef CGRT_BVH_HPP
#define CGRT_BVH_HPP

#include "cgrt/containers.hpp"

#include <array>
#include <cassert>
#include <cgrt/math/geometry.hpp>
#include <cgrt/math/point.hpp>
#include <cgrt/objects/objects.hpp>
#include <limits>
#include <optional>
#include <utility>
#include <vector>

namespace cgrt::aggregates::bvh {
    enum node_type {
        LEAF,
        ROOT
    };

    struct leaf {
        // id into primitive array
       uint64_t index;
    };

    struct edge {
        // indexes into node table
        uint64_t parent;
        uint64_t child;
    };

    struct vertex {
        uint64_t id;    // identity of this vertex
        containers::span<edge>         edges; // span into edges table
    };

    struct bound
    {
        math::point<3> min;
        math::point<3> max;
    };

    struct node {
        // type of bvh_node
        // either vertex or leaf
        node_type type{};

        // this node's index in either the leaf or vertex list
        // i,e.
        // if type is root: state->bvh_vertices[node->id]
        // if type is leaf: state->bvh_tree_[node->id]
        uint64_t id{};

        // AABB
        bound bounds;
    };

    class bounding_box {
    public:
        bound bounds;
        int points = 0;

        using limits = typename std::numeric_limits<math::scalar::value_type>;

        bounding_box() : bounds({math::point<3>{-limits::infinity(), -limits::infinity(), -limits::infinity()},
                                 math::point<3>{ limits::infinity(),  limits::infinity(),  limits::infinity()}})
        {
        }

        bounding_box(math::point<3> min, math::point<3> max) : bounds({std::move(min), std::move(max)})
        {}

        inline math::vector<3> offset(const math::point<3>& p) const
        {
            auto o = p - bounds.min;

            return math::vector<3>(
            (bounds.max.x() > bounds.min.x()) ? o.x() : o.x() / (bounds.max.x() - bounds.min.x()),
            (bounds.max.y() > bounds.min.y()) ? o.x() : o.y() / (bounds.max.y() - bounds.min.y()),
            (bounds.max.z() > bounds.min.z()) ? o.x() : o.z() / (bounds.max.z() - bounds.min.z()));
        }

        inline math::point<3> centroid() const
        {
            return math::point<3>(
                    ((bounds.min.x() * math::scalar{0.5f}) + (bounds.max.x() * math::scalar{0.5f})),
                    ((bounds.min.y() * math::scalar{0.5f}) + (bounds.max.y() * math::scalar{0.5f})),
                    ((bounds.min.z() * math::scalar{0.5f}) + (bounds.max.z() * math::scalar{0.5f})));
        }

        inline math::vector<3> diagonal() const
        {
            return bounds.max - bounds.min;
        }

        inline math::scalar area() const
        {
            auto d = diagonal();
            return math::scalar{2.0f} * (d.x() * d.y() + d.x() * d.z() + d.y() * d.z());
        }

        inline math::axis max_extent() const
        {
            return math::dimension(diagonal());
        }

        inline void include(const math::point<3>& p)
        {
            if(!points) bounds = {p, p};
            else        bounds = {math::point<3>::component_min(bounds.min, p), math::point<3>::component_max(bounds.max, p)};
            points++;
        }

        inline void include(const bounding_box& o)
        {
            include(o.bounds.min);
            include(o.bounds.max);
        }

        inline void include(const struct bound& o)
        {
            include(o.min);
            include(o.max);
        }
    };

    inline bounding_box bounds(objects::object_type _)
    {

        return {math::point<3>{- 1.0f, - 1.0f, - 1.0f},
                math::point<3>{+ 1.0f, + 1.0f, + 1.0f}};
    }

    inline bounding_box bounds(const node &n)
    {
        return {n.bounds.min, n.bounds.max};
    }

    template<typename T>
    inline bounding_box bounds(const T begin, const T end)
    {
        bounding_box b{};
        std::for_each(begin, end, [&](const auto& x)
        {
            b.include(bounds(x));
        });
        return b;
    }

    template<typename RuntimeT>
    class bvh {
    public:
        uint64_t          root;
        containers::storage<RuntimeT, node>     nodes;
        containers::storage<RuntimeT, vertex>   tree_vertices;
        containers::storage<RuntimeT, edge>     tree_edges;
        containers::storage<RuntimeT, leaf>     leaf_nodes;
    };

    template<typename RuntimeT>
    class builder {
        int max_depth            = 12;
        size_t max_objects_per_node = 2;

        bvh<RuntimeT> tree;


        struct bucket {
            long count{};
            bounding_box bounds{};
        };

        inline uint64_t partition_sah(uint64_t begin, uint64_t count)
        {
            assert(tree.nodes.size() >= begin + count);

            constexpr const size_t n_buckets = 12;
            auto buckets = std::array<bucket, n_buckets>{};

            // in order to get the center of the space
            // collect the total bounds of the primitives
            bounding_box volume{};
            for(size_t i = begin; i < begin + count; i++)
            {
                auto node_bounds = bounding_box{tree.nodes[i].bounds.min, tree.nodes[i].bounds.max};
                volume.include(node_bounds.centroid());
            };

            auto dim = volume.max_extent();

            // second pass over the nodes in order to compute the area of each bucket
            for(size_t i = begin; i < begin + count; i++)
            {
                auto   centroid = volume.centroid();
                auto   offset   = volume.offset(centroid);
                auto   span     = offset[dim];
                size_t bucket   = n_buckets * int(span);

                if(bucket == n_buckets)
                    bucket = n_buckets - 1;

                assert(bucket > 0);
                assert(bucket < n_buckets);

                buckets[bucket].bounds.include(tree.nodes[i].bounds);
                buckets[bucket].count++;
            };


            auto costs = std::array<math::scalar, n_buckets - 1>{};
            for(size_t i = 0; i < n_buckets - 1; i++)
            {
                bucket lower{};
                bucket upper{};

                for(size_t j = 0; j <= i; j++)
                {
                    if(buckets[j].count > 0)
                        lower.bounds.include(buckets[j].bounds);
                    lower.count += buckets[j].count;
                }

                for(size_t j = i+1; j < n_buckets; j++)
                {
                    if(buckets[j].count > 0)
                        upper.bounds.include(buckets[j].bounds);

                    upper.count += buckets[j].count;
                }

                auto lower_product = lower.count == 0 ? math::scalar{0.f} : math::scalar{lower.count} * lower.bounds.area();
                auto upper_product = upper.count == 0 ? math::scalar{0.f} : math::scalar{upper.count} * upper.bounds.area();
                auto total_area = volume.area();
                auto constant = 0.125f;
                costs[i] = constant + (lower_product + upper_product) / total_area;
            }


            size_t min_cost        = (size_t)costs[0];
            size_t min_cost_bucket = 0;
            for(size_t i = 1; i < n_buckets - 1; i++)
            {
                if(costs[i] < min_cost)
                {
                    min_cost   = (size_t) costs[i];
                    min_cost_bucket = i;
                }
            }

            math::scalar leaf_cost = (float)(count);

            // do cost test to determine if it's worth partitioning
            // return end to indicate that all objects should be in the same partition
            if(tree.leaf_nodes.size() <= max_objects_per_node || min_cost < leaf_cost)
                return begin + count;

            std::sort(&tree.nodes[begin], &tree.nodes[begin + count], [&](const node& a, const node& b) -> bool
            {
                auto vol_a = bounds(a);
                auto vol_b = bounds(b);

                auto dim_a = static_cast<size_t>(volume.offset(vol_a.centroid())[dim]);
                auto dim_b = static_cast<size_t>(volume.offset(vol_b.centroid())[dim]);

                auto bucket_a = std::max(n_buckets * dim_a, n_buckets - 1);
                auto bucket_b = std::max(n_buckets * dim_b, n_buckets - 1);
                return bucket_a < bucket_b;
            });


            auto mid = std::lower_bound(&tree.nodes[begin], &tree.nodes[begin + count], min_cost_bucket,
                                        [&](const node& node, size_t bucket) -> bool
                                        {
                                            auto node_volume = bounds(node);
                                            auto node_dim = static_cast<size_t>(volume.offset(node_volume.centroid())[dim]);
                                            auto node_bucket = std::max(n_buckets * node_dim, n_buckets - 1);
                                            return node_bucket < bucket;
                                        });

            // convert midpoint pointer to offset
            return mid - &tree.nodes[begin];
        }

        uint64_t partition_equal(uint64_t begin, uint64_t count)
        {
            std::sort(&tree.nodes[begin], &tree.nodes[begin] + count, [](const auto& a, const auto&b)
            {
                return dimension(bounds(a).centroid()) < dimension(bounds(b).centroid());
            });

            return count / 2;
        }

        std::optional<uint64_t> build_leaf(uint64_t begin, uint64_t count, uint64_t depth)
        {
            bounding_box total_bounds = bounds(tree.nodes.begin(), tree.nodes.end());

            auto edges = containers::span<edge>{tree.tree_edges};
            vertex vertex{};
            vertex.id = tree.tree_vertices.size();
            vertex.edges = containers::span<edge>{edges.begin()+begin, count};

            node node{};
            node.type = ROOT;
            node.id   = vertex.id;
            node.bounds = total_bounds.bounds;

            size_t node_id = tree.nodes.size();
            for(size_t i = begin; i < begin + count; i++)
            {
                edge e{};
                e.parent = node_id;
                e.child  = i;
                tree.tree_edges.emplace_back(e);
            }

            tree.tree_vertices.emplace_back(vertex);
            tree.nodes.emplace_back(node);

            return node_id;
        }

        std::optional<uint64_t> build_tree(uint64_t begin, uint64_t count, uint64_t depth)
        {
            auto mid = partition_equal(begin, count); //partition_sah(begin, count);

            // delved too deep, nothing to do
            if(depth >= max_depth)
            {
                return std::nullopt;
            }

            // no nodes
            if(count == 0)
            {
                return std::nullopt;
            }

            // all one partition, nothing to do
            if(mid == begin + count)
            {
                mid = partition_equal(begin, count);
            }

            // evenly distributed - fall back to  partitioning in half
            // todo: pass the partitioning function down so building the tree
            // doesn't keep trying partition_sah before trying partition_equal
            if(mid == begin)
            {
                mid = partition_equal(begin, count);
            }

            assert(tree.nodes.size() >= begin + mid);
            assert(tree.nodes.size() >= begin + count);
            assert(mid <= begin + count);
            auto lower_bounds = bounds(&tree.nodes[begin], &tree.nodes[begin + mid]);
            auto upper_bounds = bounds(&tree.nodes[mid], &tree.nodes[begin] + count);

            // partition lower levels
            auto left  = build(begin, mid, depth + 1);
            auto right = build(mid, count, depth + 1);

            bounding_box total_bounds{};
            total_bounds.include(lower_bounds);
            total_bounds.include(upper_bounds);

            uint32_t node_id = tree.nodes.size();
            uint32_t edge_id = tree.tree_edges.size();
            uint32_t n = 0;
            // record left & right vertices
            // note, they may not have created a left/right branch
            // if the partitioning is leaning 100% left or right
            if(left) {
                n++;
                edge e{};
                e.parent = node_id;
                e.child  = *left;
                tree.tree_edges.emplace_back(e);
            }

            if(right) {
                n++;
                edge e{};
                e.parent = node_id;
                e.child  = *right;
                tree.tree_edges.emplace_back(e);
            }

            auto edges = containers::span<edge>{tree.tree_edges};
            vertex vertex{};
            vertex.id = tree.tree_vertices.size();
            vertex.edges = containers::span<edge>{edges.begin()+edge_id, n};
            tree.tree_vertices.emplace_back(vertex);

            node node{};
            node.type   = ROOT;
            node.id     = vertex.id;
            node.bounds = total_bounds.bounds;
            tree.nodes.emplace_back(node);

            return node_id;
        }

        std::optional<uintptr_t> build(ptrdiff_t begin, size_t count, size_t depth)
        {
            if(count <= max_objects_per_node)
                return build_leaf(begin, count, depth);
            else
                return build_tree(begin, count, depth);
        }

    public:
        inline class bvh<RuntimeT> build()
        {
            tree.root = build(0, tree.nodes.size(), 0).value_or(0);
            return tree;
        }

        inline void add(size_t id, const bounding_box& b)
        {
            leaf leaf_node{};
            leaf_node.index = id;

            node node{};
            node.type = LEAF;
            node.id = tree.leaf_nodes.size();
            node.bounds = b.bounds;

            tree.leaf_nodes.emplace_back(leaf_node);
            tree.nodes.emplace_back(node);
        }
    };
}

#endif //CGRT_BVH_HPP

//
// Created by chris on 10/28/2021.
//

#ifndef CGRT_UI_VIEWPORT_HPP
#define CGRT_UI_VIEWPORT_HPP

class texture {
    std::mutex mu;
    std::unique_ptr<cgrt::img::canvas> ready;
    std::unique_ptr<cgrt::img::canvas> loading;
    nanogui::ref<nanogui::Texture> display;
protected:
public:
    texture(int width, int height) :
            ready(std::make_unique<cgrt::img::canvas>(width, height)),
            loading(std::make_unique<cgrt::img::canvas>(width, height)),
            display(new nanogui::Texture(
                    nanogui::Texture::PixelFormat::RGBA,
                    nanogui::Texture::ComponentFormat::UInt8,
                    nanogui::Vector2i{width, height},
                    nanogui::Texture::InterpolationMode::Nearest,
                    nanogui::Texture::InterpolationMode::Nearest
            ))
    {
    }



    [[nodiscard]]
    nanogui::ref<nanogui::Texture> active() const {
        return display;
    }

};

class viewport : nanogui::Window {
    nanogui::ref<nanogui::ImageView> view_;
    nanogui::ref<nanogui::Texture> display_;

    std::mutex mu;
    std::unique_ptr<cgrt::img::canvas> ready_;
    std::unique_ptr<cgrt::img::canvas> loading_;
    unsigned host_version = 0;
    unsigned host_iteration = 0;
    unsigned gpu_version = 0;
    unsigned gpu_iteration = 0;
public:
    viewport(nanogui::Widget* parent, int width, int height) :
        nanogui::Window(parent, "Viewport"),
        view_(new nanogui::ImageView{this}),
        display_(new nanogui::Texture(
            nanogui::Texture::PixelFormat::RGBA,
            nanogui::Texture::ComponentFormat::UInt8,
            nanogui::Vector2i{width, height},
            nanogui::Texture::InterpolationMode::Nearest,
            nanogui::Texture::InterpolationMode::Nearest
        )),
        ready_(std::make_unique<cgrt::img::canvas>(width, height)),
        loading_(std::make_unique<cgrt::img::canvas>(width, height))
    {
        set_size(nanogui::Vector2i(width, height));
        set_layout(new nanogui::GroupLayout());

        view_->set_image(display_);
        view_->set_size(nanogui::Vector2i(width, height));
    }


    std::unique_ptr<cgrt::img::canvas> update(std::unique_ptr<cgrt::img::canvas> c, unsigned version, unsigned iteration) {
        // give the loading back to the caller so it can fill it in again
        std::lock_guard<std::mutex> lg(mu);
        std::swap(loading_, c);
        host_version = version;
        host_iteration = iteration;
        return c;
    }

    bool reload() {
        /*
         * acquire the mutex real quick to check if the gpu version needs to be updated
         * if it does, swap out the ready canvas for the loading canvas and release before
         * starting the copy to the gpu
         *
         * if the versions match, there's no need to do the swap
         * todo: have a fast path atomic check?
         */
        unsigned new_version = 0;
        unsigned new_iteration = 0;
        {
            std::lock_guard<std::mutex> lg(mu);
            new_version = host_version;
            new_iteration = host_iteration;

            if (new_iteration > gpu_iteration)
                std::swap(ready_, loading_);
        }

        // second check while not under lock for the actual upload
        bool update = new_iteration > gpu_iteration;
        if(update)
        {
            display_->upload(ready_->storage().data());
        }


        //  update the gpu version
        gpu_version = new_version;
        gpu_iteration = new_iteration;

        return update;
    }

    void publish()
    {
        if(reload())
            screen()->redraw();
        view_->set_image(display_);
    }
};

#endif //CGRT_UI_VIEWPORT_HPP

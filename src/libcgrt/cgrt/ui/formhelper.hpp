//
// Created by chris on 10/29/2021.
//

#ifndef CGRT_FORMHELPER_HPP
#define CGRT_FORMHELPER_HPP

#include <nanogui/nanogui.h>

namespace cgrt::ui::form {
/**
 * \class FormHelper formhelper.h nanogui/formhelper.h
 *
 * \brief Convenience class to create simple AntTweakBar-style layouts that
 *        expose variables of various types using NanoGUI widgets
 *
 * **Example**:
 *
 * \rst
 * .. code-block:: cpp
 *
 *    // [ ... initialize NanoGUI, construct screen ... ]
 *
 *    FormHelper* h = new FormHelper(screen);
 *
 *    // Add a new windows widget
 *    h->add_window(Vector2i(10,10),"Menu");
 *
 *    // Start a new group
 *    h->add_group("Group 1");
 *
 *    // Expose an integer variable by reference
 *    h->add_variable("integer variable", a_int);
 *
 *    // Expose a float variable via setter/getter functions
 *    h->add_variable(
 *      [&](float value) { a_float = value; },
 *      [&]() { return *a_float; },
 *      "float variable");
 *
 *    // add a new button
 *    h->add_button("Button", [&]() { std::cout << "Button pressed" << std::endl; });
 *
 * \endrst
 */
    using Screen = nanogui::Screen;
    using Window = nanogui::Window;
    using Widget = nanogui::Widget;
    using Label = nanogui::Label;
    using Button = nanogui::Button;
    using Vector2i = nanogui::Vector2i;
    using AdvancedGridLayout = nanogui::AdvancedGridLayout;

    template<typename T>
    using ref = nanogui::ref<T>;

    namespace detail
    {
        template<typename T>
        using FormWidget = nanogui::detail::FormWidget<T>;
    }

    class FormHelper {
    public:
        /// Create a helper class to construct NanoGUI widgets on the given screen
        FormHelper(Screen *screen, Widget* parent) : m_screen(screen), m_parent(parent)
        {
            if(auto layout = dynamic_cast<AdvancedGridLayout*>(m_parent->layout()); layout != nullptr) {
                m_layout = layout;
            } else {
                m_layout = new AdvancedGridLayout({10, 0, 10, 0}, {});
                m_layout->set_margin(10);
                m_layout->set_col_stretch(2, 1);
            }

            m_parent->set_layout(m_layout);
        }

        /// Add a new top-level window
        Window *add_window(const Vector2i &pos,
                           const std::string &title = "Untitled") {
            assert(m_screen);
            Window* m_window{};
            m_window = new Window(m_screen, title);
            m_layout = new AdvancedGridLayout({10, 0, 10, 0}, {});
            m_layout->set_margin(10);
            m_layout->set_col_stretch(2, 1);
            m_window->set_position(pos);
            m_window->set_layout(m_layout);
            m_window->set_visible(true);
            return m_window;
        }

        /// Add a new group that may contain several sub-widgets
        Label *add_group(const std::string &caption) {
            Label* label = new Label(m_parent, caption, m_group_font_name, m_group_font_size);
            if (m_layout->row_count() > 0)
                m_layout->append_row(m_pre_group_spacing); /* Spacing */
            m_layout->append_row(0);
            m_layout->set_anchor(label, AdvancedGridLayout::Anchor(0, m_layout->row_count()-1, 4, 1));
            m_layout->append_row(m_post_group_spacing);
            return label;
        }

        /// Add a new data widget controlled using custom getter/setter functions
        template <typename Type> detail::FormWidget<Type> *
        add_variable(const std::string &label, const std::function<void(const Type &)> &setter,
                     const std::function<Type()> &getter, bool editable = true) {
            Label *label_w = new Label(m_parent, label, m_label_font_name, m_label_font_size);
            auto widget = new detail::FormWidget<Type>(m_parent);
            auto refresh = [widget, getter] {
                Type value = getter(), current = widget->value();
                if (value != current)
                    widget->set_value(value);
            };
            refresh();
            widget->set_callback(setter);
            widget->set_editable(editable);
            widget->set_font_size(m_widget_font_size);
            Vector2i fs = widget->fixed_size();
            widget->set_fixed_size(Vector2i(fs.x() != 0 ? fs.x() : m_fixed_size.x(),
                                            fs.y() != 0 ? fs.y() : m_fixed_size.y()));
            m_refresh_callbacks.push_back(refresh);
            if (m_layout->row_count() > 0)
                m_layout->append_row(m_variable_spacing);
            m_layout->append_row(0);
            m_layout->set_anchor(label_w, AdvancedGridLayout::Anchor(1, m_layout->row_count()-1));
            m_layout->set_anchor(widget, AdvancedGridLayout::Anchor(3, m_layout->row_count()-1));
            return widget;
        }

        /// Add a new data widget that exposes a raw variable in memory
        template <typename Type> detail::FormWidget<Type> *
        add_variable(const std::string &label, Type &value, bool editable = true) {
            return add_variable<Type>(label,
                                      [&](const Type & v) { value = v; },
                                      [&]() -> Type { return value; },
                                      editable
            );
        }

        /// Add a button with a custom callback
        Button *add_button(const std::string &label, const std::function<void()> &cb) {
            Button *button = new Button(m_parent, label);
            button->set_callback(cb);
            button->set_fixed_height(25);
            if (m_layout->row_count() > 0)
                m_layout->append_row(m_variable_spacing);
            m_layout->append_row(0);
            m_layout->set_anchor(button, AdvancedGridLayout::Anchor(1, m_layout->row_count()-1, 3, 1));
            return button;
        }

        /// Add an arbitrary (optionally labeled) widget to the layout
        void add_widget(const std::string &label, Widget *widget) {
            m_layout->append_row(0);
            if (label == "") {
                m_layout->set_anchor(widget, AdvancedGridLayout::Anchor(1, m_layout->row_count()-1, 3, 1));
            } else {
                Label *label_w = new Label(m_parent, label, m_label_font_name, m_label_font_size);
                m_layout->set_anchor(label_w, AdvancedGridLayout::Anchor(1, m_layout->row_count()-1));
                m_layout->set_anchor(widget, AdvancedGridLayout::Anchor(3, m_layout->row_count()-1));
            }
        }

        /// Cause all widgets to re-synchronize with the underlying variable state
        void refresh() {
            for (auto const &callback : m_refresh_callbacks)
                callback();
        }

        /// Access the currently active \ref Window instance
        Widget *widget() { return m_parent; }

        /// Set the active \ref Window instance.
        void set_widget(Widget *widget) {
            m_parent = widget;
            m_layout = dynamic_cast<AdvancedGridLayout *>(widget->layout());
            if (m_layout == nullptr)
                throw std::runtime_error(
                        "Internal error: window has an incompatible layout!");
        }

        /// Specify a fixed size for newly added widgets
        void set_fixed_size(const Vector2i &fw) { m_fixed_size = fw; }

        /// The current fixed size being used for newly added widgets.
        Vector2i fixed_size() { return m_fixed_size; }

        /// The font name being used for group headers.
        const std::string &group_font_name() const { return m_group_font_name; }

        /// Sets the font name to be used for group headers.
        void set_group_font_name(const std::string &name) { m_group_font_name = name; }

        /// The font name being used for labels.
        const std::string &label_font_name() const { return m_label_font_name; }

        /// Sets the font name being used for labels.
        void set_label_font_name(const std::string &name) { m_label_font_name = name; }

        /// The size of the font being used for group headers.
        int group_font_size() const { return m_group_font_size; }

        /// Sets the size of the font being used for group headers.
        void set_group_font_size(int value) { m_group_font_size = value; }

        /// The size of the font being used for labels.
        int label_font_size() const { return m_label_font_size; }

        /// Sets the size of the font being used for labels.
        void set_label_font_size(int value) { m_label_font_size = value; }

        /// The size of the font being used for non-group / non-label widgets.
        int widget_font_size() const { return m_widget_font_size; }

        /// Sets the size of the font being used for non-group / non-label widgets.
        void set_widget_font_size(int value) { m_widget_font_size = value; }

    protected:
        /// A reference to the \ref nanogui::Screen this FormHelper is assisting.
        ref<Screen> m_screen;
        /*
         * Replaced by m_parent to support building an arbitrary form
         * /// A reference to the \ref nanogui::Window this FormHelper is controlling.
         * ref<Window> m_window;
         */
        /// A reference to the \ref nanogui::Widget this FormHelper is controlling.
        ref<Widget> m_parent;
        /// A reference to the \ref nanogui::AdvancedGridLayout this FormHelper is using.
        ref<AdvancedGridLayout> m_layout;
        /// The callbacks associated with all widgets this FormHelper is managing.
        std::vector<std::function<void()>> m_refresh_callbacks;
        /// The group header font name.
        std::string m_group_font_name = "sans-bold";
        /// The label font name.
        std::string m_label_font_name = "sans";
        /// The fixed size for newly added widgets.
        Vector2i m_fixed_size = Vector2i(0, 20);
        /// The font size for group headers.
        int m_group_font_size = 20;
        /// The font size for labels.
        int m_label_font_size = 16;
        /// The font size for non-group / non-label widgets.
        int m_widget_font_size = 16;
        /// The spacing used **before** new groups.
        int m_pre_group_spacing = 15;
        /// The spacing used **after** each group.
        int m_post_group_spacing = 5;
        /// The spacing between all other widgets.
        int m_variable_spacing = 5;
    };
}

#endif //CGRT_FORMHELPER_HPP

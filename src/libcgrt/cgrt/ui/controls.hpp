//
// Created by chris on 10/28/2021.
//
#pragma once
#ifndef CGRT_CONTROLS_HPP
#	define CGRT_CONTROLS_HPP

#	include <cgrt/ui/formhelper.hpp>
#	include <cgrt/ui/state.hpp>
#	include <nanogui/nanogui.h>

#include <fmt/format.h>

namespace cgrt::ui {

using checkbox_widget = nanogui::detail::FormWidget<bool>;
using color_widget = nanogui::detail::FormWidget<nanogui::Color>;

nanogui::Color from_spectrum(const math::spectrum& s) {
	return nanogui::Color{s.x(), s.y(), s.z(), 255.f};
}

math::spectrum to_spectrum(const nanogui::Color& c) {
	return math::spectrum{c.r(), c.g(), c.b()};
}

class object_panel : public nanogui::Window {
	template<typename T>
	using object_map = std::unordered_map<objects::identifier<objects::object>, T>;

    class transform : public nanogui::Widget {
        std::array<nanogui::ref<nanogui::Label>, 4> top_labels;
        std::array<nanogui::ref<nanogui::Label>, 4> left_labels;
        std::array<nanogui::ref<nanogui::FloatBox<float>>, 16> cells;
      public:
        inline explicit transform(nanogui::Widget* w) noexcept : nanogui::Widget(w) {
            auto layout = new nanogui::AdvancedGridLayout();
            layout->set_margin(2);
            layout->append_col(5, 1.0);
            layout->append_col(5, 1.0);
            layout->append_col(5, 1.0);
            layout->append_col(5, 1.0);
            layout->append_col(5, 1.0);

            layout->append_row(5, 1.0);
            top_labels[0] = new nanogui::Label{this, "X"};
            top_labels[1] = new nanogui::Label{this, "Y"};
            top_labels[2] = new nanogui::Label{this, "Z"};
            top_labels[3] = new nanogui::Label{this, "W"};
            for(size_t i = 0; i < 4; i++) {
                layout->set_anchor(
                    top_labels[i],
                    nanogui::AdvancedGridLayout::Anchor{
                        static_cast<int>(i+1), 0,
                        nanogui::Alignment::Maximum,
                        nanogui::Alignment::Maximum
                    });
            }

            left_labels[0] = new nanogui::Label{this, "X"};
            left_labels[1] = new nanogui::Label{this, "Y"};
            left_labels[2] = new nanogui::Label{this, "Z"};
            left_labels[3] = new nanogui::Label{this, "Z"};
            for(size_t i = 0; i < 4; i++) {
                layout->set_anchor(
                    left_labels[i],
                    nanogui::AdvancedGridLayout::Anchor{
                        0, static_cast<int>(i+1),
                        nanogui::Alignment::Maximum,
                        nanogui::Alignment::Maximum});
            }


            layout->append_row(5, 1.0);
            cells[0] = new nanogui::FloatBox<float>(this);
            cells[1] = new nanogui::FloatBox<float>(this);
            cells[2] = new nanogui::FloatBox<float>(this);
            cells[3] = new nanogui::FloatBox<float>(this);

            layout->append_row(5, 1.0);
            cells[4] = new nanogui::FloatBox<float>(this);
            cells[5] = new nanogui::FloatBox<float>(this);
            cells[6] = new nanogui::FloatBox<float>(this);
            cells[7] = new nanogui::FloatBox<float>(this);


            layout->append_row(5, 1.0);
            cells[8] = new nanogui::FloatBox<float>(this);
            cells[9] = new nanogui::FloatBox<float>(this);
            cells[10] = new nanogui::FloatBox<float>(this);
            cells[11] = new nanogui::FloatBox<float>(this);

            layout->append_row(5, 1.0);
            cells[12] = new nanogui::FloatBox<float>(this);
            cells[13] = new nanogui::FloatBox<float>(this);
            cells[14] = new nanogui::FloatBox<float>(this);
            cells[15] = new nanogui::FloatBox<float>(this);

            for(size_t y = 0; y < 4; y++) {
                for(size_t x = 0; x < 4; x++) {
                    auto anchor = nanogui::AdvancedGridLayout::Anchor{1+static_cast<int>(x), 1+static_cast<int>(y)};
                    layout->set_anchor(cells[y*4+x], anchor);
                }
            }

            set_layout(layout);
        }

        inline void set_value(const math::matrix<4, 4>& mtx) noexcept {
            for(size_t y = 0; y < 4; y++) {
                for(size_t x = 0; x < 4; x++) {
                    cells[y*4+x]->set_value(mtx[y][x]);
                }
            }
        }
    };

	class material : public nanogui::Widget {
		std::vector<nanogui::ref<nanogui::Label>> labels_;
		nanogui::ref<color_widget> primary{};

	  public:
		material(nanogui::Widget* w, const state_handle* state, objects::identifier<objects::object> id)
		    : nanogui::Widget(w), primary(new color_widget{this}) {
			primary->set_value(from_spectrum(state->view().materials.at(id).primary));
			primary->set_final_callback([state, id](const nanogui::Color& c) {
				auto material = state->view().materials.at(id);
				material.primary = math::spectrum(c.r(), c.g(), c.b());
				state->update([id, material](scene::builder& b) {
					b.object(id).use_material(material).finish();
				});
			});

			auto gui = form::FormHelper(screen(), this);
			gui.add_widget("Primary Color", primary);

			auto layout_ = dynamic_cast<nanogui::AdvancedGridLayout*>(layout());
			layout_->set_margin(0);
		}
	};

	class optional_light : public nanogui::Widget {
		nanogui::ref<checkbox_widget> light_{};
		nanogui::ref<color_widget> energy_{};
		nanogui::ref<nanogui::Slider> intensity_{};

	  public:
		optional_light(nanogui::Widget* w, const state_handle* state, objects::identifier<objects::object> id)
		    : nanogui::Widget(w), energy_(new color_widget{this}), intensity_(new nanogui::Slider{this}) {
			auto gui = form::FormHelper{w->screen(), this};
			gui.add_group("Lighting Properties");
			gui.add_widget("Light Energy", energy_);
			energy_->set_value(
			    state->view().objects.at(id).is_light ? from_spectrum(
			        state->view().light_objects.at(state->view().objects.at(id).light_id).energy)
			                                                : nanogui::Color{1});

			light_ = gui.add_variable<bool>(
                "Light Source",
                std::function([this, state, id](const bool& b) {
                    auto obj = state->view().objects.at(id);
                    state->update([this, obj, b](scene::builder& builder) {
                        if(b) {
                            auto value
                                = to_spectrum(energy_->value()) * intensity_->value();
                            builder.lights().object(obj.id, value).finish();
                        } else {
                            builder.light(obj.light_id).remove();
                        }
                    });
                    energy_->set_editable(b);
                }),
                std::function([state, id]() {
                    return state->view().objects.at(id).is_light;
                }),
                true);

			energy_->set_final_callback([this, state, id](const nanogui::Color& c) {
				auto obj = state->view().objects.at(id);
				state->update([this, obj, c](scene::builder& b) {
					energy_->set_value(c);

					if(!obj.is_light)
						return;
					b.light(obj.light_id).object(obj.id, to_spectrum(c)).finish();
				});
			});
			energy_->set_editable(state->view().objects.at(id).is_light);

			intensity_->set_range({1.0f, 100.0f});
			intensity_->set_final_callback([this, state, id](float f) {
				if(light_->checked()) {
					auto obj = state->view().objects.at(id);
					state->update([this, obj, f](scene::builder& b) {
						b.light(obj.light_id).object(obj.id, to_spectrum(energy_->value()) * f).finish();
					});
				}
			});

			gui.add_widget("Light Intensity", intensity_);
		}
	};

	class object : public nanogui::Widget {
        nanogui::ref<nanogui::Label> material_label_;
		nanogui::ref<material> material_{};
        nanogui::ref<nanogui::Label> transform_label_;
        nanogui::ref<transform> transform_{};
	  public:
		object(nanogui::Widget* w, const state_handle* state, objects::identifier<objects::object> id)
		    : nanogui::Widget(w),
              material_label_(new nanogui::Label(this, "Material Properties", "sans-bold", 20)),
              material_(new material{this, state, id}),
              transform_label_(new nanogui::Label(this, "Transformation Matrix", "sans-bold", 20)),
              transform_(new transform{this})
        {
			set_layout(new nanogui::GroupLayout{});
            transform_->set_value(state->view().transforms.at(id).mtx());
			// add_child(material_);
			// auto gui = form::FormHelper{w->screen(), this};
			// gui.add_widget("", material_);
		}
	};

    class object_properties : public nanogui::Widget {
        nanogui::ref<nanogui::Label> label_;
        nanogui::ref<object> object_;
        nanogui::ref<optional_light> light_;
      public:
        object_properties(Widget* w, const state_handle* state, objects::identifier<objects::object> id, const std::string& caption)
            : nanogui::Widget(w),
              label_(new nanogui::Label{this, fmt::format("{} Properties", caption), "sans", 20}),
              object_(new object(this, state, id)),
              light_(new optional_light(this, state, id)) {
            set_layout(new nanogui::GroupLayout{15, 6, 14, 0});

        }

        void set_caption(const std::string& value) {
            label_->set_caption(value);
        }
    };

    template<typename T>
    class component_array_properties : public nanogui::Widget {
        nanogui::ref<nanogui::Label> x_label;
        nanogui::ref<nanogui::FloatBox<float>> x_val;

        nanogui::ref<nanogui::Label> y_label;
        nanogui::ref<nanogui::FloatBox<float>> y_val;

        nanogui::ref<nanogui::Label> z_label;
        nanogui::ref<nanogui::FloatBox<float>> z_val;

      public:
        component_array_properties(Widget* w, ui::state_handle& state): nanogui::Widget(w),
              x_label(new nanogui::Label(this, "X")),
              x_val(new nanogui::FloatBox<float>(this)),
              y_label(new nanogui::Label(this, "Y")),
              y_val(new nanogui::FloatBox<float>(this)),
              z_label(new nanogui::Label(this, "Z")),
              z_val(new nanogui::FloatBox<float>(this))
        {
            auto layout = new nanogui::AdvancedGridLayout();
            layout->append_col(3, 1.0);
            layout->append_col(3, 1.0);
            layout->append_col(3, 1.0);
            layout->append_row(3, 1.0);
            layout->append_row(3, 1.0);

            layout->set_anchor(
                x_label,
                nanogui::AdvancedGridLayout::Anchor{
                    0, 0, nanogui::Alignment::Middle, nanogui::Alignment::Fill
                });

            layout->set_anchor(
                y_label,
                nanogui::AdvancedGridLayout::Anchor{
                    1, 0, nanogui::Alignment::Middle, nanogui::Alignment::Fill
                });

            layout->set_anchor(
                z_label,
                nanogui::AdvancedGridLayout::Anchor{
                    2, 0, nanogui::Alignment::Middle, nanogui::Alignment::Fill
                });

            layout->set_anchor(
                x_val,
                nanogui::AdvancedGridLayout::Anchor{
                    0, 1, nanogui::Alignment::Fill, nanogui::Alignment::Fill
                });

            layout->set_anchor(
                y_val,
                nanogui::AdvancedGridLayout::Anchor{
                    1, 1, nanogui::Alignment::Fill, nanogui::Alignment::Fill
                });

            layout->set_anchor(
                z_val,
                nanogui::AdvancedGridLayout::Anchor{
                    2, 1, nanogui::Alignment::Fill, nanogui::Alignment::Fill
                });

            set_layout(layout);
            x_val->set_editable(true);
            y_val->set_editable(true);
            z_val->set_editable(true);
        }

        void set_value(const T& value) {
            x_val->set_value(value.x());
            y_val->set_value(value.y());
            z_val->set_value(value.z());
        }

        void set_callback(const std::function<void(const T&)>& fn) {
            x_val->set_callback([this, fn](float x) {
                auto value = T{x, y_val->value(), z_val->value()};
                fn(value);
            });
            y_val->set_callback([this, fn](float y) {
                auto value = T{x_val->value(), y, z_val->value()};
                fn(value);
            });
            z_val->set_callback([this, fn](float z) {
                auto value = T{x_val->value(), y_val->value(), z};
                fn(value);
            });
        }
    };

    class light_point_properties : public nanogui::Widget {
        nanogui::ref<nanogui::Label> label_;
        nanogui::ref<component_array_properties<math::point<3>>> pos_;
        nanogui::ref<color_widget> energy_;
      public:
        light_point_properties(
            Widget* w,
            ui::state_handle* state,
            objects::light_id id,
            const std::string& caption
        ):
            nanogui::Widget(w),
            label_(new nanogui::Label{this, caption}),
            pos_(new component_array_properties<math::point<3>>(this, *state)),
            energy_(new color_widget(this))
        {
            set_layout(new nanogui::GroupLayout());

            auto light = state->view().light_points.at(id);
            pos_->set_value(light.position);
            pos_->set_callback([=](const math::point<3>& pos) {
                state->update([&](scene::builder& b) {
                  b.mut().light_points.at(id).position = pos;
                });
            });

            energy_->set_value(from_spectrum(light.energy));
            energy_->set_final_callback([=](const nanogui::Color& c) {
                state->update([&](scene::builder& b) {
                    b.mut().light_points.at(id).energy = to_spectrum(c);
                });
            });
        }
    };

	class camera_properties : public nanogui::Widget {
        nanogui::ref<component_array_properties<math::vector<3>>> up_;
        nanogui::ref<component_array_properties<math::point<3>>> from_;
        nanogui::ref<component_array_properties<math::point<3>>> to_;
		nanogui::ref<nanogui::Slider> fov_;

	  public:
		camera_properties(Widget* w, ui::state_handle& state):
              nanogui::Widget(w),
              up_(new component_array_properties<math::vector<3>>(this, state)),
              from_(new component_array_properties<math::point<3>>(this, state)),
              to_(new component_array_properties<math::point<3>>(this, state))
        {
			auto gui = form::FormHelper{w->screen(), this};
			gui.add_group("Camera Settings");
			fov_ = new nanogui::Slider{this};
			fov_->set_range({0.f, math::PI() - math::EPSILON()});
			fov_->set_value(state.view().camera.fov);
			fov_->set_final_callback([&state](float f) {
				state.update([f](scene::builder& b) {
					b.camera().field_of_view(f);
				});
			});
            up_->set_value(state.builder().camera().up());
            from_->set_value(state.builder().camera().from());
            to_->set_value(state.builder().camera().to());

            up_->set_callback([&state](const math::vector<3>& v) {
                state.update([&v](scene::builder& b) {
                  b.camera().up(v);
                });
            });
            from_->set_callback([&state](const math::point<3>& v) {
                state.update([&v](scene::builder& b) {
                    b.camera().from(v);
                });
            });
            to_->set_callback([&state](const math::point<3>& v) {
                state.update([&v](scene::builder& b) {
                    b.camera().to(v);
                });
            });

			gui.add_widget("Field of View", fov_);
            gui.add_widget("Up Direction", up_);
            gui.add_widget("From Position", from_);
            gui.add_widget("To Position", to_);
		}
	};

	nanogui::ref<nanogui::Screen> screen_;
	nanogui::ref<camera_properties> camera_;
    nanogui::ref<nanogui::Widget> light_container_;
	nanogui::ref<nanogui::Widget> object_container_;

    nanogui::Widget* build_lights(state_handle* state) {
        auto& st = state->view();
        auto container = new nanogui::Widget{this};
        container->set_layout(
            new nanogui::BoxLayout{nanogui::Orientation::Vertical, nanogui::Alignment::Maximum});

        for(auto [id, light] : st.lights) {
            auto row = new nanogui::Widget{container};
            row->set_layout(
                new nanogui::BoxLayout{nanogui::Orientation::Horizontal, nanogui::Alignment::Maximum, 5, 5});

            switch(light.type) {
            case objects::LIGHT_POINT: {
                auto edit = row->add<nanogui::PopupButton>(fmt::format("Point Light #{}", id), FA_EDIT);
                auto popup = edit->popup();
                popup->add<light_point_properties>(state, light.id, fmt::format("Point Light #{}", id));
            }
            break;
            case objects::LIGHT_OBJECT:
                break;
            }
        }

        return container;
    }

	nanogui::Widget* build_objects(const state_handle* state) {
		const auto& st = state->view();
		auto container = new nanogui::Widget{this};
		container->set_layout(new nanogui::BoxLayout{nanogui::Orientation::Vertical, nanogui::Alignment::Maximum});

		for(auto [id, obj] : st.objects) {
			auto row = new nanogui::Widget{container};
			row->set_layout(
			    new nanogui::BoxLayout{nanogui::Orientation::Horizontal, nanogui::Alignment::Maximum, 5, 5});

			auto label = row->add<nanogui::Label>("");
			auto remove = row->add<nanogui::Button>("", FA_MINUS);
			auto edit = row->add<nanogui::PopupButton>("", FA_EDIT);
			auto popup = edit->popup();
			popup->set_layout(new nanogui::GroupLayout{});

            std::string caption;
			switch(obj.type) {
			case cgrt::objects::PLANE:
                caption = fmt::format("Plane #{}", (unsigned) (id));
				break;
			case cgrt::objects::SPHERE:
                caption = fmt::format("Sphere #{}", (unsigned) (id));
				break;
            case cgrt::objects::DISK:
                caption = fmt::format("Disk #{}", (unsigned) (id));
                break;
			default:
                caption = fmt::format("Object #{}", (unsigned (id)));
				break;
			}
            label->set_caption(caption);
            popup->add<object_properties>(state, obj.id, caption);

			remove->set_callback([this, state, id, row, edit]() {
				state->update([id](scene::builder& b) {
					b.object(id).remove();
				});
				edit->popup()->dispose();

				object_container_->request_focus();
				object_container_->remove_child(row);
			});
		}

		return container;
	}

  public:
	object_panel(nanogui::Screen* screen, state_handle* state)
	    : nanogui::Window(screen, "Scene Settings"), screen_(screen), camera_(new camera_properties{this, *state}) {
		auto gui = form::FormHelper{screen, this};

        light_container_ = build_lights(state);
		object_container_ = build_objects(state);

		gui.add_group("Scene Settings");
		gui.add_widget("", camera_);
        gui.add_group("Scene Lights");
        gui.add_widget("", light_container_);
		gui.add_group("Scene Objects");
		gui.add_widget("", object_container_);
		center();
	}

	object_panel(const object_panel& o) = delete;
	object_panel& operator=(const object_panel& o) = delete;
};
} // namespace cgrt::ui

#endif //CGRT_CONTROLS_HPP

//
// Created by chris on 12/24/2022.
//

#ifndef CGRT_DEBUG_HPP
#define CGRT_DEBUG_HPP

#include <nanogui/nanogui.h>
#include <cgrt/ui/formhelper.hpp>

namespace cgrt::ui {

class debug_panel : nanogui::Window {
    std::mutex mu_;
    nanogui::IntBox<int> restrict_x;
    nanogui::IntBox<int> restrict_y;
  public:
    inline explicit debug_panel(nanogui::Screen* screen):
          Window(screen),
          restrict_x(this),
          restrict_y(this)
    {
        restrict_x.set_editable(true);
        restrict_y.set_editable(true);

        auto form = form::FormHelper(screen, this);
        form.add_widget("Restrict X", &restrict_x);
        form.add_widget("Restrict Y", &restrict_y);
    }

    math::point<2, int> restrict_pixels() noexcept {
        std::lock_guard lg_(mu_);
        return math::point<2, int>(restrict_x.value(), restrict_y.value());
    }
};

}

#endif //CGRT_DEBUG_HPP

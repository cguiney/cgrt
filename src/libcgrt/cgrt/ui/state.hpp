//
// Created by chris on 10/28/2021.
//
#pragma once
#ifndef CGRT_UI_STATE_HPP
#	define CGRT_UI_STATE_HPP

#	include <memory>
#	include <mutex>

namespace cgrt::ui {

class state_handle {
	std::unique_ptr<scene::builder> b;
	std::mutex mu; // protects render_state
	std::unique_ptr<scene::state> render_state;

	unsigned int last_update{};

  public:
	explicit state_handle(scene::builder b_)
	    : b(std::make_unique<scene::builder>(std::move(b_))), render_state(std::make_unique<scene::state>(b->build())) {
	}

	void update(const std::function<void(scene::state&)>& f) const {
		f(b->mut());
		b->mut().version++;
	}

	void update(const std::function<void(scene::builder&)>& f) const {
		f(*b);
		b->mut().version++;
	}

	[[nodiscard]] const scene::state& view() const {
		return b->view();
	}

	scene::builder& builder() {
		return *b;
	}

	void publish() {
		if(view().version <= last_update)
			return;

		last_update = view().version;

		auto tmp = std::make_unique<scene::state>(b->build());
		std::lock_guard<std::mutex> lg(mu);
		render_state = std::move(tmp);
	}

	std::unique_ptr<scene::state> take() {
		std::unique_ptr<scene::state> rv = nullptr;
		std::lock_guard<std::mutex> lg(mu);
		std::swap(render_state, rv);

		return rv;
	}
};
} // namespace cgrt::ui

#endif //CGRT_UI_STATE_HPP

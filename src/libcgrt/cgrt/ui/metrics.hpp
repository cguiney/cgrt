//
// Created by chris on 10/30/2021.
//

#ifndef CGRT_METRICS_HPP
#define CGRT_METRICS_HPP

#include <mutex>
#include <nanogui/nanogui.h>
#include <fmt/format.h>

namespace cgrt::ui {
class avg_graph : public nanogui::Graph {
	std::vector<int> samples_{};
	float max_;
	int last_;
	int total_;

  public:
	avg_graph(nanogui::Widget* w, const std::string title = "", float max = 20.f)
	    : nanogui::Graph(w, title), samples_(100, 0), max_(max) {
		set_values(std::vector<float>(100, 0.0f));
	}

	inline void inc(int t, int n = 1) {
		total_ += n;
		if(t == last_) {
			samples_[t % 100] += n;
		} else {
			total_ -= samples_[t % 100];
			samples_[t % 100] = n;
			last_ = t;
		}

		float avg_fps = (float) (total_) / std::min((float) (t), 100.0f);
		values()[t % 100] = avg_fps / max_; // scaling by max to normalize as % of max
		set_footer(fmt::format("{:.2f}fp/s", avg_fps));
	}
};

class cumulative_count : public nanogui::Graph {
	std::vector<int> samples_{};
	float max_;
	float total_;

  public:
	cumulative_count(nanogui::Widget* w, const std::string title = "", float max = 20.f)
	    : nanogui::Graph(w, title), samples_(100, 0), max_(max) {
		set_values(std::vector<float>(100, 0.0f));
	}

	inline void inc(int t, int n = 1) {
		total_ += n;
		max_ = std::max(total_, max_);
		values()[t % 100] = total_ / max_; // scaling by max to normalize as % of max
		set_footer(fmt::format("{:.2f}units/s", total_));
	}

	inline void reset() {
		total_ = 0;
	}
};

class metrics_panel : public nanogui::Window {
	std::mutex mu_;
	nanogui::ref<avg_graph> fps_;
	nanogui::ref<avg_graph> samples_;
	nanogui::ref<cumulative_count> cum_samples_;

  public:
	metrics_panel(nanogui::Widget* w): nanogui::Window(w) {
		set_title("Metrics");
		set_layout(new nanogui::GroupLayout{});
		fps_ = new avg_graph{this, "Frames Per Second"};
		samples_ = new avg_graph{this, "Samples Per Second"};
		cum_samples_ = new cumulative_count{this, "Samples in Frame"};
	}

	void inc(int t, int samples) {
		std::lock_guard<std::mutex> lg_{mu_};
		fps_->inc(t);
		samples_->inc(t, samples);
		cum_samples_->inc(t, samples);
		screen()->redraw();
	}

	void reset() {
		std::lock_guard<std::mutex> lg_{mu_};
		cum_samples_->reset();
	}

	void draw(NVGcontext* ctx) override {
		std::lock_guard<std::mutex> lg_{mu_};
		Window::draw(ctx);
	}
};
} // namespace cgrt::ui

#endif //CGRT_METRICS_HPP

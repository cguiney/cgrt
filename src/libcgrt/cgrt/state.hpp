//
// Created by chris on 1/11/21.
//

#ifndef CGRT_STATE_HPP
#define CGRT_STATE_HPP

#include <cgrt/math/random.hpp>
#include <cgrt/math/transform.hpp>
#include <cgrt/objects/cameras.hpp>
#include <cgrt/objects/objects.hpp>
#include <cgrt/objects/lights.hpp>
#include <cgrt/objects/materials.hpp>
#include <cgrt/scene/builder.hpp>
#include <cgrt/scene/state.hpp>
#include <utility>

namespace cgrt {

class view;

template<typename RuntimeT>
class dense_state {
    template<typename T>
    using table_storage = typename containers::storage<
        RuntimeT,
        std::enable_if_t<std::is_default_constructible_v<T>, T>>;

    struct storage {
        /* objects */
        table_storage<objects::object> objects;
        table_storage<objects::mesh> meshes;
        table_storage<math::transform::matrix> transforms;
        table_storage<objects::material> materials;

        /* lights */
        table_storage<objects::light_object> light_objects;
        table_storage<objects::light_point> light_points;
        table_storage<objects::light> lights;

        table_storage<objects::camera> cameras;

        explicit storage(const scene::state& st)
            : objects(st.objects.size()),
              transforms(st.objects.size()),
              materials(st.objects.size()),
              light_objects(st.lights.size()),
              light_points(st.lights.size()),
              lights(st.lights.size()),
              cameras(1) {
            cameras[0] = st.camera;

            std::unordered_map<objects::object_id, objects::object_id> object_map;
            std::unordered_map<objects::light_id, objects::light_id> light_map;

            // copy light data
            auto i = 0;
            for(auto [id, light] : st.lights) {
                light.id = objects::light_id{i++};
                lights[light.id] = light;
                switch(light.type) {
                case cgrt::objects::LIGHT_POINT:
                    light_points[light.id] = st.light_points.at(id);
                    break;
                case cgrt::objects::LIGHT_OBJECT:
                    light_objects[light.id] = st.light_objects.at(id);
                    break;
                }
                light_map[id] = light.id;
            }

            // copy object data
            auto j = 0;
            for(auto [id, obj] : st.objects) {
                auto m = st.materials.at(id);
                auto t = st.transforms.at(id);
                auto obj_id = objects::object_id{j++};
                auto light_id = obj.light_id;
                object_map[id] = obj_id;
                obj.id = obj_id;
                obj.light_id = obj.is_light ? light_map.at(light_id) : objects::light_id{};

                objects[obj_id] = obj;
                transforms[obj_id] = t;
                materials[obj_id] = m;

                // update the light with the new object id
                if(obj.is_light) {
                    auto light_obj = st.light_objects.at(light_id);
                    light_obj.object_id = obj.id;
                    light_objects[obj.light_id] = light_obj;
                }
            }
        }
    };
    std::unique_ptr<storage> storage_;

  public:
    dense_state() = default;

    /* action */
    CGRT_HOST explicit dense_state(const scene::state& st):
        storage_(std::make_unique<storage>(st))
    {}

    [[nodiscard]] CGRT_HOST constexpr inline const table_storage<objects::camera>& cameras() const noexcept {
        return storage_->cameras;
    }

    [[nodiscard]] CGRT_HOST constexpr inline const table_storage<objects::object>& objects() const noexcept {
        return storage_->objects;
    }

    [[nodiscard]] CGRT_HOST constexpr inline const table_storage<objects::mesh>& meshes() const noexcept {
        return storage_->meshes;
    }

    [[nodiscard]] CGRT_HOST constexpr inline const table_storage<math::transform::matrix>& transforms() const noexcept {
        return storage_->transforms;
    }

    [[nodiscard]] CGRT_HOST constexpr inline const table_storage<objects::material>& materials() const noexcept {
        return storage_->materials;
    }

    [[nodiscard]] CGRT_HOST constexpr inline const table_storage<objects::light_object>& light_objects() const noexcept {
        return storage_->light_objects;
    }

    [[nodiscard]] CGRT_HOST constexpr inline const table_storage<objects::light_point>& light_points() const noexcept {
        return storage_->light_points;
    }

    [[nodiscard]] CGRT_HOST constexpr inline const table_storage<objects::light>& lights() const noexcept {
        return storage_->lights;
    }
};

class dense_state_view {
    template<typename T>
    using table = containers::span<const T>;

    /* objects */
    table<objects::object> objects_;
    table<objects::mesh> meshes_;
    table<math::transform::matrix> transforms_;
    table<objects::material> materials_;

    /* lights */
    table<objects::light_object> light_objects_;
    table<objects::light_point> light_points_;
    table<objects::light> lights_;

    /* camera */
    table<objects::camera> cameras_;

  public:
    template<typename RuntimeT>
    explicit dense_state_view(const dense_state<RuntimeT>& state) noexcept :
          objects_(state.objects()),
          meshes_(state.meshes()),
          transforms_(state.transforms()),
          materials_(state.materials()),
          light_objects_(state.light_objects()),
          light_points_(state.light_points()),
          lights_(state.lights()),
          cameras_(state.cameras())
    {}

    [[nodiscard]] CGRT_KERNEL_FUNC constexpr inline const objects::camera& camera() const noexcept {
        return *cameras_.begin();
    }

    [[nodiscard]] CGRT_KERNEL_FUNC constexpr inline const table<objects::object>& objects() const noexcept {
        return objects_;
    }

    [[nodiscard]] CGRT_KERNEL_FUNC constexpr inline const table<objects::mesh>& meshes() const noexcept {
        return meshes_;
    }

    [[nodiscard]] CGRT_KERNEL_FUNC constexpr inline const table<math::transform::matrix>& transforms() const noexcept {
        return transforms_;
    }

    [[nodiscard]] CGRT_KERNEL_FUNC constexpr inline const table<objects::material>& materials() const noexcept {
        return materials_;
    }

    [[nodiscard]] CGRT_KERNEL_FUNC constexpr inline const table<objects::light_object>& light_objects() const noexcept {
        return light_objects_;
    }

    [[nodiscard]] CGRT_KERNEL_FUNC constexpr inline const table<objects::light_point>& light_points() const noexcept {
        return light_points_;
    }

    [[nodiscard]] CGRT_KERNEL_FUNC constexpr inline const table<objects::light>& lights() const noexcept {
        return lights_;
    }
};


CGRT_HOST inline scene::builder default_state() {
	scene::builder b;
	b.lights().point(math::point<3>(-10.f, 10.f, -10.f), math::spectrum(1.f)).finish();

	auto m = objects::default_material();
	m.primary = math::spectrum(0.8f, 1.f, 0.6f);
	m.diffuse = 0.7f;
	m.specular = 0.2f;

	b.objects(objects::SPHERE).use_material(m).finish();

	b.objects(objects::SPHERE).use_transform(math::transform::scale(0.5f, 0.5f, 0.5f)).finish();

	return b;
}

CGRT_HOST inline scene::builder spheres_example(int w, int h) {
	using namespace math;
	using namespace math::transform;

	scene::builder b{};

	b.set_film(w, h);


	b.lights()
	     .point(math::point<3>(-5.0f, 2.5f, -5.0f), math::spectrum(10.f))
	     .finish();

	b.camera()
	    .from(math::point<3>(0.f, 3.f, -5.f))
	    .to(math::point<3>(0.f, 0.f, 1.f))
	    .up(math::vector<3>(0.f, 1.f, 0.f))
	    .field_of_view(radians(45));

	// build the floor
	[&]() {
		objects::material m = objects::default_material();
		m.pattern = objects::SOLID;
		m.primary = spectrum(0.2f, 0.2f, 0.2f);
		// m.secondary = spectrum(1.f, 0.f, 1.f);
		m.specular = 0.2;

        auto t = rotate_x(radians(90.f)) * scale(100, 100, 100);
		b.objects(objects::DISK).use_transform(t).use_material(m).finish();
	}();

    /*
	// ceiling
	[&]() {
		objects::material m = objects::default_material();
		m.pattern = objects::SOLID;
		m.primary = spectrum(0.1f);
		m.specular = 0.2;

		b.objects()
		    .create(objects::DISK)
		    .use_material(m)
		    .use_transform(rotate_x(radians(270.f)) * translation(0.f, 5.1f, 0.f))
		    .finish();
	}();

	[&]() { // forward wall
		objects::material m = objects::default_material();
		m.primary = spectrum(0.5f, 1.f, 0.f);
		m.specular = 0.f;

		b.objects().create(objects::DISK).use_material(m).use_transform(translation(0.f, 0.f, 3.f)).finish();
	}();

	[&] { // right wall
		objects::material m = objects::default_material();
		m.primary = spectrum(1.f, 0.4f, 0.2f);
		m.specular = 0.f;

		b.objects()
		    .create(objects::DISK)
		    .use_material(m)
		    .use_transform(translation(3.f, 0.f, 0.f) * rotate_y(radians(90.f)))
		    .finish();
	}();

	[&] { // left wall
		objects::material m = objects::default_material();
		m.primary = spectrum(2.f, 1.f, 0.0f);
		m.specular = 0;

		b.objects()
		    .create(objects::DISK)
		    .use_material(m)
		    .use_transform(translation(-3.f, 0.f, 0.f) * rotate_y(math::radians(270.f)))
		    .finish();
	}();

	[&] { // back wall
		objects::material m = objects::default_material();
		m.primary = spectrum(1.f, 0.9f, 0.9f);
		m.specular = 0.f;

		b.objects().create(objects::DISK).use_material(m).use_transform(translation(0.f, 0.f, 10.f)).finish();
	}();

    */
	[&] { // middle sphere
		objects::material m = objects::default_material();
		// m.type = objects::MIRROR;
		m.type = objects::LAMBERTIAN;
		m.primary = spectrum(1.f);

		auto id = objects::identifier<objects::object>{};
		b.objects(objects::SPHERE).tag(id).use_material(m).use_transform(translation(0.f, 1.f, 0.f)).finish();

		b.lights().object(id, spectrum(1.f)).finish();
	}();


	[&] { // right sphere
		objects::material m = objects::default_material();
		m.primary = spectrum(0.5f, 0.1f, 1.0f);
		m.diffuse = 0.7f;
		m.specular = 0.3f;

		b.objects(objects::SPHERE)
		    .use_material(m)
		    .use_transform(translation(1.5f, 0.5f, -0.5f) * scale(0.5f, 0.5f, 0.5f))
		    .finish();
	}();

	[&] { // left sphere
		objects::material m = objects::default_material();
		m.primary = spectrum(1.f, 0.8f, 0.1f);
		m.diffuse = 0.7f;
		m.specular = 0.3f;

		b.objects(objects::SPHERE)
		    .use_material(m)
		    .use_transform(translation(-1.5f, 0.33f, -0.75f) * scale(0.33f, 0.33f, 0.33f))
		    .finish();

	}();

	return b;
}

} // namespace cgrt

#endif //CGRT_STATE_HPP

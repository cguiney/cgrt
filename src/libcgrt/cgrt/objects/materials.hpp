//
// Created by chris on 1/16/21.
//

#ifndef CGRT_MATERIALS_HPP
#define CGRT_MATERIALS_HPP

#include <cgrt/math/scalar.hpp>
#include <cgrt/math/radiosity.hpp>

namespace cgrt::objects {
    enum material_type {
        DEFAULT = 0,
        LAMBERTIAN = 1,
        MIRROR = 2,
    };

    enum material_pattern {
        SOLID = 0,
        CHECKERS = 1,
        THROB = 2,
    };

    struct material {
        material_type type{};
        material_pattern pattern{};
        math::spectrum primary;
        math::spectrum secondary;
        math::scalar ambient;
        math::scalar diffuse;
        math::scalar specular;
        math::scalar shininess;
        math::scalar reflectiveness;
        math::scalar transparency;
        math::scalar refractive_index;
    };

    inline material default_material() {
        material m;
        m.type = DEFAULT;
        m.primary = math::spectrum(1.f, 1.f, 1.f);
        m.secondary = math::spectrum(0.f);
        m.ambient = 0.1f;
        m.diffuse = 0.9f;
        m.specular = 0.9f;
        m.shininess = 200.0f;
        m.reflectiveness = 0.f;
        m.transparency = 0.f;
        m.refractive_index = 1.f;
        return m;
    }
}

#endif //CGRT_MATERIALS_HPP

//
// Created by chris on 1/16/21.
//

#ifndef CGRT_LIGHTS_HPP
#define CGRT_LIGHTS_HPP

#include <cgrt/math/point.hpp>
#include <cgrt/math/radiosity.hpp>
#include <cgrt/objects/objects.hpp>

#include <utility>

namespace cgrt::objects {

    struct light_point {
        math::point<3> position;
        math::spectrum energy;

    public:
        light_point() = default;

        light_point(
            math::point<3>  pos,
            math::spectrum  energy
        ) : position(std::move(pos)), energy(std::move(energy)) {}
    };

    struct light_object {
        identifier<object> object_id{};
        math::spectrum energy;

    public:
        light_object() = default;

        light_object(
            identifier<objects::object> id,
            math::spectrum  energy
        ) : object_id(id), energy(std::move(energy)) {}
    };

    enum light_type {
        LIGHT_POINT,
        LIGHT_OBJECT
    };

    struct light {
        enum light_type type{};
        light_id id{};
    };
}

#endif //CGRT_LIGHTS_HPP

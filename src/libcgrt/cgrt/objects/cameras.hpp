//
// Created by chris on 1/23/21.
//

#ifndef CGRT_CAMERAS_HPP
#define CGRT_CAMERAS_HPP

#include <cgrt/runtime.hpp>
#include <cgrt/math/constants.h>
#include <cgrt/math/scalar.hpp>
#include <cgrt/math/transform.hpp>
#include <cmath>
#include <utility>

namespace cgrt::objects
{

    struct camera
    {
        uint32_t width{};
        uint32_t height{};
        math::scalar fov{};
        math::transform::matrix transform{};

        // derived
        math::scalar half{};
        math::scalar aspect{};
        math::scalar half_width{};
        math::scalar half_height{};
        math::scalar pixel_size{};

        CGRT_KERNEL_FUNC void update()
        {
            half = tan(fov / 2.f);
            aspect = (float) (width) / (float) (height);

            if (aspect >= 1.f) {
                half_width = half;
                half_height = half / aspect;
            } else {
                half_width = half * aspect;
                half_height = half;
            }

            pixel_size = (half_width * 2.f) / (float) (width);
        }

    public:
        inline camera() = default;
        CGRT_KERNEL_FUNC inline explicit camera(
            uint32_t width,
            uint32_t height,
            math::scalar fov,
            math::transform::matrix transform
        ) : width(width), height(height), fov(fov), transform(transform)
        {
            update();
        }

        camera(const camera& other) = default;

        CGRT_KERNEL_FUNC uint64_t pixels() const {
            return width * height;
        }
    };

    [[nodiscard]] CGRT_KERNEL_FUNC inline camera default_camera()
    {
        camera c(160, 120, CGRT_PI_2, math::identity<4>());
        return c;
    }

    CGRT_KERNEL_FUNC inline math::ray<3> generate_ray(const camera &c, const math::scalar& px, const math::scalar& py)
    {
        auto xoffset = c.pixel_size * (px + 0.5f);
        auto yoffset = c.pixel_size * (py + 0.5f);

        auto worldx = c.half_width - xoffset;
        auto worldy = c.half_height - yoffset;

        using math::transform::inverse;
        using math::normalize;
        using math::point;
        auto target = inverse(c.transform)(point<3>(worldx, worldy, -1.f));
        auto origin = inverse(c.transform)(point<3>(0.f, 0.f, 0.f));
        auto direction = normalize(target - origin);

        return {origin, direction};
    }
}

#endif //CGRT_CAMERAS_HPP

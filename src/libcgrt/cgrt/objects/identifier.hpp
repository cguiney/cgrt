//
// Created by chris on 10/22/2021.
//
#pragma once
#ifndef CGRT_IDENTIFIER_HPP
#	define CGRT_IDENTIFIER_HPP

namespace cgrt::objects {
template<typename T>
class identifier {
	uint64_t id;

  public:
	using type = T;

	CGRT_KERNEL_FUNC constexpr inline identifier(): id(0) {
	}

	template<typename U>
	CGRT_KERNEL_FUNC constexpr inline explicit identifier(U value): id(static_cast<uint64_t>(value)) {
	}

	CGRT_KERNEL_FUNC constexpr inline operator size_t() const {
		return id;
	}

	CGRT_KERNEL_FUNC constexpr inline identifier<T>& operator++() {
		id++;
		return *this;
	}

	CGRT_KERNEL_FUNC constexpr inline bool operator==(identifier<T> o) const {
		return id == o.id;
	}
};
}; // namespace cgrt::objects

namespace std {
template<typename T>
struct hash<cgrt::objects::identifier<T>> {
	std::size_t operator()(const cgrt::objects::identifier<T>& id) const {
		return hash<uint64_t>()(static_cast<uint64_t>(id));
	}
};

} // namespace std

#endif //CGRT_IDENTIFIER_HPP

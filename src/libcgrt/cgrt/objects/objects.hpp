//
// Created by chris on 1/8/21.
//

#ifndef CGRT_OBJECTS_HPP
#define CGRT_OBJECTS_HPP

#include <cgrt/objects/identifier.hpp>

namespace cgrt::objects {

struct object;
struct light;

using object_id = identifier<objects::object>;
using light_id = identifier<objects::light>;

enum object_type
{
	SPHERE = 0,
	CUBE = 1,
	PLANE = 2,
	MESH = 3,
	DISK = 4
};

struct object {
	object_type type{};
    identifier<object> id{};

	bool is_light{};
    identifier<light> light_id;
};

struct mesh {
    identifier<objects::mesh> id;
};

} // namespace cgrt::objects

#endif //CGRT_OBJECTS_HPP

//
// Created by chris on 10/28/2021.
//
#pragma once
#ifndef CGRT_SCENE_STATE_HPP
#    define CGRT_SCENE_STATE_HPP

#    include <cgrt/objects/cameras.hpp>
#    include <cgrt/objects/identifier.hpp>
#    include <cgrt/objects/lights.hpp>
#    include <cgrt/objects/materials.hpp>
#    include <cgrt/objects/objects.hpp>
#    include <unordered_map>
namespace cgrt::scene {

struct state {
    template<typename K, typename T = K>
    using table = std::unordered_map<objects::identifier<K>, T>;

    unsigned version{};
    objects::camera camera{};
    table<cgrt::objects::object> objects;
    // table<mesh> meshes;
    table<cgrt::objects::object, math::transform::matrix> transforms;
    table<cgrt::objects::object, objects::material> materials;

    table<cgrt::objects::light> lights;
    table<cgrt::objects::light, objects::light_point> light_points;
    table<cgrt::objects::light, objects::light_object> light_objects;
};
} // namespace cgrt::scene

#endif //CGRT_SCENE_STATE_HPP

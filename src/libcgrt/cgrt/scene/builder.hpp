//
// Created by chris on 10/28/2021.
//
#pragma once
#ifndef CGRT_SCENE_BUILDER_HPP
#define CGRT_SCENE_BUILDER_HPP

#include <variant>

#include <cgrt/scene/state.hpp>

namespace cgrt::scene {
    class builder {
        state st_;

        objects::object_id object_id_{};
        objects::light_id light_id{};

        struct {
            int width = 1024;
            int height = 1024;
        } film_args;

        struct {
            math::point<3> from = math::point<3>(0, 2, -7);
            math::point<3> to = math::point<3>(0, 0, 1);
            math::vector<3> up = math::vector<3>(0, 1, 0);
            math::scalar fov = CGRT_PI_2;
        } camera_args;

    public:
        builder() : st_() {
        }

        builder(const builder &o) :
                st_(o.st_),
                film_args(o.film_args),
                camera_args(o.camera_args) {
        }

        struct camera_builder {
            builder &parent;

            camera_builder &from(const math::point<3> &f) {
                parent.camera_args.from = f;
                return *this;
            }

            [[nodiscard]] const math::point<3>& from() const {
                return parent.camera_args.from;
            }

            camera_builder &to(const math::point<3> &f) {
                parent.camera_args.to = f;
                return *this;
            }

            [[nodiscard]] const math::point<3>& to() const {
                return parent.camera_args.to;
            }

            camera_builder &up(const math::vector<3> &f) {
                parent.camera_args.up = f;
                return *this;
            }

            [[nodiscard]] const math::vector<3>& up() const {
                return parent.camera_args.up;
            }

            camera_builder &field_of_view(const math::scalar &fov) {
                parent.camera_args.fov = fov;
                return *this;
            }

            builder &finish() {
                return parent;
            }

        };

        class light_builder {
            builder& builder_;
            struct light_state {
                objects::light light;
                std::variant<objects::light_point, objects::light_object> value;
            };

            light_state light_;

          public:
            explicit light_builder(builder& b): builder_(b) {
                light_.light.id = ++b.light_id;
            }

            light_builder(builder& b, objects::identifier<objects::light> id): builder_(b) {
                light_.light.id = id;
                light_.light = b.st_.lights.at(id);
                switch(light_.light.type) {
                case objects::LIGHT_POINT:
                    light_.value = b.st_.light_points[id];
                    break;
                case objects::LIGHT_OBJECT:
                    light_.value = b.st_.light_objects[id];
                    break;
                default:
                    throw std::invalid_argument("unknown light id");
                }
            }

            template<typename... Args>
            light_builder point(Args... args) {
                light_.light.type = objects::LIGHT_POINT;
                light_.value = objects::light_point(args...);
                return *this;
            }

            template<typename... Args>
            light_builder& object(Args... args) {
                light_.light.type = objects::LIGHT_OBJECT;
                light_.value = objects::light_object(args...);
                return *this;
            }

            light_builder& clear() {
                auto& st = builder_.st_;
                for(const auto& [_, obj] : st.light_objects) {
                    st.objects[obj.object_id].is_light = false;
                    st.objects[obj.object_id].light_id = {};
                }
                st.lights.clear();
                st.light_points.clear();
                st.light_objects.clear();

                return *this;
            }

            builder& remove() {
                auto id = light_.light.id;
                auto& st = builder_.st_;

                if(!containers::contains(st.lights, id)) {
                    throw std::runtime_error("cannot remove light: does not exist");
                }

                auto light = st.lights[id];
                switch(light.type) {
                case objects::LIGHT_POINT: {
                    st.light_points.erase(id);
                    break;
                }
                case objects::LIGHT_OBJECT: {
                    auto obj = st.light_objects[id];
                    if(!containers::contains(st.objects, obj.object_id))
                        throw std::runtime_error("object light does not have associated object");
                    st.light_objects.erase(id);
                    st.objects[obj.object_id].is_light = false;
                    st.objects[obj.object_id].light_id = {};
                    break;
                }
                }

                st.lights.erase(id);

                return builder_;
            }

            builder& finish() {
                auto& li = light_.light;
                auto& value = light_.value;
                auto& st = builder_.st_;

                switch(li.type) {
                case objects::LIGHT_POINT: {
                    auto point = std::get<objects::light_point>(value);
                    st.light_points[li.id] = point;
                    break;
                }
                case objects::LIGHT_OBJECT: {
                    auto obj = std::get<objects::light_object>(value);
                    st.light_objects[li.id] = obj;
                    st.objects[obj.object_id].is_light = true;
                    st.objects[obj.object_id].light_id = li.id;
                    break;
                }
                }
                st.lights[li.id] = li;

                return builder_;
            }
        };

        class object_builder {
            builder& builder_;
            objects::object object_;
            std::optional<math::transform::matrix> transform_;
            std::optional<objects::material> material_;
          public:
            object_builder(builder& b, objects::object_type t) : builder_(b) {
                    object_.id = ++b.object_id_;
                    object_.type = t;
            }

            object_builder(builder& b, objects::object_id id) : builder_(b) {
                object_ = b.st_.objects.at(id);
                transform_ = b.st_.transforms.at(id);
                material_ = b.st_.materials.at(id);
            }

            inline object_builder& use_transform(const math::transform::matrix& t) {
                transform_ = t;
                return *this;
            }

            inline object_builder& use_material(const objects::material& m) {
                material_ = m;
                return *this;
            }

            inline object_builder& tag(objects::identifier<objects::object>& id) {
                id = object_.id;
                return *this;
            }

            inline builder& remove() {
                auto& obj = object_;
                auto& st = builder_.st_;

                if(obj.light_id) {
                    light_builder(builder_, obj.light_id).remove();
                }

                st.objects.erase(obj.id);
                st.transforms.erase(obj.id);
                st.materials.erase(obj.id);

                return builder_;
            }

            inline builder& finish() {
                auto& obj = object_;
                auto& st = builder_.st_;

                st.objects[obj.id] = obj;
                st.transforms[obj.id] = transform_.value_or(math::identity<4>());
                st.materials[obj.id] = material_.value_or(objects::default_material());
                return builder_;
            }
        };

        inline builder &set_film(int width, int height) {
            film_args.width = width;
            film_args.height = height;
            return *this;
        }

        inline camera_builder camera() {
            return camera_builder{*this};
        }

        inline light_builder lights() {
            return light_builder{*this};
        }

        inline light_builder light(objects::light_id id) {
            return light_builder{*this, id};
        }

        inline object_builder objects(objects::object_type t) {
            return object_builder{*this, t};
        }

        inline object_builder object(objects::object_id id) {
            return object_builder{*this, id};
        }

        inline state &mut() {
            return st_;
        }

        [[nodiscard]]
        const state &view() const {
            return st_;
        }

        [[nodiscard]]
        state build() const {
            auto st = st_;
            st.camera = objects::camera(
                    film_args.width,
                    film_args.height,
                    camera_args.fov,
                    math::transform::camera_view(camera_args.from, camera_args.to, camera_args.up)
            );

            return st;
        }
    };
}

#endif //CGRT_BUILDER_HPP

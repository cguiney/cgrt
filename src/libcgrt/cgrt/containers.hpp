//
// Created by chris on 11/20/2021.
//

#ifndef CGRT_CONTAINERS_HPP
#define CGRT_CONTAINERS_HPP

#include <cgrt/runtime.hpp>

#include <type_traits>
#include <memory>
#include <optional>
#include <tuple>
#include <vector>
#include <assert.h>

#if CGRT_CUDA
#include <thrust/optional.h>
namespace cgrt::containers {

template<typename T>
using optional = thrust::optional<T>;
using nullopt_t = thrust::nullopt_t;
}
#else
namespace cgrt::containers {
template<typename T>
using optional = std::optional<T>;
using nullopt_t = std::nullopt_t;
}
#endif

namespace cgrt::containers {

template<typename ...Ts>
struct tuple;

template<typename RuntimeT, typename T>
struct storage;

template<typename T>
struct span;

template<typename RuntimeT, typename T>
struct storage {
    friend span<T>;

    using type = storage<RuntimeT, T>;
    using element_type = T;
    using value_type = std::remove_cv_t<T>;
    using span_type = span<element_type>;
    using const_span_type = span<const value_type>;
    using device_vector_type = runtime::device_vector_t<RuntimeT, T>;
    using host_vector_type = runtime::host_vector_t<RuntimeT, T>;
    using size_type = size_t;
    using difference_type = ptrdiff_t;
    using pointer = T*;
    using const_pointer = const T*;
    using reference = typename device_vector_type::reference;
    using const_reference = const T&;
    using iterator = typename device_vector_type::iterator;
    using const_iterator = const T*;

    template<typename A, typename B = T>
    using is_span_assignable = typename std::disjunction<
        std::is_constructible<span<B>, A&>,
        std::is_assignable<span<B>, A&>,
        std::is_convertible<A&, span<B>>
    >;

    static_assert(is_span_assignable<device_vector_type>::value, "device_vector_type not implicitly convertible to span");

  private:
    device_vector_type device_storage_;

  public:
    storage() = default;
    storage(storage<RuntimeT, T>&& other) = default;
    ~storage() = default;

    CGRT_HOST explicit storage(size_t size) noexcept : device_storage_(size) {}

    template<typename U>
    CGRT_HOST explicit storage(U values) noexcept : device_storage_(values) {}

    CGRT_HOST constexpr inline reference operator[](size_type index) noexcept {
        return device_storage_[index];
    }

    CGRT_HOST constexpr inline T* data() noexcept {
        return device_storage_.data();
    }

    CGRT_HOST inline size_type size() const {
        return device_storage_.size();
    }

    CGRT_HOST constexpr inline type* operator->() noexcept {
        return &device_storage_;
    }

    CGRT_HOST constexpr inline explicit operator span<const T>() const {
        return device_storage_;
    }

    CGRT_HOST constexpr inline operator span<T>() {
        return device_storage_;
    }

    CGRT_HOST constexpr inline operator host_vector_type () {
        return host_vector_type{device_storage_};
    }

    // todo: find better way to handle conversions to span than this
    CGRT_HOST constexpr inline device_vector_type& raw() {
        return device_storage_;
    }

    CGRT_HOST constexpr inline const device_vector_type& craw() const {
        return device_storage_;
    }

    CGRT_HOST constexpr inline host_vector_type to_host() const noexcept {
        return host_vector_type{device_storage_};
    }
};

template<typename T>
struct span {
  public:
	using element_type = T;
	using value_type = std::remove_cv_t<T>;
	using size_type = size_t;
	using difference_type = ptrdiff_t;
	using pointer = element_type*;
    using reference = element_type&;
    using iterator = element_type*;

	using const_pointer = const value_type*;
	using const_reference = const value_type&;
	using const_iterator = const value_type*;

  private:
    element_type* values_ = nullptr;
    size_t len_ = 0;

  public:
    inline span() = default;
    inline span(span<T>&& other) noexcept = default;
    inline span(const span<T>& other) noexcept = default;
    inline ~span() = default;

    template<typename RuntimeT>
    CGRT_HOST inline span(storage<RuntimeT, T>& storage) noexcept : span(static_cast<span<T>>(storage.raw())) {}

    CGRT_KERNEL_FUNC inline span(pointer values, size_type len) noexcept: values_(values), len_(len) {}

    CGRT_HOST inline span(std::vector<value_type>& values) noexcept : values_(values.data()), len_(values.size()) {}

    CGRT_HOST inline span(const std::vector<value_type>& values) noexcept : values_(values.data()), len_(values.size()) {}

    CGRT_KERNEL_FUNC inline span<T>& operator=(const span<T>& other) noexcept {
        values_ = other.values_;
        len_ = other.len_;
        return *this;
    }

    CGRT_KERNEL_FUNC inline span<T>& operator=(span<T>&& other) noexcept {
        std::exchange(values_, other.values_);
        std::exchange(len_, other.len_);
    }

	CGRT_KERNEL_FUNC constexpr inline iterator begin() const noexcept {
		return values_;
	}

	CGRT_KERNEL_FUNC constexpr inline iterator end() const noexcept {
		return begin() + len_;
	}

	CGRT_KERNEL_FUNC constexpr inline const_iterator cbegin() const noexcept {
		return values_;
	}

	CGRT_KERNEL_FUNC constexpr inline const_iterator cend() const noexcept {
		return cbegin() + len_;
	}

	CGRT_KERNEL_FUNC constexpr inline pointer rbegin() const noexcept {
		return end();
	}

	CGRT_KERNEL_FUNC constexpr inline pointer rend() const noexcept {
		return rbegin() - len_;
	}

	CGRT_KERNEL_FUNC constexpr inline pointer front() const {
		return begin();
	}

	CGRT_KERNEL_FUNC constexpr inline pointer back() const {
		return end() - 1;
	}

	[[nodiscard]] CGRT_KERNEL_FUNC constexpr inline size_type size() const noexcept {
		return len_;
	}

	[[nodiscard]] CGRT_KERNEL_FUNC constexpr inline pointer data() const noexcept {
		return values_;
	}

	CGRT_KERNEL_FUNC constexpr inline reference operator[](size_type idx) const noexcept {
        // if(idx >= len_) printf("assertion failure: %llu < %llu\n", idx, len_);
        assert(idx < len_);
		return data()[idx];
	}

	[[nodiscard]] CGRT_KERNEL_FUNC constexpr inline size_type size_bytes() const noexcept {
		return size() * sizeof(element_type);
	}

	[[nodiscard]] CGRT_KERNEL_FUNC constexpr inline bool empty() const noexcept {
		return size() == 0;
	}
};

template<typename T>
class stack {
    static_assert(std::is_default_constructible_v<T>);
    using size_type = typename span<T>::size_type;
    span<T> data_;
    size_type pos_;
  public:
    CGRT_KERNEL_FUNC constexpr inline explicit stack(span<T> data) noexcept : data_(data), pos_(0) {
        assert(!data_.empty());
    }

    [[nodiscard]]
    CGRT_KERNEL_FUNC constexpr inline bool full() const noexcept {
        return pos_ == data_.size();
    }

    [[nodiscard]]
    CGRT_KERNEL_FUNC constexpr inline bool empty() const noexcept {
        return pos_ == 0;
    }

    CGRT_KERNEL_FUNC constexpr inline size_type size() const noexcept {
        return pos_;
    }

    CGRT_KERNEL_FUNC constexpr inline T& current() noexcept {
        assert(pos_ > 0);
        return data_[pos_ - 1];
    }

    CGRT_KERNEL_FUNC constexpr inline T& push() noexcept {
        assert(pos_ < data_.size());
        auto& v = data_[pos_++];
        v = T{};
        return v;
    }

    CGRT_KERNEL_FUNC constexpr inline T& pop() noexcept {
        assert(pos_ > 0);
        return data_[--pos_];
    }
};

template<typename C, typename T>
constexpr inline bool contains(const C& container, const T& elem) {
	if(auto it = container.find(elem); it != container.end())
		return true;
	return false;
}

};     // namespace cgrt::containers
#endif //CGRT_CONTAINERS_HPP

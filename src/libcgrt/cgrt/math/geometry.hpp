//
// Created by chris on 11/19/2020.
//

#ifndef CGRT_GEOMETRY_HPP
#define CGRT_GEOMETRY_HPP

#include <cgrt/math/point.hpp>
#include <cgrt/math/ray.hpp>
#include <cgrt/math/vector.hpp>
#include <cgrt/math/constants.h>

namespace cgrt::math {

enum axis {
	X,
	Y,
	Z
};

CGRT_KERNEL_FUNC inline enum axis dimension(const vector<3>& f) {
	if(max(f.x(), f.y()) == f.x()) {
		if(max(f.x(), f.z()) == f.x()) {
			return X;
		} else {
			return Z;
		}
	} else {
		if(max(f.y(), f.z()) == f.y()) {
			return Y;
		} else {
			return Z;
		}
	}
}

CGRT_KERNEL_FUNC inline enum axis dimension(const point<3>& f) {
	if(max(f.x(), f.y()) == f.x()) {
		if(max(f.x(), f.z()) == f.x()) {
			return X;
		} else {
			return Z;
		}
	} else {
		if(max(f.y(), f.z()) == f.y()) {
			return Y;
		} else {
			return Z;
		}
	}
}

CGRT_KERNEL_FUNC inline scalar radians(scalar d) noexcept {
	return (CGRT_PI / 180.f) * clamp(d, 0.f, 360.f);
}

} // namespace cgrt::math

#endif //CGRT_GEOMETRY_HPP

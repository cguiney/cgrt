//
// Created by chris on 9/10/20.
//
#pragma once
#ifndef CGRT_ARRAY_HPP
#    define CGRT_ARRAY_HPP

#    include <algorithm>
#    include <cassert>
#    include <cgrt/math/scalar.hpp>

namespace cgrt::math {

template<typename S, size_t N, typename T = scalar>
class array;

template<typename R, typename T, typename U, typename BinaryOp>
CGRT_KERNEL_FUNC inline constexpr R zip(const T& lhs, const U& rhs, BinaryOp op) noexcept {
    auto result = R();
    auto dest = result.begin();
    auto left = lhs.cbegin();
    auto right = rhs.cbegin();
    const auto end = lhs.cend();
    for(; left < end; ++left, ++right, ++dest) {
        *dest = op(*left, *right);
    }
    return result;
}

struct component_operator {
    template<typename T, typename U>
    struct plus {
        [[nodiscard]] CGRT_KERNEL_FUNC constexpr inline auto operator()(const T& lhs, const U& rhs) noexcept {
            return lhs + rhs;
        }
    };

    template<typename T, typename U>
    struct minus {
        [[nodiscard]] CGRT_KERNEL_FUNC constexpr inline auto operator()(const T& lhs, const U& rhs) noexcept {
            return lhs - rhs;
        }
    };

    template<typename T, typename U>
    struct mul {
        [[nodiscard]] CGRT_KERNEL_FUNC constexpr inline auto operator()(const T& lhs, const U& rhs) noexcept {
            return lhs * rhs;
        }
    };

    template<typename T, typename U>
    struct div {
        [[nodiscard]] CGRT_KERNEL_FUNC constexpr inline auto operator()(const T& lhs, const U& rhs) noexcept {
            return lhs / rhs;
        }
    };
};

template<typename S, size_t N, typename T>
class array {
    // static_assert(std::is_arithmetic_v<T>, "value type must be arithmetic");
  public:
    using type = array<S, N, T>;
    using sub_type = S;
    using value_type = T;
    using pointer_type = T*;
    using reference_type = T&;
    using array_type = value_type[N]; // std::array<value_type, N>;
    using size_type = size_t;
    using iterator = pointer_type;
    using const_iterator = T const* const;
  private:
    array_type elements_;

  public:
    static const size_t length = N;

    template<typename... Vs>
    CGRT_KERNEL_FUNC constexpr inline explicit array(Vs... args) noexcept {
        // static_assert(sizeof...(args) <= N, "too many arguments in constructor");
        static_assert(sizeof...(args) == N || sizeof...(args) == 1, "incorrect number of arguments in constructor");
        if constexpr(sizeof...(args) == 1) {
            for(size_t i = 0; i < N; i++)
                ((this->elements_[i] = args), ...);
        } else {
            size_t i = 0;
            ((this->elements_[i++] = args), ...);
        }
    }


    CGRT_KERNEL_FUNC constexpr inline explicit array(std::initializer_list<T> list) noexcept {
        // static_assert(sizeof...(args) <= N, "too many arguments in constructor");
        assert(list.size() == N || list.size() == 1);
        size_t i = 0;
        for(auto&& el : list) {
            this->elements_[i++] = el;
        }
    }

    CGRT_KERNEL_FUNC constexpr explicit inline array(const value_type& fill = value_type{}) noexcept {
        for(size_t i = 0; i < N; i++)
            this->elements_[i] = fill;
    }

    CGRT_KERNEL_FUNC constexpr inline array(const type& other) noexcept {
        for(size_t i = 0; i < N; i++) {
            this->elements_[i] = other.elements_[i];
        }
    }

    template<typename U, typename... Vs>
    CGRT_KERNEL_FUNC static inline sub_type make(const U& other, Vs... args) noexcept {
        static_assert(U::length < N, "source array is smaller than destination array");
        sub_type v;
        for(size_t i = 0; i < U::length; i++) {
            v[i] = other[i];
        }

        size_t i = U::length;
        ((v.mutable_data()[i++] = args), ...);

        return v;
    }

    [[nodiscard]] CGRT_KERNEL_FUNC constexpr inline const_iterator cbegin() const noexcept {
        return elements_;
    }

    CGRT_KERNEL_FUNC constexpr inline const_iterator cend() const noexcept {
        return elements_ + N;
    }

    CGRT_KERNEL_FUNC constexpr inline iterator begin() noexcept {
        return elements_;
    }

    CGRT_KERNEL_FUNC constexpr inline iterator end() noexcept {
        return elements_ + N;
    }

    CGRT_KERNEL_FUNC constexpr inline value_type operator[](size_type n) const noexcept {
        assert(n < N);
        return elements_[n];
    }

    CGRT_KERNEL_FUNC inline reference_type operator[](size_type n) noexcept {
        assert(n < N);
        return elements_[n];
    }

    CGRT_KERNEL_FUNC constexpr inline const array_type& data() const noexcept {
        return elements_;
    }

    [[nodiscard]] CGRT_KERNEL_FUNC constexpr inline value_type x() const noexcept {
        return data()[0];
    }

    CGRT_KERNEL_FUNC constexpr inline value_type& mut_x() noexcept {
        return &elements_[0];
    }

    CGRT_KERNEL_FUNC constexpr inline value_type y() const noexcept {
        static_assert(N >= 2, "array type must have size of >= 2 to take the Y value");
        return data()[1];
    }

    CGRT_KERNEL_FUNC constexpr inline value_type& mut_y() noexcept {
        static_assert(N >= 2, "array type must have size of >= 2 to take the Y value");
        return &elements_[1];
    }

    [[nodiscard]] CGRT_KERNEL_FUNC constexpr inline value_type z() const noexcept {
        static_assert(N >= 3, "array type must have size of >= 3 to take the Z value");
        return data()[2];
    }

    CGRT_KERNEL_FUNC constexpr inline value_type& mut_z() noexcept {
        static_assert(N >= 3, "array type must have size of >= 3 to take the Z value");
        return &elements_[2];
    }

    CGRT_KERNEL_FUNC constexpr inline type& operator+=(const type& rhs) noexcept {
        for(size_t i = 0; i < N; i++)
            elements_[i] += rhs.elements_[i];
        return *this;
    }

    CGRT_KERNEL_FUNC constexpr inline type& operator+=(const value_type& rhs) noexcept {
        for(size_t i = 0; i < N; i++)
            elements_[i] += rhs;
        return *this;
    }

    CGRT_KERNEL_FUNC constexpr inline type& operator-=(const type& rhs) noexcept {
        for(size_t i = 0; i < N; i++)
            elements_[i] -= rhs.elements_[i];
        return *this;
    }

    CGRT_KERNEL_FUNC constexpr inline type& operator-=(const value_type& rhs) noexcept {
        for(size_t i = 0; i < N; i++)
            elements_[i] -= rhs;
        return *this;
    }

    CGRT_KERNEL_FUNC constexpr inline type& operator*=(const type& rhs) noexcept {
        for(size_t i = 0; i < N; i++)
            elements_[i] *= rhs.elements_[i];
        return *this;
    }

    CGRT_KERNEL_FUNC constexpr inline type& operator*=(const value_type& rhs) noexcept {
        for(size_t i = 0; i < N; i++)
            elements_[i] *= rhs;
        return *this;
    }

    CGRT_KERNEL_FUNC constexpr inline type& operator/=(const type& rhs) noexcept {
        for(size_t i = 0; i < N; i++)
            elements_[i] /= rhs.elements_[i];
        return *this;
    }

    CGRT_KERNEL_FUNC constexpr inline type& operator/=(const value_type& rhs) noexcept {
        for(size_t i = 0; i < N; i++)
            elements_[i] /= rhs;
        return *this;
    }

    CGRT_KERNEL_FUNC friend inline sub_type operator+(sub_type lhs, const type& rhs) noexcept {
        lhs += rhs;
        return lhs;
    }

    CGRT_KERNEL_FUNC friend inline sub_type operator+(sub_type lhs, const value_type& rhs) noexcept {
        lhs += rhs;
        return lhs;
    }

    CGRT_KERNEL_FUNC friend inline sub_type operator-(sub_type lhs, const type& rhs) noexcept {
        lhs -= rhs;
        return lhs;
    }

    CGRT_KERNEL_FUNC friend inline sub_type operator-(sub_type lhs, const value_type& rhs) noexcept {
        lhs -= rhs;
        return lhs;
    }

    CGRT_KERNEL_FUNC friend inline sub_type operator*(sub_type lhs, const sub_type& rhs) noexcept {
        lhs *= rhs;
        return lhs;
    }

    CGRT_KERNEL_FUNC friend inline sub_type operator*(sub_type lhs, const value_type& rhs) noexcept {
        lhs *= rhs;
        return lhs;
    }

    CGRT_KERNEL_FUNC friend inline sub_type operator/(sub_type lhs, const sub_type& rhs) noexcept {
        lhs /= rhs;
        return lhs;
    }

    CGRT_KERNEL_FUNC friend inline sub_type operator/(sub_type lhs, const value_type& rhs) noexcept {
        lhs /= rhs;
        return lhs;
    }

    CGRT_KERNEL_FUNC friend inline bool operator==(const type& lhs, const type& rhs) noexcept {
        for(size_t i = 0; i < N; i++) {
            if(lhs[i] != rhs[i]) {
                return false;
            }
        }

        return true;
    }

    CGRT_KERNEL_FUNC friend inline bool operator!=(const type& lhs, const type& rhs) noexcept {
        return !(lhs == rhs);
    }

  protected:
    CGRT_KERNEL_FUNC inline array_type& mutable_data() noexcept {
        return elements_;
    }
};

} // namespace cgrt::math

#endif //CGRT_ARRAY_HPP

//
// Created by chris on 11/28/20.
//

#ifndef CGRT_TRANSFORM_HPP
#define CGRT_TRANSFORM_HPP

#include <cgrt/math/matrix.hpp>
#include <cgrt/math/point.hpp>
#include <cgrt/math/ray.hpp>
#include <utility>

namespace cgrt::math::transform {

class matrix;

class view;

CGRT_KERNEL_FUNC constexpr inline view inverse(const matrix&);

CGRT_KERNEL_FUNC constexpr inline view inverse(const view&);

CGRT_KERNEL_FUNC constexpr inline view transpose(const matrix&);

CGRT_KERNEL_FUNC constexpr inline view transpose(const view&);

class view {
	using matrix_type = cgrt::math::matrix<4, 4>;
	using vector_type = typename matrix_type::vector_type;
	const matrix_type* matrix_;
	const matrix_type* inverted_;
	const matrix_type* transposed_;
	const matrix_type* inverse_transposed_;

  public:
	CGRT_KERNEL_FUNC constexpr inline view(
        const matrix_type* matrix,
        const matrix_type* inverse,
        const matrix_type* transposed,
        const matrix_type* inverse_transposed
    ) : matrix_(matrix), inverted_(inverse), transposed_(transposed), inverse_transposed_(inverse_transposed)
    {}

	CGRT_KERNEL_FUNC inline point<3> operator()(const point<3>& in) const {
		auto v = vector_type(in.x(), in.y(), in.z(), 1);
		auto x = (*matrix_) * v;
		return point<3>{x.x(), x.y(), x.z()};
	}

	CGRT_KERNEL_FUNC inline vector<3> operator()(const vector<3>& in) const {
		auto v = vector_type(in.x(), in.y(), in.z(), 0);
		auto x = (*matrix_) * v;
		return vector<3>{x.x(), x.y(), x.z()};
	}

	CGRT_KERNEL_FUNC inline ray<3> operator()(const ray<3>& in) const {
		return ray<3>{(*this)(in.origin), (*this)(in.direction)};
	}

	CGRT_KERNEL_FUNC inline const vector_type operator[](size_t i) const {
		return (*matrix_)[i];
	}

	CGRT_KERNEL_FUNC constexpr friend view inverse(const view& v);

	CGRT_KERNEL_FUNC constexpr friend view transpose(const view& v);
};

class matrix {
  public:
	using matrix_type = cgrt::math::matrix<4, 4>;

  private:
	matrix_type matrix_;
	matrix_type inverted_;
	matrix_type transposed_;
	matrix_type inverse_transposed_;

	CGRT_KERNEL_FUNC inline void rebuild() {
		inverted_ = math::inverse(matrix_);
		transposed_ = math::transpose(matrix_);
		inverse_transposed_ = math::inverse(math::transpose(matrix_));
	}

	class mutator {
		using vector_type = typename matrix_type::vector_type;

		matrix* parent;
		vector_type* row;

	  public:
		CGRT_KERNEL_FUNC constexpr inline mutator(matrix* parent, vector_type* row): parent(parent), row(row) {
		}

		CGRT_KERNEL_FUNC constexpr inline mutator(mutator&& other) noexcept: parent(other.parent), row(other.row) {
			other.parent = nullptr;
			other.row = nullptr;
		}

		CGRT_KERNEL_FUNC constexpr inline mutator& operator=(mutator&& other) noexcept {
			parent = other.parent;
			row = other.row;
			other.parent = nullptr;
			other.row = nullptr;
			return *this;
		}

		CGRT_KERNEL_FUNC constexpr inline vector_type& operator[](size_t i) {
			return row[i];
		}

		CGRT_KERNEL_FUNC constexpr inline const vector_type& operator[](size_t i) const {
			return row[i];
		}

		CGRT_KERNEL_FUNC inline ~mutator() {
			if(parent)
				parent->rebuild();
		}
	};

  public:
	CGRT_KERNEL_FUNC matrix()
	    : matrix_(math::identity<4>()), inverted_(math::identity<4>()), transposed_(math::identity<4>()),
	      inverse_transposed_(math::identity<4>()) {
	}

	CGRT_KERNEL_FUNC inline matrix(const matrix_type& o): matrix_(o) {
		rebuild();
	}

    inline ~matrix() = default;

	[[nodiscard]] CGRT_KERNEL_FUNC constexpr inline const matrix_type& mtx() const {
		return matrix_;
	}

	[[nodiscard]] CGRT_KERNEL_FUNC constexpr inline const matrix_type& inverted() const {
		return inverted_;
	}

	[[nodiscard]] CGRT_KERNEL_FUNC constexpr inline const matrix_type& transposed() const {
		return transposed_;
	}

	[[nodiscard]] CGRT_KERNEL_FUNC constexpr inline const matrix_type& inverse_transposed() const {
		return inverse_transposed_;
	}

	CGRT_KERNEL_FUNC inline matrix operator*(const matrix& other) const noexcept {
		return matrix(matrix_ * other.matrix_);
	}

	CGRT_KERNEL_FUNC inline matrix::mutator operator[](size_t i) {
		return {this, &matrix_[i]};
	}

	CGRT_KERNEL_FUNC inline const matrix_type::vector_type operator[](size_t i) const {
		return matrix_[i];
	}

	template<typename T>
	CGRT_KERNEL_FUNC inline T operator()(const T& v) const {
		auto vw = view(&matrix_, &inverted_, &transposed_, &inverse_transposed_);
		return vw(v);
	}

	CGRT_KERNEL_FUNC constexpr inline friend view inverse(const matrix& m);

	CGRT_KERNEL_FUNC constexpr inline friend view transpose(const matrix& m);

	CGRT_KERNEL_FUNC constexpr inline friend bool operator==(const matrix& lhs, const matrix_type& rhs);
};

CGRT_KERNEL_FUNC constexpr inline view inverse(const matrix& m) {
	return {&m.inverted_, &m.matrix_, &m.inverse_transposed_, &m.transposed_};
}

CGRT_KERNEL_FUNC constexpr inline view inverse(const view& v) {
	return {v.inverted_, v.matrix_, v.inverse_transposed_, v.transposed_};
}

CGRT_KERNEL_FUNC constexpr inline view transpose(const matrix& m) {
	return {&m.transposed_, &m.inverse_transposed_, &m.matrix_, &m.inverted_};
}

CGRT_KERNEL_FUNC constexpr inline view transpose(const view& v) {
	return {v.transposed_, v.inverse_transposed_, v.matrix_, v.inverted_};
}

CGRT_KERNEL_FUNC inline matrix translation(scalar x, scalar y, scalar z) {
	auto m = math::identity<4>();
	m[0][3] = x;
	m[1][3] = y;
	m[2][3] = z;
	return m;
}

CGRT_KERNEL_FUNC inline matrix scale(scalar x, scalar y, scalar z) {
	auto m = math::identity<4>();
	m[0][0] = x;
	m[1][1] = y;
	m[2][2] = z;
	return m;
}

CGRT_KERNEL_FUNC inline matrix rotate_x(scalar radians) {
	auto m = math::identity<4>();
	m[1][1] = std::cos(radians);
	m[1][2] = -std::sin(radians);
	m[2][1] = std::sin(radians);
	m[2][2] = std::cos(radians);
	return m;
}

CGRT_KERNEL_FUNC inline matrix rotate_y(scalar radians) {
	auto m = math::identity<4>();
	m[0][0] = std::cos(radians);
	m[0][2] = std::sin(radians);
	m[2][0] = -std::sin(radians);
	m[2][2] = std::cos(radians);
	return m;
}

CGRT_KERNEL_FUNC inline matrix rotate_z(scalar radians) {
	auto m = math::identity<4>();
	m[0][0] = std::cos(radians);
	m[0][1] = -std::sin(radians);
	m[1][0] = std::sin(radians);
	m[1][1] = std::cos(radians);
	return m;
}

CGRT_KERNEL_FUNC inline matrix rotate_x(const math::matrix<4, 4>& m, scalar radians) {
	return m * rotate_x(radians).mtx();
}

CGRT_KERNEL_FUNC inline matrix rotate_y(const math::matrix<4, 4>& m, scalar radians) {
	return m * rotate_y(radians).mtx();
}

CGRT_KERNEL_FUNC inline matrix rotate_z(const math::matrix<4, 4>& m, scalar radians) {
	return m * rotate_z(radians).mtx();
}

CGRT_KERNEL_FUNC inline matrix shear(scalar xy, scalar xz, scalar yx, scalar yz, scalar zx, scalar zy) {
	auto m = math::identity<4>();
	m[0][1] = xy;
	m[0][2] = xz;
	m[1][0] = yx;
	m[1][2] = yz;
	m[2][0] = zx;
	m[2][1] = zy;
	return m;
}

CGRT_KERNEL_FUNC inline matrix camera_view(const point<3>& from, const point<3>& to, const vector<3>& up) {
	// form coordinate system
	auto forward = math::normalize(to - from);
	auto left = math::cross(forward, normalize(up));
	auto true_up = math::cross(left, forward);

	auto orientation = math::identity<4>();
	orientation[0][0] = left[0];
	orientation[0][1] = left[1];
	orientation[0][2] = left[2];

	orientation[1][0] = true_up[0];
	orientation[1][1] = true_up[1];
	orientation[1][2] = true_up[2];

	orientation[2][0] = -forward[0];
	orientation[2][1] = -forward[1];
	orientation[2][2] = -forward[2];

	auto t = matrix(orientation) * translation(-from[0], -from[1], -from[2]);
	return t;
}

CGRT_KERNEL_FUNC constexpr inline bool operator==(const matrix& lhs, const typename matrix::matrix_type& rhs) {
	return lhs.matrix_ == rhs;
}

} // namespace cgrt::math::transform

#endif //CGRT_TRANSFORM_HPP

//
// Created by chris on 9/11/20.
//

#ifndef CGRT_SCALAR_HPP
#define CGRT_SCALAR_HPP

#include <cgrt/runtime.hpp>

#include <cassert>
#include <cmath>
#include <ostream>
#include <type_traits>
#include <sstream>

namespace cgrt::math {

struct scalar;

CGRT_DEVICE static constexpr const float epsilon = 0.00001f;
CGRT_KERNEL_FUNC inline bool scalar_isinf(const math::scalar& v) noexcept;
CGRT_KERNEL_FUNC inline bool scalar_isnan(const math::scalar& v) noexcept;

struct scalar {
	using value_type = float;
	using type = float;

	value_type value;

	CGRT_KERNEL_FUNC constexpr inline scalar() noexcept: value(0.0f) {
	}

    inline ~scalar() = default;

    CGRT_KERNEL_FUNC inline scalar(const scalar& other) noexcept : value(other.value) {
      assert(!::cgrt::math::scalar_isnan(value));
      assert(!::cgrt::math::scalar_isinf(value));
    };

	CGRT_KERNEL_FUNC inline scalar(float f) noexcept: value(static_cast<value_type>(f)) {}

	CGRT_KERNEL_FUNC constexpr inline explicit scalar(double d) noexcept: value(static_cast<value_type>(d)) {
	}

	CGRT_KERNEL_FUNC constexpr inline explicit scalar(int i) noexcept: value(static_cast<value_type>(i)) {
	}

	CGRT_KERNEL_FUNC constexpr inline explicit scalar(long i) noexcept: value(static_cast<value_type>(i)) {
	}

	CGRT_KERNEL_FUNC constexpr inline explicit scalar(long long i) noexcept: value(static_cast<value_type>(i)) {
	}

	CGRT_KERNEL_FUNC constexpr inline explicit scalar(unsigned i) noexcept: value(static_cast<value_type>(i)) {
	}

	CGRT_KERNEL_FUNC constexpr inline explicit scalar(unsigned long i) noexcept: value(static_cast<value_type>(i)) {
	}

	CGRT_KERNEL_FUNC constexpr inline explicit scalar(unsigned long long i) noexcept: value(static_cast<value_type>(i)) {
	}

	CGRT_KERNEL_FUNC constexpr inline operator float() const {
		return static_cast<float>(value);
	}

	CGRT_KERNEL_FUNC constexpr explicit inline operator double() const {
		return static_cast<double>(value);
	}

	CGRT_KERNEL_FUNC constexpr explicit inline operator int() const {
		return static_cast<int>(value);
	}

	CGRT_KERNEL_FUNC constexpr explicit inline operator unsigned int() const {
		return static_cast<unsigned int>(value);
	}

	CGRT_KERNEL_FUNC constexpr explicit inline operator long() const {
		return static_cast<long>(value);
	}

	CGRT_KERNEL_FUNC constexpr explicit inline operator unsigned long() const {
		return static_cast<unsigned long>(value);
	}


	template<typename T>
	CGRT_KERNEL_FUNC inline scalar& operator=(T v) noexcept {
		// static_assert(std::is_arithmetic_v<T>, "type must be arithmetic");
        value = static_cast<value_type>(v);
	    assert(!::cgrt::math::scalar_isnan(*this));
	    assert(!::cgrt::math::scalar_isinf(*this));
		return *this;
	}

	/* scalar -> scalar operators
     *
     * in order:
     *  <, <=, >, >=, ==, !=
     *  +, -, *, /
     *  +=, -=, *=, /=
     *
     *  todo? ++? --?
     * */

	CGRT_KERNEL_FUNC constexpr inline friend bool operator<(const scalar& lhs, const scalar& rhs) noexcept {
		return lhs.value < rhs.value;
	}

	CGRT_KERNEL_FUNC constexpr inline friend bool operator<=(const scalar& lhs, const scalar& rhs) noexcept {
		return lhs.value <= rhs.value;
	}

	CGRT_KERNEL_FUNC constexpr inline friend bool operator>(const scalar& lhs, const scalar& rhs) noexcept {
		return lhs.value > rhs.value;
	}

	CGRT_KERNEL_FUNC constexpr inline friend bool operator>=(const scalar& lhs, const scalar& rhs) noexcept {
		return lhs.value >= rhs.value;
	}

	CGRT_KERNEL_FUNC inline friend bool operator==(const scalar& lhs, const scalar& rhs) noexcept {
		return fabsf(lhs.value - rhs.value) < epsilon;
	}

	CGRT_KERNEL_FUNC inline friend bool operator!=(const scalar& lhs, const scalar& rhs) noexcept {
		return !(lhs == rhs);
	}

	CGRT_KERNEL_FUNC inline friend scalar operator+(const scalar& lhs, const scalar& rhs) noexcept {
		return lhs.value + rhs.value;
	}

	CGRT_KERNEL_FUNC inline friend scalar operator-(const scalar& lhs, const scalar& rhs) noexcept {
		return lhs.value - rhs.value;
	}

	CGRT_KERNEL_FUNC inline friend scalar operator*(const scalar& lhs, const scalar& rhs) noexcept {
		return lhs.value * rhs.value;
	}

	CGRT_KERNEL_FUNC inline friend scalar operator/(const scalar& lhs, const scalar& rhs) noexcept {
		return lhs.value / rhs.value;
	}

	CGRT_KERNEL_FUNC inline friend scalar operator+=(scalar& lhs, const scalar& rhs) noexcept {
		return lhs.value += rhs.value;
	}

	CGRT_KERNEL_FUNC inline friend scalar operator-=(scalar& lhs, const scalar& rhs) noexcept {
		return lhs.value -= rhs.value;
	}

	CGRT_KERNEL_FUNC inline friend scalar operator*=(scalar& lhs, const scalar& rhs) noexcept {
		return lhs.value *= rhs.value;
	}

	CGRT_KERNEL_FUNC inline friend scalar operator/=(scalar& lhs, const scalar& rhs) noexcept {
		return lhs.value /= rhs.value;
	}

	/*
     * scalar -> scalar::value_type operators
     */
	template<typename T, std::enable_if_t<std::is_arithmetic_v<T>, bool> = true>
	CGRT_KERNEL_FUNC inline friend bool operator<(const scalar& lhs, const T& rhs) {
		static_assert(std::is_arithmetic_v<T>);
		return lhs < scalar{rhs};
	}

	template<typename T, std::enable_if_t<std::is_arithmetic_v<T>, bool> = true>
	CGRT_KERNEL_FUNC inline friend bool operator<=(const scalar& lhs, const T& rhs) {
		static_assert(std::is_arithmetic_v<T>);
		return lhs <= scalar{rhs};
	}

	template<typename T, std::enable_if_t<std::is_arithmetic_v<T>, bool> = true>
	CGRT_KERNEL_FUNC inline friend bool operator>(const scalar& lhs, const T& rhs) {
		static_assert(std::is_arithmetic_v<T>);
		return lhs > scalar{rhs};
	}

	template<typename T, std::enable_if_t<std::is_arithmetic_v<T>, bool> = true>
	CGRT_KERNEL_FUNC inline friend bool operator>=(const scalar& lhs, const T& rhs) {
		static_assert(std::is_arithmetic_v<T>);
		return lhs >= scalar{rhs};
	}

	template<typename T, std::enable_if_t<std::is_arithmetic_v<T>, bool> = true>
	CGRT_KERNEL_FUNC inline friend bool operator==(const scalar& lhs, const T& rhs) {
		static_assert(std::is_arithmetic_v<T>);
		return lhs == scalar{rhs};
	}

	template<typename T, std::enable_if_t<std::is_arithmetic_v<T>, bool> = true>
	CGRT_KERNEL_FUNC inline friend bool operator!=(const scalar& lhs, const T& rhs) {
		static_assert(std::is_arithmetic_v<T>);
		return !(lhs == scalar{rhs});
	}

	template<typename T, std::enable_if_t<std::is_arithmetic_v<T>, bool> = true>
	CGRT_KERNEL_FUNC constexpr inline friend scalar operator+(const scalar& lhs, const T& rhs) noexcept {
		static_assert(std::is_arithmetic_v<T>);
		return lhs + scalar{rhs};
	}

	template<typename T, std::enable_if_t<std::is_arithmetic_v<T>, bool> = true>
	CGRT_KERNEL_FUNC constexpr inline friend scalar operator-(const scalar& lhs, const T& rhs) noexcept {
		static_assert(std::is_arithmetic_v<T>);
		return lhs - scalar{rhs};
	}

	template<typename T, std::enable_if_t<std::is_arithmetic_v<T>, bool> = true>
	CGRT_KERNEL_FUNC constexpr inline friend scalar operator*(const scalar& lhs, const T& rhs) noexcept {
		static_assert(std::is_arithmetic_v<T>);
		return lhs * scalar{rhs};
	}

	template<typename T, std::enable_if_t<std::is_arithmetic_v<T>, bool> = true>
	CGRT_KERNEL_FUNC constexpr inline friend scalar operator/(const scalar& lhs, const T& rhs) noexcept {
		static_assert(std::is_arithmetic_v<T>);
		return lhs / scalar{rhs};
	}

	template<typename T, std::enable_if_t<std::is_arithmetic_v<T>, bool> = true>
	CGRT_KERNEL_FUNC constexpr inline friend scalar operator+=(scalar& lhs, const T& rhs) noexcept {
		static_assert(std::is_arithmetic_v<T>);
		return lhs += scalar{rhs};
	}

	template<typename T, std::enable_if_t<std::is_arithmetic_v<T>, bool> = true>
	CGRT_KERNEL_FUNC constexpr inline friend scalar operator-=(scalar& lhs, const T& rhs) noexcept {
		static_assert(std::is_arithmetic_v<T>);
		return lhs -= scalar{rhs};
	}

	template<typename T, std::enable_if_t<std::is_arithmetic_v<T>, bool> = true>
	CGRT_KERNEL_FUNC constexpr inline friend scalar operator*=(scalar& lhs, const T& rhs) noexcept {
		static_assert(std::is_arithmetic_v<T>);
		return lhs.value *= rhs;
	}

	template<typename T, std::enable_if_t<std::is_arithmetic_v<T>, bool> = true>
	CGRT_KERNEL_FUNC constexpr inline friend scalar operator/=(scalar& lhs, const T& rhs) noexcept {
		static_assert(std::is_arithmetic_v<T>);
		return lhs /= scalar{rhs};
	}

	/*
 * scalar::value_type -> scalar operators
 */
	template<typename T, std::enable_if_t<std::is_arithmetic_v<T>, bool> = true>
	CGRT_KERNEL_FUNC inline friend bool operator<(const T& lhs, const scalar& rhs) {
		static_assert(std::is_arithmetic_v<T>);
		return scalar{lhs} < rhs;
	}

	template<typename T, std::enable_if_t<std::is_arithmetic_v<T>, bool> = true>
	CGRT_KERNEL_FUNC inline friend bool operator<=(const T& lhs, const scalar& rhs) {
		static_assert(std::is_arithmetic_v<T>);
		return scalar{lhs} <= rhs;
	}

	template<typename T, std::enable_if_t<std::is_arithmetic_v<T>, bool> = true>
	CGRT_KERNEL_FUNC inline friend bool operator>(const T& lhs, const scalar& rhs) {
		static_assert(std::is_arithmetic_v<T>);
		return scalar{lhs} > rhs;
	}

	template<typename T, std::enable_if_t<std::is_arithmetic_v<T>, bool> = true>
	CGRT_KERNEL_FUNC inline friend bool operator>=(const T& lhs, const scalar& rhs) {
		static_assert(std::is_arithmetic_v<T>);
		return scalar{lhs} >= rhs;
	}

	template<typename T, std::enable_if_t<std::is_arithmetic_v<T>, bool> = true>
	CGRT_KERNEL_FUNC inline friend bool operator==(const T& lhs, const scalar& rhs) {
		static_assert(std::is_arithmetic_v<T>);
		return scalar{lhs} == rhs;
	}

	template<typename T, std::enable_if_t<std::is_arithmetic_v<T>, bool> = true>
	CGRT_KERNEL_FUNC inline friend bool operator!=(const T& lhs, const scalar& rhs) {
		static_assert(std::is_arithmetic_v<T>);
		return !(scalar{lhs} == rhs);
	}

	template<typename T, std::enable_if_t<std::is_arithmetic_v<T>, bool> = true>
	CGRT_KERNEL_FUNC constexpr inline friend scalar operator+(const T& lhs, const scalar& rhs) noexcept {
		static_assert(std::is_arithmetic_v<T>);
		return scalar{lhs} + rhs;
	}

	template<typename T, std::enable_if_t<std::is_arithmetic_v<T>, bool> = true>
	CGRT_KERNEL_FUNC constexpr inline friend scalar operator-(const T& lhs, const scalar& rhs) noexcept {
		static_assert(std::is_arithmetic_v<T>);
		return scalar{lhs} - rhs;
	}

	template<typename T, std::enable_if_t<std::is_arithmetic_v<T>, bool> = true>
	CGRT_KERNEL_FUNC constexpr inline friend scalar operator*(const T& lhs, const scalar& rhs) noexcept {
		static_assert(std::is_arithmetic_v<T>);
		return scalar{lhs} * rhs;
	}

	template<typename T, std::enable_if_t<std::is_arithmetic_v<T>, bool> = true>
	CGRT_KERNEL_FUNC constexpr inline friend scalar operator/(const T& lhs, const scalar& rhs) noexcept {
		static_assert(std::is_arithmetic_v<T>);
		return scalar{lhs} / rhs;
	}

    /*
     * Unary Operators: -
     */
    CGRT_KERNEL_FUNC inline friend scalar operator-(const scalar& v) noexcept {
	return scalar{} - v;
    }

	inline friend std::ostream& operator<<(std::ostream& o, const scalar& x) {
		o << x.value;
		return o;
	}

	[[nodiscard]] std::string repr() const {
		auto ss = std::stringstream{};
		ss << value;
		return ss.str();
	}
};

CGRT_KERNEL_FUNC inline math::scalar clamp(math::scalar x, math::scalar low, math::scalar high)
{
	if (x < low)
		return low;
	if (x > high)
		return high;
	return x;
}

CGRT_KERNEL_FUNC inline bool fisinf(float v) noexcept {
#if CGRT_CUDA
    return ::isinf(v);
#else
    return std::isinf(v);
#endif
}

CGRT_KERNEL_FUNC inline bool scalar_isinf(const math::scalar& v) noexcept {
    return fisinf(v.value);
}

CGRT_KERNEL_FUNC inline bool scalar_isnan(const math::scalar& v) noexcept {
#if CGRT_CUDA
    return ::isnan(v.value);
#else
     return std::isnan(v.value);
#endif
}

} // namespace cgrt::math

#endif //CGRT_SCALAR_HPP

//
// Created by chris on 9/12/20.
//

#ifndef CGRT_MATRIX_HPP
#define CGRT_MATRIX_HPP

#include "vector.hpp"

#include <array>
#include <cassert>
#include <iostream>

namespace cgrt::math {

template<size_t M, size_t N>
class matrix;

template<size_t M, size_t N>
CGRT_KERNEL_FUNC constexpr inline matrix<N, M> transpose(const matrix<M, N>& in);

template<size_t N>
CGRT_KERNEL_FUNC constexpr inline matrix<N, N> transpose(const matrix<N, N>& in);

template<size_t M, size_t N>
CGRT_KERNEL_FUNC constexpr inline scalar dot(const array<matrix<M, N>, N>& a, const array<matrix<M, N>, N>& b) {
	scalar out{};
	for(size_t i = 0; i < N; i++)
		out += a[i] * b[i];
	return out;
}

template<size_t M, size_t N>
class matrix {
	static_assert(N > 0, "matrix dimensions must be > 0");
	static_assert(M > 0, "matrix dimensions must be > 0");

  public:
	using vector_type = array<matrix<M, N>, M>;
	using matrix_type = vector_type[N];

  private:
	matrix_type matrix_;
	constexpr static const size_t columns_ = N;
	constexpr static const size_t rows_ = M;

  public:
	CGRT_KERNEL_FUNC constexpr inline explicit matrix(scalar fill = 0.f) {
		for(size_t c = 0; c < columns_; c++) {
			for(auto& v : matrix_[c]) {
				v = fill;
			}
		}
	}

	CGRT_KERNEL_FUNC constexpr inline matrix(const std::initializer_list<std::initializer_list<scalar>>& v) {
		auto m = 0, n = 0;
		assert(v.size() == N);

		for(auto row : v) {
		  assert(row.size() == M);

		  m = 0;
		  for(const auto& val : row) {
		    matrix_[n][m] = val;
		    m++;
		  }
		  n++;
		}
	}

	CGRT_KERNEL_FUNC constexpr inline bool operator==(const matrix<M, N>& o) const {
		for(size_t n = 0; n < N; n++) {
			for(size_t m = 0; m < M; m++) {
				if(matrix_[n][m] != o.matrix_[n][m])
					return false;
			}
		}
		return true;
	}

	CGRT_KERNEL_FUNC constexpr inline bool operator!=(const matrix& o) const {
		return !(*this == o);
	}

	CGRT_KERNEL_FUNC constexpr inline matrix<M, N> operator*(const matrix<M, N>& o) const {
		matrix<M, N> out;

		for(size_t i = 0; i < M; i++) {
			for(size_t j = 0; j < N; j++) {
				scalar sum;
				for(size_t k = 0; k < M; k++) {
					sum += matrix_[i][k] * o[k][j];
				}
				out[i][j] = sum;
			}
		}
		return out;
	}

	CGRT_KERNEL_FUNC constexpr inline vector_type operator*(const vector_type& o) const {
		vector_type out;
		for(size_t i = 0; i < M; i++) {
			out[i] = dot(matrix_[i], o);
		}

		return out;
	};

	CGRT_KERNEL_FUNC constexpr inline vector_type& operator[](size_t i) {
		assert(i < N);
		return matrix_[i];
	}

	CGRT_KERNEL_FUNC constexpr inline const vector_type& operator[](size_t i) const {
		assert(i < N);
		return matrix_[i];
	}

	CGRT_KERNEL_FUNC constexpr friend matrix<N, M> transpose<M, N>(const matrix<M, N>& in);

	CGRT_KERNEL_FUNC constexpr friend matrix<N, N> transpose<N>(const matrix<N, N>& in);
};

template<size_t M, size_t N>
CGRT_KERNEL_FUNC constexpr inline matrix<N, M> transpose(const matrix<M, N>& in) {
	auto out = matrix<N, M>{};

	/**
	 *   m=3,n=2                              m=2,n=3
	 *   ---------------                     ---------------
	 *   | n0,m0 | n0,m1 | n0,m2 |          | n0,m0 | n1,m0
	 *   | n1,m0 | n1,m1 | n0,m2 |          | n0,m1 | n1,m1
	 *                                      | n0,m2 | n1,m2
	 *
	 */

	for(size_t n = 0; n < N; n++) {
		for(size_t m = 0; m < M; m++) {
			out[m][n] = in[n][m];
		}
	}

	return out;
}

template<size_t N>
CGRT_KERNEL_FUNC constexpr inline matrix<N, N> transpose(const matrix<N, N>& in) {
	return transpose<N, N>(in);
}

CGRT_KERNEL_FUNC inline matrix<1, 1> submatrix(const matrix<1, 1>& in) {
	return in;
}

template<size_t N>
CGRT_KERNEL_FUNC constexpr inline matrix<N - 1, N - 1> submatrix(const matrix<N, N>& in, size_t cut_row,
								 size_t cut_col) {
	assert(cut_row < N);
	assert(cut_col < N);

	auto out = matrix<N - 1, N - 1>{};

	for(size_t r = 0, i = 0; r < N; r++) {
		if(r == cut_row)
			continue;

		for(size_t c = 0, j = 0; c < N; c++) {
			if(c == cut_col)
				continue;

			out[i][j] = in[r][c];

			j++;
		}

		i++;
	}

	return out;
}

template<size_t N>
CGRT_KERNEL_FUNC constexpr inline scalar determinant(const matrix<N, N>&);

template<size_t N>
CGRT_KERNEL_FUNC constexpr inline scalar cofactor(const matrix<N, N>& in, size_t row, size_t col);

template<size_t N>
CGRT_KERNEL_FUNC constexpr inline scalar minor(const matrix<N, N>& in, size_t row, size_t col);

template<size_t N>
CGRT_KERNEL_FUNC constexpr inline scalar determinant(const matrix<N, N>& in) {
	if constexpr(N < 2) {
		static_assert(N >= 2, "matrix size must be a square >= 2");
	}

	if constexpr(N == 2) {
		return (in[0][0] * in[1][1]) - (in[0][1] * in[1][0]);
	}

	if constexpr(N > 2) {
		scalar d;
		for(size_t i = 0; i < N; i++) {
			d = d + in[0][i] * cofactor(in, 0, i);
		}
		return d;
	}
};

template<size_t N>
CGRT_KERNEL_FUNC constexpr inline scalar cofactor(const matrix<N, N>& in, size_t row, size_t col) {
	scalar m = (row + col) % 2 == 0 ? 1.f : -1.f;
	return m * minor(in, row, col);
}

template<size_t N>
CGRT_KERNEL_FUNC constexpr inline scalar minor(const matrix<N, N>& in, size_t row, size_t col) {
	return determinant(submatrix(in, row, col));
}

template<size_t M, size_t N>
CGRT_KERNEL_FUNC constexpr inline matrix<M, N> inverse(const matrix<M, N>& in) {
	auto d = determinant(in);
	assert(d != 0.f);

	auto out = matrix<M, N>();
	for(size_t i = 0; i < M; i++) {
		for(size_t j = 0; j < N; j++) {
			auto c = cofactor(in, i, j);
			out[j][i] = c / d;
		}
	}

	return out;
}

template<size_t N>
CGRT_KERNEL_FUNC constexpr inline matrix<N, N> identity() {
	auto m = matrix<N, N>();
	for(size_t i = 0; i < N; i++)
		m[i][i] = 1.f;

	return m;
}
} // namespace cgrt::math
#endif //CGRT_MATRIX_HPP

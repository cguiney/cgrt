//
// Created by chris on 1/16/21.
//

#ifndef CGRT_RADIOSITY_HPP
#define CGRT_RADIOSITY_HPP

#include <cgrt/math/array.hpp>
#include <cgrt/math/scalar.hpp>

namespace cgrt::math {

class spectrum : public math::array<spectrum, 3> {
    using base_type = math::array<spectrum, 3>;

  public:

    CGRT_KERNEL_FUNC constexpr inline spectrum(const spectrum& other) noexcept
        : base_type(other[0], other[1], other[2]) {
    }

    CGRT_KERNEL_FUNC inline spectrum(const math::scalar& fill = 0.f) noexcept: base_type(fill, fill, fill) {
    }

    // CGRT_KERNEL_FUNC inline spectrum(float fill) noexcept: base_type(fill, fill, fill) {
    // }

    CGRT_KERNEL_FUNC inline spectrum(const math::scalar& x, const math::scalar& y, const math::scalar& z) noexcept
        : base_type(x, y, z) {
    }
};

inline std::ostream& operator<<(std::ostream& os, const spectrum& v) {
    os << "{";

    size_t i = 0;
    for(const auto& x : v.data()) {
        os << x;
        if(i++ < 3 - 1)
            os << " ";
    }

    os << "}";

    return os;
}

} // namespace cgrt::math

#endif //CGRT_RADIOSITY_HPP

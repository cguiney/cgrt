//
// Created by chris on 9/19/20.
//

#ifndef CGRT_POINT_HPP
#define CGRT_POINT_HPP

#include "operators.hpp"
#include <cgrt/math/array.hpp>
#include <cgrt/math/scalar.hpp>
#include <cgrt/math/vector.hpp>


namespace cgrt::math {

template<size_t N, typename T = scalar>
struct point : array<point<N, T>, N, T> {
    using type = point<N, T>;
	using base_type = array<type, N, T>;

    CGRT_KERNEL_FUNC inline point() : base_type() {}

	template<typename... Vs>
	CGRT_KERNEL_FUNC inline explicit point(Vs... args): base_type(args...) {}

	CGRT_KERNEL_FUNC static type component_min(const type& a, const type& b) {
		auto m = type{};
		for(size_t i = 0; i < N; i++) {
			m[i] = fmin(a[i], b[i]);
		};
		return m;
	}

	CGRT_KERNEL_FUNC static type component_max(const type& a, const type& b) {
		auto m = type{};
		for(size_t i = 0; i < N; i++) {
			m[i] = fmax(a[i], b[i]);
		};
		return m;
	}

	CGRT_KERNEL_FUNC friend inline type operator+(const type& lhs, const type& rhs) noexcept {
		return zip<type>(lhs, rhs, plus<scalar>{});
	}

	CGRT_KERNEL_FUNC friend inline type operator+(const type& lhs, const vector<N>& rhs) noexcept {
		return zip<type>(lhs, rhs, plus<scalar>{});
	}

	CGRT_KERNEL_FUNC friend inline type operator+(const vector<N>& lhs, const type& rhs) noexcept {
		return zip<type>(lhs, rhs, plus<scalar>{});
	}

	CGRT_KERNEL_FUNC friend inline vector<N> operator-(const type& lhs, const type& rhs) noexcept {
		return zip<vector<N>>(lhs, rhs, minus<scalar>{});
	}

	CGRT_KERNEL_FUNC friend inline type operator-(const type& lhs, const vector<N>& rhs) noexcept {
		return zip<type>(lhs, rhs, minus<scalar>{});
	}

	friend inline std::ostream& operator<<(std::ostream& os, const type& v) {
		os << "{";

		size_t i = 0;
		for(const auto& x : v.data()) {
			os << x;
			if(i++ < N - 1)
				os << " ";
		}
		os << "}";

		return os;
	}

	inline std::string repr() const {
		auto ss = std::stringstream{};
		ss << *this;
		return ss.str();
	}
};

} // namespace cgrt::math
#endif //CGRT_POINT_HPP

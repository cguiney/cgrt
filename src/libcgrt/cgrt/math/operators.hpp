//
// Created by chris on 11/16/2021.
//
#pragma once
#ifndef CGRT_OPERATORS_HPP
#define CGRT_OPERATORS_HPP

#include <cgrt/runtime.hpp>

namespace cgrt::math {
template<typename T>
struct plus {
	CGRT_KERNEL_FUNC constexpr inline auto operator()(const T& lhs, const T& rhs) const {
		return lhs + rhs;
	}
};

template<typename T>
struct minus {
	CGRT_KERNEL_FUNC constexpr inline auto operator()(const T& lhs, const T& rhs) const {
		return lhs - rhs;
	}
};

template<typename T>
struct mul {
	CGRT_KERNEL_FUNC constexpr inline auto operator()(const T& lhs, const T& rhs) const {
		return lhs * rhs;
	}
};

template<typename T>
struct div {
	CGRT_KERNEL_FUNC constexpr inline auto operator()(const T& lhs, const T& rhs) const {
		return lhs / rhs;
	}
};

template<typename T>
CGRT_KERNEL_FUNC constexpr inline auto min(const T& lhs, const T& rhs) {
	if(lhs < rhs)
		return lhs;
	return rhs;
}

template<typename T>
CGRT_KERNEL_FUNC constexpr inline auto max(const T& lhs, const T& rhs) {
	if(lhs > rhs)
		return lhs;
	return rhs;
}

} // namespace cgrt::math
#endif //CGRT_OPERATORS_HPP

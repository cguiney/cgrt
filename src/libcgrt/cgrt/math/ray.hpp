//
// Created by chris on 11/25/2020.
//

#ifndef CGRT_RAY_HPP
#define CGRT_RAY_HPP

#include <cgrt/math/point.hpp>
#include <cgrt/math/vector.hpp>

namespace cgrt::math {

template<size_t N>
struct ray {
	point<N> origin;
	vector<N> direction;

  public:
	ray() = default;
	CGRT_KERNEL_FUNC constexpr inline ray(const point<N>& p, const vector<N>& dir) noexcept: origin(p), direction(dir) {}
    CGRT_KERNEL_FUNC constexpr inline ray(const ray<N>& other) noexcept : origin(other.origin), direction(other.direction) {}
    // CGRT_KERNEL_FUNC constexpr inline ray(ray<N>&& other) noexcept = default;

    CGRT_KERNEL_FUNC constexpr inline ray<3>& operator=(const ray<N>& other) noexcept {
        origin = other.origin;
        direction = other.direction;
        return *this;
    }

    CGRT_KERNEL_FUNC constexpr inline ray<3>& operator=(ray<N>&& other) noexcept {
        origin = other.origin;
        direction = other.direction;
        return *this;
    }


	friend inline std::ostream& operator<<(std::ostream& os, const ray<N>& v) {
		os << "{ origin: ";
		os << v.origin;
		os << " direction: ";
		os << v.direction;
		os << "}";
		return os;
	}

	inline std::string repr() const {
		auto ss = std::stringstream{};
		ss << *this;
		return ss.str();
	}
};

template<size_t N>
CGRT_KERNEL_FUNC constexpr inline point<N> position(const ray<N>& r, scalar t) {
	return r.origin + (r.direction * t);
}
} // namespace cgrt::math

#endif //CGRT_RAY_HPP

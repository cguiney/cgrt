//
// Created by chris on 9/11/20.
//

#ifndef CGRT_VECTOR_HPP
#define CGRT_VECTOR_HPP

#include <cgrt/math/array.hpp>
#include <cgrt/math/scalar.hpp>
#include <numeric>
#include <initializer_list>

namespace cgrt::math {

template<size_t N>
struct vector : public array<vector<N>, N> {
    using base_type = array<vector<N>, N>;

    vector() = default;

    CGRT_KERNEL_FUNC constexpr inline vector(std::initializer_list<scalar> l) noexcept : base_type(l) {}

    template<typename... Vs>
    CGRT_KERNEL_FUNC constexpr inline explicit vector(Vs&&... args) noexcept: base_type(std::forward<Vs>(args)...) {
    }

    template<typename T>
    CGRT_KERNEL_FUNC constexpr inline explicit vector(const array<T, N>& o) {
        for(size_t i = 0; i < N; i++)
            this->mutable_data()[i] = o[i];
    }

    CGRT_KERNEL_FUNC inline friend vector<N> operator-(const vector<N>& v) {
        return vector<N>() - v;
    }

    friend inline std::ostream& operator<<(std::ostream& os, const vector<N>& v) {
        os << "{";

        size_t i = 0;
        for(const auto& x : v.data()) {
            os << x;
            if(i++ < N - 1)
                os << " ";
        }

        os << "}";

        return os;
    }

    inline std::string repr() const {
        auto ss = std::stringstream{};
        ss << *this;
        return ss.str();
    }
};

template<size_t N>
CGRT_KERNEL_FUNC constexpr inline scalar length(const vector<N>& v) {
    auto sum = scalar();
    for(auto x : v.data())
        sum += x * x;
    return sum;
}

template<size_t N>
CGRT_KERNEL_FUNC constexpr inline scalar magnitude(const vector<N>& v) noexcept {
    return std::sqrt(length(v));
}

template<size_t N>
CGRT_KERNEL_FUNC constexpr inline vector<N> normalize(const vector<N>& v) noexcept {
    return v / magnitude(v);
}

template<size_t N>
CGRT_KERNEL_FUNC constexpr inline scalar dot(const vector<N>& a, const vector<N>& b) noexcept {
    auto out = math::scalar{0.f};
    for(size_t i = 0; i < N; i++)
        out += a[i] * b[i];
    return out;
}

template<size_t N>
CGRT_KERNEL_FUNC constexpr inline vector<N> cross(const vector<N>& a, const vector<N>& b) {
    vector<N> x;
    for(size_t i = 0; i < N; i++) {
        x[i] = a[(i + 1) % N] * b[(i + 2) % N] - a[(i + 2) % N] * b[(i + 1) % N];
    }
    return x;
}

template<size_t N>
CGRT_KERNEL_FUNC constexpr inline vector<N> forward(const vector<N>& normal, const vector<N>& direction) {
    if(dot(normal, direction) < 0.f) {
        return -normal;
    }
    return normal;
}

template<size_t N>
CGRT_KERNEL_FUNC constexpr inline vector<N> reflect(const vector<N>& in, const vector<N>& normal) {
    return in - (normal * (scalar{2.f} * dot(in, normal)));
}

} // namespace cgrt::math

#endif //CGRT_VECTOR_HPP

//
// Created by chris on 11/17/2021.
//
#pragma once
#ifndef CGRT_RANDOM_HPP
#	define CGRT_RANDOM_HPP

#	include "vendor/pcg_basic.hpp"

#include <cgrt/math/scalar.hpp>
#include <cgrt/math/array.hpp>

namespace cgrt::math {

class rng;

class rng {
	pcg::pcg32_random_t r_;

  public:
    rng() = default;
    ~rng() = default;

	CGRT_KERNEL_FUNC inline rng(uint64_t state, uint64_t seq) {
		pcg32_srandom_r(&r_, state, seq);
	}

	CGRT_KERNEL_FUNC inline double real() {
		return ldexp((double) pcg::pcg32_random_r(&r_), (int) -32);
	}

    CGRT_KERNEL_FUNC inline uint32_t integer(uint32_t bound) {
        return pcg::pcg32_boundedrand_r(&r_, bound);
    }
};

static_assert(std::is_standard_layout_v<rng>);
static_assert(std::is_default_constructible_v<rng>);
static_assert(std::is_copy_constructible_v<rng>);
static_assert(std::is_copy_assignable_v<rng>);
static_assert(std::is_move_constructible_v<rng>);
static_assert(std::is_move_assignable_v<rng>);
static_assert(std::is_default_constructible_v<rng>);

template<size_t N>
class continuous_random_variable;

template<size_t N, size_t M>
struct split {
    friend continuous_random_variable<N>;
    static_assert(N + M > N);

    using type = split<N, M>;
    using head_type = continuous_random_variable<N>;
    using tail_type = continuous_random_variable<M>;

    constexpr static const size_t initial_size = N+M;
    constexpr static const size_t head_size = M;
    constexpr static const size_t tail_size = N-M;

    split(split<N, M>&& other) noexcept = default;

  public:
    CGRT_KERNEL_FUNC explicit split(continuous_random_variable<N+M>&& input) {
        size_t i = 0;
        size_t j = 0;
        size_t k = 0;
        while(i < N) {
            head_[j++] = input[i];
            input[i++] = {};
        }
        while(i < N+M) {
            tail_[k++] = input[i];
            input[i++] = {};
        }
    }
    CGRT_KERNEL_FUNC head_type&& head() noexcept { return std::move(head_); }
    CGRT_KERNEL_FUNC tail_type&& tail() noexcept { return std::move(tail_); }

  private:
    head_type head_;
    tail_type tail_;
};

template<size_t N>
class continuous_random_variable : public math::array<continuous_random_variable<N>, N> {
    static_assert(N > 0, "N must be greater than zero");
    using base_type = math::array<continuous_random_variable<N>, N>;

    continuous_random_variable() = default;

  public:
    template<size_t Head, size_t Tail>
    friend struct split;

    CGRT_KERNEL_FUNC constexpr inline explicit continuous_random_variable(rng& rng) : base_type() {
        for(size_t i = 0; i < N; i++) {
            this->mutable_data()[i] = rng.real();
        }
    }

    template<size_t M>
    CGRT_KERNEL_FUNC static split<M, N-M> take(continuous_random_variable<N>&& input) noexcept {
        return split<M, N-M>(std::move(input));
    }

    template<size_t Head, size_t Tail>
    static continuous_random_variable<N> join(continuous_random_variable<Head> lhs, continuous_random_variable<Tail> rhs) {
        static_assert(N > 0);
        static_assert(Head > 0);
        static_assert(Tail > 0);
        static_assert(Head + Tail == N);

        auto x = continuous_random_variable<N>();

        size_t i = 0;
        if constexpr(Head > 1) {
            for(auto&& v : lhs.data()) {
                x.mutable_data()[i++] = v;
            }
        } else {
            x.mutable_data()[i++] = lhs;
        }
        if constexpr(Tail > 1) {
            for(auto&& v : rhs.data()) {
                x.mutable_data()[i++] = v;
            }
        } else {
            x.mutable_data()[i++] = rhs;
        }
        return x;
    }
};

template<>
class continuous_random_variable<1> : public math::scalar {
  public:
    continuous_random_variable(math::scalar f) noexcept : math::scalar(f) {}
};



}; // namespace cgrt::math

#endif //CGRT_RANDOM_HPP

//
// Created by chris on 2/6/2021.
//

#ifndef CGRT_CONSTANTS_H
#define CGRT_CONSTANTS_H

#include <cgrt/runtime.hpp>

#define CGRT_E        2.7182818284590452354f   /* e */
#define CGRT_LOG2E    1.4426950408889634074f   /* log_2 e */
#define CGRT_LOG10E   0.43429448190325182765f  /* log_10 e */
#define CGRT_LN2      0.69314718055994530942f  /* log_e 2 */
#define CGRT_LN10     2.30258509299404568402f  /* log_e 10 */
#define CGRT_PI       3.14159265358979323846f  /* pi */
#define CGRT_PI_2     1.57079632679489661923f  /* pi/2 */
#define CGRT_PI_4     0.78539816339744830962f  /* pi/4 */
#define CGRT_1_PI     0.31830988618379067154f  /* 1/pi */
#define CGRT_2_PI     0.63661977236758134308f  /* 2/pi */
#define CGRT_2_SQRTPI 1.12837916709551257390f  /* 2/sqrt(pi) */
#define CGRT_SQRT2    1.41421356237309504880f  /* sqrt(2) */
#define CGRT_SQRT1_2  0.70710678118654752440f  /* 1/sqrt(2) */
#define CGRT_EPSILON  0.0001f;

namespace cgrt::math {
CGRT_KERNEL_FUNC constexpr float PI() noexcept { return CGRT_PI; };
CGRT_KERNEL_FUNC constexpr float EPSILON() noexcept { return  CGRT_EPSILON; };
CGRT_KERNEL_FUNC constexpr float SQRT2() noexcept { return CGRT_SQRT2; };
CGRT_KERNEL_FUNC constexpr float SQRT1_2() noexcept { return CGRT_SQRT1_2; };
}

#endif //CGRT_CONSTANTS_H

//
// Created by chris on 1/12/21.
//

#ifndef CGRT_SHELL_HPP
#define CGRT_SHELL_HPP

#include "kernel.hpp"

#include <cgrt/aggregates/bvh.hpp>
#include <cgrt/img/canvas.hpp>
#include <cgrt/math/transform.hpp>
#include <cgrt/scene/builder.hpp>
#include <cgrt/scene/state.hpp>
#include <cgrt/state.hpp>
#include <cgrt/ui/controls.hpp>
#include <cgrt/ui/metrics.hpp>
#include <cgrt/ui/viewport.hpp>
#include <cgrt/ui/debug.hpp>

#include <iostream>
#include <memory>
#include <mutex>
#include <nanogui/nanogui.h>
#include <nanogui/opengl.h>
#include <string>
#include <thread>
#include <utility>
#include <fmt/format.h>

namespace cgrt::ui {

class camera_handler {
	constexpr static const float step = 0.5f;

	math::matrix<4, 4> default_;
	math::matrix<4, 4> view_;

	objects::camera& camera_;

	enum direction
	{
		left = 0,
		up = 1,
		forward = 2
	};

  public:
	inline void move_forward() {
		view_[forward][3] += step;
	}

	inline void move_backward() {
		view_[forward][3] -= step;
	}

	inline void move_up() {
		view_[up][3] -= step;
	}

	inline void move_down() {
		view_[up][3] += step;
	}

	inline void move_left() {
		view_[left][3] -= step;
	}

	inline void move_right() {
		view_[left][3] += step;
	}

	inline void rotate() {
		view_ = math::transform::rotate_y(view_, step).mtx();
	}

	inline void tilt() {
		view_ = math::transform::rotate_x(view_, -step).mtx();
	}

	inline math::scalar& fov() {
		return camera_.fov;
	};

	inline void set_fov(float fov) {
		camera_ = objects::camera(camera_.width, camera_.height, fov, view_);
	}

	camera_handler(objects::camera& camera)
	    : default_(camera.transform.mtx()), view_(camera.transform.mtx()), camera_(camera) {
	}

	/*
        inline void handle_input(const SDL_Event &e)
        {
            switch (e.key.keysym.sym) {
                case SDLK_w:
                    move_forward();
                    break;
                case SDLK_a:
                    move_left();
                    break;
                case SDLK_s:
                    move_backward();
                    break;
                case SDLK_d:
                    move_right();
                    break;
                case SDLK_UP:
                    move_up();
                    break;
                case SDLK_DOWN:
                    move_down();
                    break;
                case SDLK_f:
                    rotate();
                    break;
                case SDLK_t:
                    tilt();
                    break;
                case SDLK_r:
                    view_ = default_;
                    break;
            }
            camera_.transform = math::transform::matrix(view_);
        }
         */
};

template<typename Kernel>
class shell {
	std::atomic<bool> mainloop_active;
	nanogui::ref<nanogui::Screen> screen;
	nanogui::ref<viewport> viewport_;

	std::unique_ptr<object_panel> objects_;
	std::unique_ptr<state_handle> state;
	std::unique_ptr<metrics_panel> metrics_;
    std::unique_ptr<debug_panel> debug_;

	Kernel kernel;

	// ui::settings_handler settings;
	// ui::camera_handler camera;

	std::unique_ptr<cgrt::img::canvas> canvas;

	std::thread render;
	std::atomic<bool> stop{};
	bool pause{};

	unsigned time{};

	inline void execute_kernel(const scene::state& st) {
		kernel::environment global{
		    .state = &st,
            .canvas = canvas.get(),
		    .time = time++,
            .restrict_pixels = debug_->restrict_pixels()
		};

		kernel(global);
	}

	inline void render_async() {
		render = std::thread([this]() {
			using std::chrono::seconds;
			using std::chrono::microseconds;
            using std::chrono::milliseconds;
			using std::chrono::system_clock;
			using std::chrono::duration_cast;

			std::unique_ptr<scene::state> st = state->take();
            state->update([](scene::builder& x) {});
            state->publish();

			auto iteration = 0;
			auto second = (size_t)(0);
			auto total = milliseconds{};
			auto count = 0;
			while(!stop) {
				if(auto&& next = state->take(); next != nullptr) {
					metrics_->reset();
					std::cerr << "rendering new state" << std::endl;
					std::swap(st, next);
				}

				auto start = system_clock::now();
                auto end = start;
                auto duration = milliseconds{};
                {
                    execute_kernel(*st);
                    end = system_clock::now();
                    duration = duration_cast<milliseconds>(end - start);
                    std::cerr << "executed kernel in " << duration.count() << "ms" << std::endl;
                }
                {
                    canvas = viewport_->update(std::move(canvas), st->version, ++iteration);
                    end = system_clock::now();
                    duration = duration_cast<milliseconds>(end - start);
                    total += duration;
                    count += 1;
                    if(total >= std::chrono::seconds{1}) {
                        second += total / duration_cast<milliseconds>(seconds{1});
                        total = milliseconds{0};
                        count = 0;
                    }

                    std::cerr << fmt::format(
                        "rendered frame version {} iteration {} in {}ms; fps: {}",
                        st->version, (int) iteration, duration.count(), (int) count) << std::endl;
                }

				metrics_->inc(second, 1);
			}
		});
	}

  public:
	inline shell(const scene::builder& b, Kernel k, int width, int height):
          state(std::make_unique<state_handle>(b)),
          kernel(std::move(k)),
	      canvas(std::make_unique<img::canvas>(width, height)) {
		nanogui::init();
		std::cerr << "initialized ui" << std::endl;

		screen = new nanogui::Screen(nanogui::Vector2i(width, height), "CGRT Cuda",
		                             /* resizable    */ false,
		                             /* fullscreen   */ false);

		screen->set_size(nanogui::Vector2i(2400, 1200));
		objects_ = std::make_unique<object_panel>(screen, state.get());
		metrics_ = std::make_unique<metrics_panel>(screen);
        debug_ = std::make_unique<debug_panel>(screen);
        viewport_ = new viewport{screen, width, height};
	}

	inline void loop(int refresh) {
		if(mainloop_active)
			throw std::runtime_error("Main loop is already running!");

		mainloop_active = true;

		std::thread refresh_thread;
		if(refresh > 0) {
			/* If there are no mouse/keyboard events, try to refresh the
                   view roughly every 50 ms (default); this is to support animations
                   such as progress bars while keeping the system load
                   reasonably low */
			refresh_thread = std::thread([this, refresh]() {
				std::chrono::milliseconds time(refresh);
				while(mainloop_active) {
					std::this_thread::sleep_for(time);
					glfwPostEmptyEvent();
				}
			});
		}

		try {
			while(mainloop_active) {
				if(!screen->visible()) {
					/* Give up if there was nothing to draw */
					mainloop_active = false;
					break;
				}
				if(glfwWindowShouldClose(screen->glfw_window())) {
					screen->set_visible(false);
					/* Give up if there was nothing to draw */
					mainloop_active = false;
					break;
				}

				state->publish();
				viewport_->publish();
				// objects_->refresh();
				screen->perform_layout();
				screen->draw_all();

				/* Wait for mouse/keyboard or empty refresh events */
				glfwWaitEvents();
			}

			/* Process events once more */
			glfwPollEvents();
		} catch(const std::exception& e) {
			std::cerr << "Caught exception in main loop: " << e.what() << std::endl;
			mainloop_active = false;
		}

		if(refresh > 0)
			refresh_thread.join();
	}

	inline void run() {
		std::cerr << "starting renderer" << std::endl;
		render_async();

		std::cerr << "displaying ui" << std::endl;
		screen->set_visible(true);
		screen->perform_layout();

		std::cerr << "starting ui loop" << std::endl;
		loop(50);

		stop = true;

		if(render.joinable())
			render.join();
	}
};
}; // namespace cgrt::ui

#endif //CGRT_SHELL_HPP

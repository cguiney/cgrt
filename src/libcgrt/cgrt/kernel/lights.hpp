//
// Created by chris on 12/27/2021.
//

#ifndef CGRT_KERNEL_LIGHTS_HPP
#define CGRT_KERNEL_LIGHTS_HPP

#include <cgrt/runtime.hpp>
#include <cgrt/containers.hpp>
#include <cgrt/math/radiosity.hpp>
#include <cgrt/math/vector.hpp>
#include <cgrt/kernel/interactions.hpp>
#include <cgrt/kernel/surfaces.hpp>
#include <cgrt/objects/lights.hpp>
#include <cgrt/state.hpp>

namespace cgrt::kernel::lighting {

struct sample {
    math::vector<3> normal;
    sampling::sample reading;
    bool delta;
};

CGRT_KERNEL_FUNC inline math::spectrum object_emit(
    const objects::light_object& light,
    const math::vector<3>& normal,
    const math::vector<3>& wi)
{
    if(math::dot(normal, wi) <= 0.f) {
        return math::spectrum{};
    }
    return light.energy;
}

CGRT_KERNEL_FUNC inline sample sample_point(const interactions::interaction& ia, const objects::light_point& light) {
    auto path = light.position - ia.surface.position;
    auto decay = 1.f / math::magnitude(path);

    auto sample = lighting::sample{};
    sample.reading.pos = light.position;
    sample.reading.direction = math::normalize(path);
    sample.reading.energy = light.energy * decay;
    sample.reading.pdf = 1.f;
    sample.delta = true;
    return sample;
}

CGRT_KERNEL_FUNC inline containers::optional<sample> sample_object(
    const interactions::interaction& ia,
    const objects::light_object& light,
    const containers::span<const objects::object> objects,
    const containers::span<const math::transform::matrix>& transforms,
    const math::continuous_random_variable<2>& x
){
    const auto& obj = objects[light.object_id];
    const auto& transform = transforms[light.object_id];

    auto surface_sample = surfaces::sample(obj, transform, ia.above, x);
    if(!surface_sample) {
        return {};
    }

    // auto direction = surface_sample->pos - ia.above;
    if(math::length(surface_sample->direction) == 0.f) {
        return {};
    }

    auto sample = lighting::sample{};
    sample.reading = *surface_sample;
    sample.reading.direction = math::normalize(sample.reading.direction);

    auto wi = -sample.reading.direction;
    sample.reading.energy = object_emit(light, sample.reading.normal, wi);

    return sample;
}

CGRT_KERNEL_FUNC inline containers::optional<sample> lighting_sample(
    const interactions::interaction& ia, const objects::light& light,
    const containers::span<const objects::light_point>& light_points,
    const containers::span<const objects::light_object>& light_objects,
    const containers::span<const objects::object>& objects,
    const containers::span<const math::transform::matrix>& transforms,
    const math::continuous_random_variable<2>& xi
) {
    switch(light.type) {
    case objects::LIGHT_OBJECT: {
        const auto& li_obj = light_objects[light.id];
        return sample_object(ia, li_obj, objects, transforms, xi);
    }
    case objects::LIGHT_POINT: {
        const auto& point = light_points[light.id];
        return sample_point(ia, point);
    }
    }

    return {};
}

CGRT_KERNEL_FUNC inline math::scalar lighting_pdf_object(const objects::light_object& li_obj,
                                                         const containers::span<const objects::object>& objects,
                                                         const containers::span<const math::transform::matrix>& transforms,
                                                         const math::point<3>& from, const math::vector<3>& wi) {
    const auto& obj = objects[li_obj.object_id];
    const auto& transform = transforms[li_obj.object_id];
    switch(obj.type) {
    case objects::SPHERE:
        return surfaces::surface_pdf(obj, transform, from, wi);
        break;
    case objects::CUBE:
        break;
    case objects::PLANE:
        break;
    case objects::DISK:
        break;
    case objects::MESH:
        break;
    }
    return 0.f;
}

CGRT_KERNEL_FUNC inline math::scalar lighting_pdf(const objects::light& light,
                                                  const containers::span<const objects::light_object>& light_objects,
                                                  const containers::span<const objects::object>& objects,
                                                  const containers::span<const math::transform::matrix>& transforms,
                                                  const math::point<3>& from, const math::vector<3>& wo) {
    switch(light.type) {
    case objects::LIGHT_OBJECT: {
        const auto& obj = light_objects[light.id];
        return lighting_pdf_object(obj, objects, transforms, from, wo);
    }
    case objects::LIGHT_POINT:
        return 0.f;
    }
    return 0.f;
}

CGRT_KERNEL_FUNC inline bool is_singularity(const objects::light& light) {
    return light.type == objects::LIGHT_POINT;
}

} // namespace cgrt::kernel::lighting

#endif //CGRT_KERNEL_LIGHTS_HPP

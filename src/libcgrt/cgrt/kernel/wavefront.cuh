//
// Created by chris on 11/20/2021.
//

#ifndef CGRT_KERNEL_WAVEFRONT_HPP
#define CGRT_KERNEL_WAVEFRONT_HPP

#include <cgrt/runtime.hpp>
#include <cgrt/kernel/illumination.hpp>
#include <cgrt/kernel/intersections.hpp>
#include <cgrt/kernel/surfaces.hpp>
#include <cgrt/kernel/pipeline.hpp>
#include <cgrt/math/random.hpp>
#include <cgrt/math/ray.hpp>
#include <cgrt/objects/cameras.hpp>
#include <cgrt/objects/lights.hpp>
#include <cgrt/objects/materials.hpp>
#include <cgrt/objects/objects.hpp>
#include <cgrt/state.hpp>

namespace cgrt::kernel::wavefront {

class state_view {
	template<typename T>
	using table = cuda::device_vector<T>;
};

struct state_owner {
	template<typename T>
	using table = cuda::device_vector<T>;

	cuda::device_ptr<objects::camera> camera;

	table<objects::object> objects;
	table<objects::material> materials;
	table<math::transform::matrix> transforms;

	table<objects::light> lights;
	table<objects::light_point> light_points;
	table<objects::light_object> light_objects;

  public:
	explicit state_owner(const dense_state& densest) {
		camera = cuda::make_device(densest.camera);
		objects = table<objects::object>{densest.objects};
		materials = table<objects::material>{densest.materials};
		transforms = table<math::transform::matrix>{densest.transforms};

		lights = table<objects::light>{densest.lights};
		light_points = table<objects::light_point>{densest.light_points};
		light_objects = table<objects::light_object>{densest.light_objects};
	}
};

template<typename T, typename U, typename F>
CGRT_KERNEL void parallel_map(T in, U out, F fn) {
	auto x = threadIdx.x + blockIdx.x * blockDim.x;
	auto y = threadIdx.y + blockIdx.y * blockDim.y;
	auto i = y * (gridDim.x * blockDim.x) + x;
	if(i >= in.size()) {
		return;
	}

	out[i] = fn(in[i]);
}

template<typename T, typename F>
CGRT_KERNEL void parallel_each(T in, F fn) {
	auto x = threadIdx.x + blockIdx.x * blockDim.x;
	auto y = threadIdx.y + blockIdx.y * blockDim.y;
	auto i = y * (gridDim.x * blockDim.x) + x;
	if(i >= in.size()) {
		return;
	}

	fn(in[i], i);
}

template<typename T, typename F>
CGRT_KERNEL void parallel_each2D(T in, F fn) {
	auto x = threadIdx.x + blockIdx.x * blockDim.x;
	auto y = threadIdx.y + blockIdx.y * blockDim.y;
	auto i = y * (gridDim.x * blockDim.x) + x;
	if(i >= in.size()) {
		return;
	}

	fn(in[i], x, y);
}

template<typename T, typename U, typename F>
CGRT_KERNEL void parallel_filter(T in, U out, F fn) {
	auto x = threadIdx.x + blockIdx.x * blockDim.x;
	auto y = threadIdx.y + blockIdx.y * blockDim.y;
	auto i = y * (gridDim.x * blockDim.x) + x;
	if(i >= in.size()) {
		return;
	}

	auto r = fn(in[i]);
	if(r)
		out.push_back(*r);
}

struct pipeline {
	dim3 grid;
	dim3 block;

	state_owner st;
	cuda::device_vector<math::rng> rngs;
	cuda::device_vector<pixel> pixels;
	cuda::device_vector<path_segment> path_segments;
	cuda::device_vector<shading_eval> shading_evals;
	cuda::device_vector<occlusion_test> occlusion_tests;
	cuda::device_vector<path_extension> path_extensions;

	pipeline(dim3 grid, dim3 block, state_owner st): grid(grid), block(block), st(std::move(st)) {
		auto size = grid.x * grid.y * block.x * block.y;
		rngs = cuda::device_vector<math::rng>(size, size, size);
		pixels = cuda::device_vector<pixel>(size, size, size);
		path_segments = cuda::device_vector<path_segment>(size, size, size);
		shading_evals = cuda::device_vector<shading_eval>(size, size, size);
		occlusion_tests = cuda::device_vector<occlusion_test>(size, size, size);
		path_extensions = cuda::device_vector<path_extension>(size, size, size);
	}

	void init() const {
		auto grid = this->grid;
		auto block = this->block;
		parallel_each2D<<<grid, block>>>(pixels.span(), [grid, block] CGRT_KERNEL_FUNC(pixel & pixel, int x, int y) {
			pixel.x = x;
			pixel.y = y;
			pixel.index = y * (grid.x * block.x) + x;
		});

		parallel_each<<<grid, block>>>(rngs.span(), [] CGRT_KERNEL_FUNC(math::rng & rng, int i) {
			rng = math::rng(0x853c49e6748fea9bUL * (i + 1), 0xda3e39cb94b95bdbUL * (i + 1));
		});

		auto c = st.camera.get();
		auto randoms = rngs.span();

		parallel_map<<<grid, block>>>(pixels.span(), path_segments.span(),
		                              [c, randoms] CGRT_KERNEL_FUNC(const pixel& pixel) {
			                              auto rng = randoms[pixel.index];
			                              math::scalar rx = pixel.x + rng.real();
			                              math::scalar ry = pixel.y + rng.real();
			                              math::ray<3> r = cgrt::objects::generate_ray(*c, rx, ry);
			                              return path_segment{pixel.index, 0, r, 1.f};
		                              });
	}

	void trace() const {
		auto objects = st.objects.span();
		auto transforms = st.transforms.span();
		parallel_filter<<<grid, block>>>(
		    path_segments.span(), shading_evals.queue(),
		    [objects, transforms] CGRT_KERNEL_FUNC(const path_segment& seg) -> thrust::optional<shading_eval> {
			    auto closest = intersections::find_closest(seg.path, objects, transforms);
			    if(!closest)
				    return {};

			    return shading_eval{*closest};
		    });
	}

	void shade() const {
		// wavefront::shade<<<shading_evals.size(), block>>>(shading_evals.span(), occlusion_tests.queue());

		parallel_filter<<<grid, block>>>(shading_evals.span(), occlusion_tests.queue(), [] CGRT_KERNEL_FUNC(const shading_eval& shading) -> thrust::optional<occlusion_test> {

		});
	}

	void occlude() const {
		// wavefront::occlude<<<occlusion_tests.size(), block>>>(occlusion_tests.span(), path_extensions.queue());
	}

	void extend() const {
		/*
		wavefront::extend<<<path_extensions.size(), block>>>(path_extensions.span(), pixels.span(),
		                                                     path_segments.queue());
		*/
	}

	void execute() {
		do {
			trace();
			shade();
			occlude();
			extend();
		} while(path_segments.size() > 0);
	}
};

} // namespace cgrt::kernel::wavefront
#endif //CGRT_KERNEL_WAVEFRONT_HPP

//
// Created by chris on 12/27/2021.
//

#ifndef CGRT_SURFACES_HPP
#define CGRT_SURFACES_HPP

#include <cgrt/runtime.hpp>
#include <cgrt/math/constants.h>
#include <cgrt/math/random.hpp>
#include <cgrt/math/geometry.hpp>
#include <cgrt/math/point.hpp>
#include <cgrt/math/vector.hpp>
#include <cgrt/objects/objects.hpp>
#include <cgrt/kernel/sampling.hpp>
#include <cgrt/kernel/intersections.hpp>

namespace cgrt::kernel::surfaces {

struct surface {
    /*
     * point in world space
     * this is equivalent to surface.position (which is in object space)
     */
    math::point<3> position;

    /*
     * Details about the surface as it needs to be shown
     */
    struct {
        /* 2d mapping coordinates */
        math::scalar u;
        math::scalar v;

        math::vector<3> normal;
    } shading;

    /*
     * Details about the physical surface area
     */
    struct {
        math::vector<3> normal;

        /* partial derivatives used for creating an orthonormal basis with the surface normal */
        math::vector<3> du;
        math::vector<3> dv;
    } geometry;
};

CGRT_KERNEL_FUNC inline math::scalar surface_area_sphere() {
    math::scalar phi_max = math::radians(360.f), radius = 1.f, z_min = -radius, z_max = radius;

    return phi_max * radius * (z_max - z_min);
}

CGRT_KERNEL_FUNC inline float surface_area(const objects::object& obj) {
    switch(obj.type) {
    case objects::SPHERE:
        return surface_area_sphere();
        break;
    default:
        return 0;
    }
}

CGRT_KERNEL_FUNC inline surface surface_query_sphere(const math::point<3>& op) {
    math::scalar radius = 1.f;
    auto origin = math::point<3>{};
    auto path = op - origin;
    auto distance = math::magnitude(path);

    auto px = op.x() * (radius / distance);
    auto py = op.y() * (radius / distance);
    auto pz = op.z() * (radius / distance);

    if(px == 0.0f && py == 0.0f)
        px = math::EPSILON() * radius;

    auto phi = atan2f(py, px);
    if(phi < 0.f)
        phi += 2.f * math::PI();

    auto phi_max = 2 * math::PI(); // radians(360.f);
    auto theta = acosf(math::clamp(pz / radius, -1.f, 1.f));
    auto theta_min = math::PI(); // acosf(-1f);
    auto theta_max = 0.0f;     // acosf(1f);
    auto theta_diff = (theta_max - theta_min);

    auto surf = surface();
    surf.shading.u = (phi / phi_max); // (1.0f - ((phi / phi_max) + 0.5f));
    surf.shading.v = (theta - theta_min) / theta_diff;

    // float inv_z_radius = rsqrt(px * px + py * py);
    auto inv_z_radius = 1.f / sqrtf(px * px + py * py);
    auto cos_phi = px * inv_z_radius;
    auto sin_phi = py * inv_z_radius;

    surf.position = math::point<3>{px, py, pz};
    surf.geometry.normal = path;
    surf.geometry.du = math::vector<3>{-phi_max * py, phi_max * px, 0.f};
    surf.geometry.dv = math::vector<3>{theta_diff * (pz * cos_phi),
                                       theta_diff * (pz * sin_phi),
                                       theta_diff * (-radius * sinf(theta))};
    return surf;
}

CGRT_KERNEL_FUNC inline surface surface_query_disk(const math::point<3>& op) {
    auto px = op.x();
    auto py = op.y();
    auto distsq = px * px + py * py;
    auto r = sqrtf(distsq);
    auto phi_max = 2 * math::PI();
    auto phi = atan2f(py, px);
    if(phi < 0)
        phi += 2 * math::PI();

    auto surf = surface();
    surf.position = math::point<3>{px, py, 0};
    surf.shading.u = phi / phi_max;
    surf.shading.v = (1.f - r);
    surf.geometry.du = math::vector<3>{-phi_max * py, phi_max * px, 0.0f};
    surf.geometry.dv = math::vector<3>{-px, -py, 0.0f} / r;
    return surf;
}

CGRT_KERNEL_FUNC inline surface surface_query_plane(const math::point<3>& op) {
    auto surf = surface{};
    surf.position = math::point<3>{op.x(), op.y(), 0};
    surf.geometry.normal = math::vector<3>{0.0f, 0.0f, 1.0f};
    // vector_coordinate_system(&surf->geometry.normal, &surf->geometry.du, &surf->geometry.dv);
    //surf->geometry.du = INIT_VECTOR(-1, 1, 0);
    //surf->geometry.dv = INIT_VECTOR(1, 1, 0);
    // surf->geometry.du = vector_negate(&surf->geometry.du);
    // surf->geometry.du = vector_negate(&surf->geometry.du);
    // surf.geometry.du = math::vector<3>{-op.y(), op.x(), 0.0f};
    // surf.geometry.dv = math::vector<3>{op.x(), op.y(), 0.0f};
    surf.geometry.du = math::vector<3>{-1.f, 1.f, 0.f};
    surf.geometry.dv = math::vector<3>{1.f, 1.f, 0.f};
    surf.shading.u = 1.0f;
    surf.shading.v = 1.0f;
    return surf;
}

CGRT_KERNEL_FUNC inline surface surface_query(
    const objects::object& obj,
    const math::transform::matrix& transform,
    const math::point<3>& wp
) {
    auto op = math::transform::inverse(transform)(wp);

    auto surf = surface{};

    switch(obj.type) {
    case objects::SPHERE:
        surf = surface_query_sphere(op);
        break;
    case objects::PLANE:
        surf = surface_query_plane(op);
        break;
    case objects::DISK:
        surf = surface_query_disk(op);
        break;
    default:
        break;
    }

    surf.geometry.normal = math::cross(surf.geometry.du, surf.geometry.dv);

    /* transform normal to world space, normalize & capture */
    surf.position = transform(surf.position);
    surf.geometry.normal = math::normalize(transpose(inverse(transform))(surf.geometry.normal));
    surf.geometry.du = transform(surf.geometry.du);
    surf.geometry.dv = transform(surf.geometry.dv);

    surf.shading.normal = surf.geometry.normal;

    auto dp = math::dot(surf.shading.normal, surf.geometry.normal);
    if(dp < 0.f) {
        surf.shading.normal = -surf.shading.normal;
    }
    return surf;
}

CGRT_KERNEL_FUNC inline sampling::sample surface_sample_sphere(const objects::object& obj,
                                                        const math::continuous_random_variable<2>& xi) {
    auto radius = math::scalar{1.f};
    auto origin = math::point<3>{0.f};
    auto random_direction = sampling::hemisphere_uniform(xi);
    auto direction = random_direction * radius;
    auto position = origin + direction;

    auto sample = sampling::sample{};
    sample.pos = position; // point_scale(&position, 1 / distance(&position, &origin));
    sample.direction = direction;
    sample.normal = math::vector<3>{position.x(), position.y(), position.z()};
    sample.pdf = 1.f / surface_area_sphere();
    return sample;
}

CGRT_KERNEL_FUNC inline math::scalar surface_pdf_sphere(const objects::object& obj, const math::vector<3>& wi,
                                                        const math::vector<3>& wo) {
    return 1.0f / surface_area_sphere();
}

CGRT_KERNEL_FUNC inline math::scalar surface_pdf(const objects::object& obj, const math::transform::matrix& transform,
                                                 const math::point<3>& from, const math::vector<3>& wi) {
    auto r = math::ray{from, wi};
    auto hit = intersections::intersects(r, obj, transform);
    if(!hit) {
        return 0.0f;
    }

    auto p = math::position(r, *hit);

    auto surf = surface_query(obj, transform, p);

    auto dir = -wi;
    auto angle = fabsf(math::dot(surf.geometry.normal, dir));
    auto dist = math::length(from - surf.position);
    auto area = surface_area(obj);
    auto pdf = dist / (angle * area);

    if(math::scalar_isinf(pdf)) {
        return 0.f;
    }

    return pdf;
}

CGRT_KERNEL_FUNC inline containers::optional<sampling::sample> sample(
    const objects::object& obj,
    const math::transform::matrix& transform,
    const math::point<3>& from,
    const math::continuous_random_variable<2>& x
) {
    auto sample = sampling::sample{};

    switch(obj.type) {
    case objects::SPHERE:
        sample = surface_sample_sphere(obj, x);
    break;
    case objects::PLANE:
        return {};
    case objects::DISK:
        return {};
    default:;
        return {};
    }

    sample.pos = math::transform::inverse(transform)(sample.pos);
    sample.normal = math::transform::transpose(math::transform::inverse(transform))(sample.normal);

    // adjust pdf from probability to sample point in area
    // to probability of sampling the angle
    // struct vector wi = point_sub(&sample->pos, &from->position);
    if(math::length(sample.pos - from) == 0.f) {
        return {};
    }

    sample.direction = math::normalize(sample.pos - from);

    sample.pdf *= math::length(from - sample.pos) * fabsf(math::dot(sample.normal, -sample.direction));
    if(math::fisinf(sample.pdf)) {
        return {};
    }

    return sample;
}

} // namespace cgrt::kernel::surfaces

#endif //CGRT_SURFACES_HPP

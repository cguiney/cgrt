//
// Created by chris on 9/25/2022.
//

#ifndef CGRT_KERNEL_INTERACTIONS_HPP
#define CGRT_KERNEL_INTERACTIONS_HPP

#include <cgrt/math/point.hpp>
#include <cgrt/math/ray.hpp>
#include <cgrt/state.hpp>
#include <cgrt/kernel/intersections.hpp>
#include <cgrt/kernel/bsdf.hpp>
#include <cgrt/kernel/surfaces.hpp>

namespace cgrt::kernel::interactions {

struct interaction {
    intersections::intersection intersection;
    math::ray<3> input;
    math::point<3> at;
    math::point<3> above;
    math::point<3> below;

    bsdf::bsdf bsdf;
    surfaces::surface surface;

    struct shading {
        math::vector<3> normal;
    };

  public:
    interaction() = default;
};

CGRT_KERNEL_FUNC inline interaction interact(
    const dense_state_view state,
    const intersections::intersection& xs,
    const math::ray<3>& input
) {
    auto direction = -input.direction;
    auto position = math::position(input, xs.time);
    auto ia = interaction{};

    ia.intersection = xs;
    ia.input = math::ray<3>{
        input.origin,
        math::normalize(direction)
    };

    ia.surface = surfaces::surface_query(xs.obj, state.transforms()[xs.obj.id], position);
    ia.surface.geometry.normal = math::forward(ia.surface.geometry.normal, direction);
    ia.surface.shading.normal = math::forward(ia.surface.shading.normal, direction);
    ia.at = position;
    ia.above = position + (ia.surface.geometry.normal * math::EPSILON());
    ia.below = position - (ia.surface.geometry.normal * math::EPSILON());
    ia.bsdf = bsdf::bsdf(ia.surface, state.materials()[ia.intersection.obj.id]);
    return ia;
}

}

#endif //CGRT_KERNEL_INTERACTIONS_HPP

//
// Created by chris on 9/11/2022.
//

#ifndef CGRT_PATH_TRACE_HPP
#define CGRT_PATH_TRACE_HPP

#include <cgrt/kernel/bsdf.hpp>
#include <cgrt/kernel/estimators.hpp>
#include <cgrt/kernel/interactions.hpp>
#include <cgrt/kernel/intersections.hpp>
#include <cgrt/kernel/lights.hpp>
#include <cgrt/kernel/sampling.hpp>
#include <cgrt/kernel/surfaces.hpp>
#include <cgrt/math/radiosity.hpp>
#include <cgrt/math/vendor/pcg_basic.hpp>
#include <cgrt/state.hpp>
#include <utility>

namespace cgrt::kernel {

static constexpr const size_t TRACE_DEPTH = 5;

CGRT_KERNEL_FUNC inline math::scalar gamma_correct(math::scalar channel) {
    if(channel <= 0.0031308f)
        return 12.92f * channel;
    return 1.055f * powf(channel, 1.f / 2.4f) - 0.055f;
}

CGRT_KERNEL_FUNC inline math::scalar clamp(math::scalar v, math::scalar low, math::scalar high) {
    if(v < low) return low;
    if(v > high) return high;
    return v;
}

class srgb {
  private:
    CGRT_KERNEL_FUNC inline srgb(math::scalar r, math::scalar g, math::scalar b, math::scalar a) noexcept
        : r(r), g(g), b(b), a(a) {}
  public:
    math::scalar r, g, b, a;
    inline srgb() noexcept = default;
    inline srgb(const srgb& other) = default;
    inline srgb(srgb&& other) noexcept = default;
    inline ~srgb() noexcept = default;

    CGRT_KERNEL_FUNC inline srgb& operator=(const srgb& other) {
        *this = srgb(other);
        return *this;
    }

    CGRT_KERNEL_FUNC inline srgb& operator=(srgb&& other) {
        r = other.r;
        b = other.b;
        g = other.g;
        a = other.a;
        return *this;
    }


    CGRT_KERNEL_FUNC inline srgb(math::spectrum rgb) noexcept
        : r(gamma_correct(rgb[0])),
          g(gamma_correct(rgb[1])),
          b(gamma_correct(rgb[2])),
          a(1.0f) {
    }

    CGRT_KERNEL_FUNC static inline srgb to_ints(const srgb& s) noexcept {
        return srgb{
            clamp(ceilf(s.r * 255.f), 0.f, 255.f),
            clamp(ceilf(s.g * 255.f), 0.f, 255.f),
            clamp(ceilf(s.b * 255.f), 0.f, 255.f),
            255.f
        };
    }
};

struct frame {
    uint64_t id;
    uint32_t x;
    uint32_t y;
    uint32_t width;
    uint32_t height;
    uint32_t depth;
    uint32_t spp;
    int64_t time;
    bool redraw;
    bool shadowing;
};

enum trace_state {
    TRACING,
    CONNECTING,
    EMITTING,
    ESTIMATING,
    CONVERGED
};

struct path_connection {
    math::ray<3> path;
    math::spectrum weight;
    bsdf::scatter_sample sample;
};

struct path_segment {
    trace_state state;
    path_connection input;
    containers::optional<interactions::interaction> ia;
    containers::optional<math::spectrum> emitted;
    containers::optional<math::spectrum> estimated;
    containers::optional<math::spectrum> indirect;
    containers::optional<path_connection> next;
};

CGRT_KERNEL_FUNC inline math::scalar sample_random_real(math::pcg::pcg_state_setseq_64& rng) {
    // ctx->frame->stats->rng_samples++;
    return ldexp(static_cast<float>(pcg32_random_r(&rng)), -32);
}

CGRT_KERNEL_FUNC inline containers::optional<interactions::interaction> integration_trace(
    math::rng& rng,
    const dense_state_view& state,
    const math::ray<3>& input
) noexcept {
    auto hit = intersections::find_closest(input, state.objects(), state.transforms());
    if(!hit) {
        return {};
    }
    /* precompute what's happening at the intersection */
    return interactions::interact(state, *hit, input);
}

CGRT_KERNEL_FUNC inline containers::optional<math::spectrum> integration_emittance(
    const dense_state_view& state,
    const interactions::interaction& ia,
    size_t bounce
) {
    if(bounce != 1) {
        return {};
    }

    if(!ia.intersection.obj.is_light) {
        return {};
    }

    /* account for any emission from the object */
    auto li = state.light_objects()[ia.intersection.obj.light_id];
    auto wi = ia.input.direction; // already negated as part of the interaction
    auto n  = ia.surface.shading.normal;
    return lighting::object_emit(li, n, wi);
}

CGRT_KERNEL_FUNC inline math::spectrum integration_estimation(
    math::rng& rng,
    const dense_state_view& state,
    const interactions::interaction& ia
) {
    /*
     * - pick a light uniform randomly
     * - estimate the light contribution of this bounce
     * - scale up by the number of lights
     */
    auto& light = state.lights()[rng.integer(state.lights().size())];
    return estimate_direct(
        ia,
        light,
        state.light_points(),
        state.light_objects(),
        state.objects(),
        state.transforms(),
        math::continuous_random_variable<4>(rng)
    ) * state.lights().size();
}

CGRT_KERNEL_FUNC inline containers::optional<path_connection> integrate_next_path(
    math::rng& rng,
    const dense_state_view& state,
    const interactions::interaction& ia
) {
    /*
     * Start setting up the next frame based on a sampling from the current intersection
     * that is, we're testing the bsdf
     */
    auto sample = bsdf::sample(ia.bsdf, ia.input.direction, math::continuous_random_variable<2>(rng));
    if (!sample) {
       return {};
    }
    assert(sample->energy != math::spectrum{});
    assert(sample->pdf > 0.f);

    auto angle = fabsf(math::dot(sample->direction, ia.surface.shading.normal));
    auto weight = (sample->energy * angle) / sample->pdf;
    auto path = math::ray<3>(ia.above, sample->direction);
    return path_connection{path, weight, *sample};
}

CGRT_KERNEL_FUNC inline math::spectrum integrate_unidirectional(
    frame& frame,
    math::rng& rng,
    const dense_state_view& state,
    const containers::span<path_segment>& stack_data,
    const math::ray<3>& input
)
{
    auto contribution = math::spectrum{}; /* be sure to default the contribution */
    auto stack = containers::stack<path_segment>(stack_data);

    /* initialize the first frame */
    path_segment& init = stack.push();
    init.state = TRACING;
    init.input = path_connection{
       input,
       math::spectrum{1.f},
       bsdf::scatter_sample{},
    };

    while (!stack.empty()) {
        auto& sp = stack.current();
        // printf("frame #%zd: weight: %f %f %f\n", stack.size(), (float)sp.weight[0], (float)sp.weight[1], (float)sp.weight[2]);
        // fflush(NULL);
        /* based on the current ray state, decide what to do next*/
        switch (sp.state) {
        case TRACING:
            sp.ia = integration_trace(rng, state, sp.input.path);
            sp.state = sp.ia ? EMITTING : CONVERGED;
            break;
        case EMITTING:
            assert(sp.ia.has_value());
            sp.emitted = integration_emittance(state, *sp.ia, stack.size());
            sp.state = ESTIMATING;
            break;
        case ESTIMATING:
            assert(sp.ia.has_value());
            sp.estimated = integration_estimation(rng, state, *sp.ia);
            if(sp.estimated == math::spectrum{}) {
                sp.state = CONVERGED;
            } else {
                sp.state = CONNECTING;
            }
            break;
        case CONNECTING: {
            assert(sp.ia.has_value());
            sp.next = integrate_next_path(rng, state, *sp.ia);
            sp.state = CONVERGED;
            if(!sp.next) {
                continue;
            }
            if(stack.full()) {
                continue;
            }
            auto& next = stack.push();
            next.input = *sp.next;
            break;
        }
        case CONVERGED: {
            /*
             * all of a rays children have been processed:
             * all different paths contributions are ready, add the reflection and contribution together
             * combine them, and then
             */
            auto segment_contribution = math::spectrum{};
            segment_contribution += sp.estimated.value_or(math::spectrum{}) * sp.input.weight;
            segment_contribution += sp.emitted.value_or(math::spectrum{}) * sp.input.weight;
            segment_contribution += sp.indirect.value_or(math::spectrum{});

            stack.pop();
            if(stack.empty()) {
                contribution = segment_contribution;
            } else {
                auto prev = stack.current();
                prev.indirect = segment_contribution;
            }
            break;
        }
        default:
            goto exit;
        }
    }
exit:
    assert(stack.empty());
    return contribution;
}

CGRT_KERNEL_FUNC inline math::spectrum path_trace_n(
    frame& frame,
    math::rng& rng,
    const dense_state_view& state,
    const containers::span<path_segment>& stack
) {
    math::spectrum contribution;
    math::ray<3> r = {};
    auto& camera = state.camera();
    for(uint32_t i = 0; i < frame.spp; i++) {
        auto xi = math::continuous_random_variable<2>(rng);
        math::scalar x = frame.x + xi[0];
        math::scalar y = frame.y + xi[1];
        // frame.initial = generate_ray(state.camera(), x, y);
        // frame.accumulation = frame.accumulation + integrate_unidirectional(frame, state, stack, frame.initial);
        r = generate_ray(camera, x, y);
        contribution = contribution + integrate_unidirectional(frame, rng, state, stack, r);
    }
    return contribution;
}

CGRT_KERNEL_FUNC inline void path_trace(
    kernel::frame& frame,
    dense_state_view state,
    math::rng& rng,
    // stack storage
    containers::span<path_segment> stack,
    // scratch, output & options
    containers::span<math::spectrum> sums,
    containers::span<int> samples,
    containers::span<srgb> averages
) {
    if(frame.x >= frame.width || frame.y >= frame.height) {
        return;
    }

    // printf("frame #%llu: beginning trace\n", frame.id);
    math::spectrum contribution = path_trace_n(frame, rng, state, stack);

    if(frame.redraw) {
        sums[frame.id] = contribution; // frame.accumulation;
        samples[frame.id] = 1; // static_cast<int>(frame.spp);
    } else {
        sums[frame.id] = sums[frame.id] + contribution; // frame.accumulation;
        samples[frame.id] += 1; // static_cast<int>(frame.spp);
    }

    math::scalar factor = 1.f / math::scalar{samples[frame.id]};
    math::spectrum avg = sums[frame.id] * factor;

    /*
     * convert from linear rgb to srgb
     */
    auto v = srgb(avg);
    auto ints = srgb::to_ints(v);
    /*
     * clamp and convert for output
     */
    //printf("writing averages for frame #%llu; total averages: %llu\n",
    //      frame.id, averages.size());
    averages[frame.id] = ints;
}

struct integrator_state_view {
    containers::span<frame> frames;
    containers::span<math::rng> generators;
    containers::span<kernel::path_segment> stacks;
    containers::span<math::spectrum> sums;
    containers::span<int> samples;
    containers::span<srgb> averages;
};

template<typename RuntimeT>
struct integrator_state {
    template<typename T>
    using storage_type = typename std::enable_if_t<
        std::is_default_constructible_v<T> &&
        std::is_copy_constructible_v<T> &&
        std::is_copy_assignable_v<T> &&
        std::is_trivially_destructible_v<T>,
        typename containers::storage<RuntimeT, T>::type
        >;
    storage_type<frame> frames;
    storage_type<math::rng> generators;
    storage_type<kernel::path_segment> stacks;
    storage_type<math::spectrum> sums;
    storage_type<int> samples;
    storage_type<srgb> averages;
    CGRT_HOST explicit integrator_state(size_t pixels) noexcept :
          frames(pixels),
          generators(pixels),
          stacks(pixels * TRACE_DEPTH),
          sums(pixels),
          samples(pixels),
          averages(pixels)

    {
        for(size_t i = 0; i < generators.size(); ++i) {
            auto rng = math::rng(i, 0xda3e39cb94b95bdbUL + i);
            generators[i] = rng;
        }
    }

    CGRT_HOST void reset() noexcept {
        for(size_t i = 0; i < frames.size(); ++i) {
            frames[i] = frame{};
        }
        for(size_t i = 0; i < stacks.size(); ++i) {
            stacks[i] = path_segment{};
        }
    }

    CGRT_HOST operator integrator_state_view() {
        return integrator_state_view{
            frames,
            generators,
            stacks,
            sums,
            samples,
            averages
        };
    }
};

class path_integrator {
    dense_state_view scene_;
    integrator_state_view integration_;

  public:
    CGRT_HOST explicit path_integrator(
        dense_state_view scene,
        integrator_state_view integration
    ) noexcept : scene_(std::move(scene)), integration_(std::move(integration))
    {}


    CGRT_KERNEL_FUNC inline void operator()(frame& frame) noexcept {
        auto stack_start = integration_.stacks.begin() + (frame.id * kernel::TRACE_DEPTH);
        auto stack_len = kernel::TRACE_DEPTH;
        auto stack = containers::span<kernel::path_segment>(stack_start, stack_len);
        auto& rng = integration_.generators[frame.id];
        path_trace(frame, scene_, rng, stack, integration_.sums, integration_.samples, integration_.averages);
    }

    CGRT_KERNEL_FUNC inline dense_state_view scene() const {
        return scene_;
    }

    CGRT_KERNEL_FUNC inline integrator_state_view integration() const {
        return integration_;
    }
};

static_assert(std::is_copy_constructible_v<path_integrator>);
static_assert(std::is_copy_assignable_v<path_integrator>);
static_assert(std::is_move_constructible_v<path_integrator>);
static_assert(std::is_move_assignable_v<path_integrator>);


} // namespace cgrt::kernel

#endif //CGRT_PATH_TRACE_HPP

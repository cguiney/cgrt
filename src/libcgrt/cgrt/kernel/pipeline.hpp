//
// Created by chris on 11/21/2021.
//

#ifndef CGRT_PIPELINE_HPP
#define CGRT_PIPELINE_HPP

#include <cgrt/containers.hpp>
#include <cgrt/math/radiosity.hpp>
#include <cgrt/math/ray.hpp>
#include <cgrt/objects/objects.hpp>
#include <cgrt/kernel/bsdf.hpp>
#include <cgrt/kernel/intersections.hpp>
#include <cgrt/kernel/surfaces.hpp>

namespace cgrt::kernel {

struct interaction {
	int pixel_index{};
	cgrt::objects::object_id obj_id;
	cgrt::math::ray<3> path;
	cgrt::math::scalar time;

	surfaces::surface surface;
	math::point<3> above;

    kernel::bsdf::bsdf bsdf;
};

struct pixel {
	int index;
	int x;
	int y;
	cgrt::math::spectrum contribution;
};

struct path_segment {
	int pixel_index{};
	int depth;
	cgrt::math::ray<3> path;
	cgrt::math::scalar weight;
};

struct shading_eval {
	intersections::intersection intersection;
};

struct occlusion_test {

};

struct path_extension {

};

} // namespace cgrt::kernel

#endif //CGRT_PIPELINE_HPP

//
// Created by chris on 12/27/2021.
//

#ifndef CGRT_ESTIMATORS_HPP
#define CGRT_ESTIMATORS_HPP

#include <cgrt/containers.hpp>
#include <cgrt/math/radiosity.hpp>
#include <cgrt/math/transform.hpp>
#include <cgrt/kernel/interactions.hpp>
#include <cgrt/kernel/lights.hpp>
#include <cgrt/kernel/bsdf.hpp>

namespace cgrt::kernel {

CGRT_KERNEL_FUNC inline math::scalar power_heuristic(math::scalar nf, math::scalar fpdf, math::scalar ng, math::scalar gpdf)
{
	auto f = nf * fpdf;
	auto g = ng  * gpdf;
	return (f*f) / ((f * f) + (g * g));
}

CGRT_KERNEL_FUNC inline math::spectrum emission(
    const objects::light_object& li,
    const math::vector<3>& wi,
    const math::vector<3>& normal
) noexcept {
    if(dot(normal, wi) <= 0)
        return math::spectrum{};
    return li.energy;
}

CGRT_KERNEL_FUNC inline math::spectrum estimate_direct_lighting(
    const interactions::interaction& ia,
    const objects::light& light,
    const containers::span<const objects::light_point>& light_points,
    const containers::span<const objects::light_object>& light_objects,
    const containers::span<const objects::object>& objects,
    const containers::span<const math::transform::matrix>& transforms,
    const math::continuous_random_variable<2>& x
)
{
	auto contribution = math::spectrum{};
	auto f = math::spectrum{};
	auto scattering_pdf = 0.f;

	auto ls = lighting::lighting_sample(ia, light, light_points, light_objects, objects, transforms, x);
    if(!ls) {
        return {};
    }
    assert(ls->reading.pdf > 0.f);

	/* evaluate bsdf using the sampled path to the light's surface */
	f = bsdf::eval(ia.bsdf, ia.input.direction, ls->reading.direction);
	scattering_pdf = bsdf::pdf(ia.bsdf, ia.input.direction, ls->reading.direction);

	/* scale by the angle of the light from the surface normal */
	f *= fabsf(math::dot(ls->reading.direction, ia.surface.shading.normal));

	/* if there is no contribution, early exit before the occlusion test */
	if(f == math::spectrum{} || scattering_pdf <= 0.f) {
		return math::spectrum{0.f, 0.f, 0.f};
	}

	/* make sure that the light isn't actually occluded by another object */
    auto direction = ls->reading.pos - ia.above;
	auto path = math::ray<3>{
	    ia.above,
	    math::normalize(direction)
	};

    auto hit = intersections::find_closest(path, objects, transforms);
	if (hit) {
        // if(hit->obj.id == ia.intersection.obj.id) {
        //    printf("disk intersected with itself\n");
        // }
        auto distance = math::magnitude(direction);
		if(!hit->obj.is_light && hit->time < distance) {
            return math::spectrum{0.f, 0.f, 0.f};
		}
	}

    auto Lpdf = ls->reading.pdf;
    auto& L = ls->reading.energy;
    auto& Li = contribution;
    assert(Lpdf > 0.f);

    /*
    printf("I{%.2f %.2f %.2f} = f{%.2f %.2f %.2f}(%.2f%%) * L{%.2f, %.2f, %.2f} (%.2f%%)\n",
       (float) I[0], (float) I[1], (float) I[2],
       (float) f[0], (float) f[1], (float) f[2], (float) scattering_pdf,
       (float) L[0], (float) L[1], (float) L[2], (float) Lpdf
   );
   */


    /* incidence += light energy * scattered energy / pdf */
    Li = f * L * (1.f / Lpdf);


	if (!lighting::is_singularity(light)) {
        auto weight = power_heuristic(1.0f, Lpdf, 1.0f, scattering_pdf);
        /* direct += light energy * scattered energy * weight / pdf */
        Li *= weight;
	}

	return Li;
}

CGRT_KERNEL_FUNC inline math::spectrum estimate_direct_bsdf(
    const interactions::interaction& ia,
    const objects::light& light,
    const containers::span<const objects::light_object>& light_objects,
    const containers::span<const objects::object>& objects,
    const containers::span<const math::transform::matrix>& transforms,
    const math::continuous_random_variable<2>& x
)
{
	bool is_specular_sample = false;

	/* if it's a singularity, there's no real sampling to be done */
	if (lighting::is_singularity(light)) {
		return {};
	}

    auto bs = bsdf::sample(ia.bsdf, ia.input.direction, x);
    if(!bs) {
        return {};
    }
    assert(bs->pdf > 0.f);

	auto f = bs->energy;
    auto angle = fabsf(math::dot(bs->direction, ia.surface.shading.normal));
	f *= angle;
    if(f == math::spectrum{}) {
        return {};
    }

	auto weight = 1.f;

	/* there's no chance of the light sampling in the specular direction */
	is_specular_sample = bs->reflection == bsdf::SPECULAR;
	if(!is_specular_sample) {
		/* get the probability of the sampled surface direction being hit by the lighting object */
		auto light_pdf = lighting::lighting_pdf(light, light_objects, objects, transforms, ia.above, bs->direction);
		if(light_pdf <= 0.f) {
			/*
             * if there's no possibility that light could hit the sampled surface location,
             * there's no contribution to be had
             */
			return {};
		}

		weight = power_heuristic(1.0f, bs->pdf, 1.0f, light_pdf);
	}

	/*
     * do an intersection test to ensure that the path from the surface sample location to the light isn't
     * obstructed by any other object
     */
	auto path = math::ray<3>{ia.above, bs->direction};
    auto hit = intersections::find_closest(path, objects, transforms);
	if(!hit) {
		return {};
	}

	if(hit->time > ia.intersection.time) {
		return {};
	}

	/*
     * if the intersected object is not a light, there's no contribution to be had
     */
    auto li = light_objects[light.id];
	if(hit->obj.id != li.object_id) {
		return {};
	}

    auto lighting = lighting::object_emit(li, ia.surface.shading.normal, bs->direction);
	if(lighting == math::spectrum{}) {
        return {};
    }

	math::spectrum contribution;
	contribution = f * lighting;
	contribution *= weight;
	contribution *= 1.f / bs->pdf;

	return contribution;
}

CGRT_KERNEL_FUNC inline math::spectrum estimate_direct(
    const interactions::interaction& ia,
    const objects::light& light,
    const containers::span<const objects::light_point>& light_points,
    const containers::span<const objects::light_object>& light_objects,
    const containers::span<const objects::object>& objects,
    const containers::span<const math::transform::matrix>& transforms,
    math::continuous_random_variable<4> x
)
{
    auto xs = math::split<2, 2>(std::move(x));
	math::spectrum lighting = estimate_direct_lighting(ia, light, light_points, light_objects, objects, transforms, xs.head());
    math::spectrum bsdf = estimate_direct_bsdf(ia, light, light_objects, objects, transforms, xs.tail());
	return lighting + bsdf;
}

}

#endif //CGRT_ESTIMATORS_HPP

//
// Created by chris on 12/4/2021.
//

#ifndef CGRT_KERNEL_INTERSECTIONS_HPP
#define CGRT_KERNEL_INTERSECTIONS_HPP

#include <cgrt/runtime.hpp>
#include <cgrt/containers.hpp>
#include <cgrt/math/constants.h>
#include <cgrt/math/point.hpp>
#include <cgrt/math/ray.hpp>
#include <cgrt/math/transform.hpp>
#include <cgrt/objects/objects.hpp>

namespace cgrt::kernel::intersections {

template<typename T>
using optional = containers::optional<T>;

struct intersection {
    uint64_t id;
    objects::object obj;
    math::scalar time;

    CGRT_KERNEL_FUNC static inline void set(intersection& x, objects::object_id id, math::scalar time) {
        if(time <= 0)
            return;
        if(time >= x.time)
            return;
        x.id = id;
        x.time = time;
    }
};

template<objects::object_type T>
struct intersection_test {
    CGRT_KERNEL_FUNC inline optional<math::scalar> operator()(const math::ray<3>& r);
    // bool intersects(const math::ray<3>& r, const objects::object& obj);
};

template<>
struct intersection_test<objects::PLANE> {
    CGRT_KERNEL_FUNC inline optional<math::scalar> operator()(const math::ray<3>& r) {
        /*
         * ignore rays that're coplanar with the plane
         */
        if(r.direction.z() == math::scalar{})
            return {};

        auto n = -r.origin.z();
        auto d = r.direction.z();
        auto t = n / d;

        if(t <= 0)
            return {};

        return t;
    }
};

template<>
struct intersection_test<objects::DISK> {
    CGRT_KERNEL_FUNC inline optional<math::scalar> operator()(const math::ray<3>& r) {
        if(r.direction.z() == 0)
            return {};

        auto t = -r.origin.z() / r.direction.z();
        if(t <= 0)
            return {};

        auto p = math::position(r, t);
        auto distsq = p.x() * p.x() + p.y() * p.y();
        if(distsq > 1.f || distsq < 0.f)
            return {};

        auto phi_max = 2 * CGRT_PI;
        float phi = atan2f(p.y(), p.x());
        if(phi < 0.f)
            phi += 2.f * CGRT_PI;
        if(phi > phi_max)
            return {};

        return t;
    }
};

template<>
struct intersection_test<objects::SPHERE> {
    CGRT_KERNEL_FUNC inline optional<math::scalar> operator()(const math::ray<3>& r) {
        auto path = r.origin - math::point<3>{0.f};

        auto a = math::dot(r.direction, r.direction);
        auto b = 2.f * dot(r.direction, path);
        auto c = dot(path, path) - 1.f;

        auto b2 = (b * b);
        auto ac4 = (4.f * a * c);
        auto disc = (b2) - (ac4);

        if(disc < 0.f)
            return {};

        auto t1 = (-b - sqrtf(disc)) / (2.f * a);
        auto t2 = (-b + sqrtf(disc)) / (2.f * a);

        if(0 < t1 && t1 <= t2)
            return t1;
        if(0 < t2)
            return t2;

        return {};
    }
};

CGRT_KERNEL_FUNC inline optional<math::scalar> intersects(const math::ray<3>& ray, const objects::object& obj,
                                                          const math::transform::matrix& transform) {
    auto view = math::transform::inverse(transform);
    auto xray = view(ray);

    switch(obj.type) {
    case objects::SPHERE:
        return intersection_test<objects::SPHERE>{}(xray);
    case objects::PLANE:
        return intersection_test<objects::PLANE>{}(xray);
    case objects::DISK:
        return intersection_test<objects::DISK>{}(xray);
    default:
        return {};
    }
}

CGRT_KERNEL_FUNC inline optional<intersection> find_closest(
    const math::ray<3>& r,
    const containers::span<const objects::object>& objs,
    const containers::span<const math::transform::matrix>& transforms) {
    auto closest = optional<intersection>{};
    for(const auto& obj : objs) {
        const auto& transform = transforms[obj.id];
        if(auto&& t = intersects(r, obj, transform).value_or(0.f); t > math::epsilon) {
            if(!closest) {
                closest = intersection{obj.id, obj, t};
                continue;
            }
            if(t > math::epsilon && t < closest->time) {
                closest = intersection{obj.id, obj, t};
                continue;
            }
        }
    }
    return closest;
}

} // namespace cgrt::kernel::intersections

#endif //CGRT_KERNEL_INTERSECTIONS_HPP

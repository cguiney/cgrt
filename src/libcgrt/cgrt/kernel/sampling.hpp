//
// Created by chris on 12/29/2021.
//

#ifndef CGRT_SAMPLING_HPP
#define CGRT_SAMPLING_HPP

#include <cgrt/math/array.hpp>
#include <cgrt/math/point.hpp>
#include <cgrt/math/radiosity.hpp>
#include <cgrt/math/random.hpp>
#include <cgrt/math/constants.h>

namespace cgrt::kernel::sampling {

struct sample {
	math::point<3> pos;
	math::vector<3> direction;
	math::vector<3> normal;
	math::spectrum energy;
	math::scalar pdf;
};

CGRT_KERNEL_FUNC inline math::vector<3> hemisphere_uniform(const math::continuous_random_variable<2>& u) {
    auto z = u[0];
    auto r = sqrtf(fmaxf(0.f, 1.f - (z * z)));
    auto phi = 2.f * math::PI() * u[1];

    return math::vector<3>{r * cosf(phi), r * sinf(phi), z};
}

CGRT_KERNEL_FUNC inline math::vector<3> sphere_uniform(const math::continuous_random_variable<2>& u) {
	auto z = 1.f - 2.f * u[0];
	auto r = sqrtf(fmaxf(0.f, 1.f - (z * z)));
	auto phi = 2.f * math::PI() * u[1];

	return math::vector<3>{r * cosf(phi), r * sinf(phi), z};
}

CGRT_KERNEL_FUNC inline math::point<2> disk_concentric(const math::continuous_random_variable<2>& u) {
	auto x = 2.f * u[0] - 1.f;
	auto y = 2.f * u[1] - 1.f;

	if(x == 0.f && y == 0.f) {
		return {};
	}

	math::scalar theta = 0.f, weight = 0.f;

	if(fabsf(x) > fabsf(y)) {
		weight = x;
		theta = CGRT_PI_4 * (y / x);
	} else {
		weight = y;
		theta = CGRT_PI_2 - CGRT_PI_4 * (x / y);
	}

	return math::point<2>{cosf(theta), sinf(theta)} * weight;
}

[[nodiscard]]
CGRT_KERNEL_FUNC inline math::vector<3> hemisphere_cos(const math::continuous_random_variable<2>& u) {
	auto p = disk_concentric(u);
    auto x2 = p.x() * p.x();
    auto y2 = p.y() * p.y();
    auto n = 1.0f - x2 - y2;
    auto m = fmaxf(0.f, n);
    auto sqrt_m = sqrtf(m);
	return math::vector<3>{
        p.x(),
        p.y(),
        sqrt_m
    };
}

} // namespace cgrt::kernel::sampling

#endif //CGRT_SAMPLING_HPP

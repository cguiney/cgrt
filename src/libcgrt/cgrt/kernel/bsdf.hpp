//
// Created by chris on 12/30/2021.
//

#ifndef CGRT_BSDF_HPP
#define CGRT_BSDF_HPP

#include <cgrt/runtime.hpp>
#include <cgrt/math/constants.h>
#include <cgrt/math/scalar.hpp>
#include <cgrt/math/transform.hpp>
#include <cgrt/math/vector.hpp>
#include <cgrt/objects/materials.hpp>

#include <cgrt/kernel/sampling.hpp>
#include <cgrt/kernel/surfaces.hpp>

namespace cgrt::kernel::bsdf {

enum scattering_type {
    REFLECTION = 0,
    REFRACTION = 1,
};

enum reflection_type {
    DIFFUSE = 0,
    SPECULAR = 1,
};

enum function_type {
    LAMBERTIAN = 0,
};

struct scatter_sample : public sampling::sample {
    scattering_type scattering{};
    reflection_type reflection{};
};

namespace detail {
    CGRT_KERNEL_FUNC constexpr inline math::scalar cos_theta(const math::vector<3>& wi) {
        return wi.z();
    }

    CGRT_KERNEL_FUNC inline math::scalar abs_cos_theta(const math::vector<3>& wi) {
      return math::scalar{fabsf(cos_theta(wi))};
    }
} // namespace detail

struct lambertian {
    math::spectrum energy;

    [[nodiscard]] CGRT_KERNEL_FUNC inline math::spectrum eval(const math::vector<3>& wo,
                                                              const math::vector<3>& wi) const {
        return energy * CGRT_1_PI;
    }

    [[nodiscard]] CGRT_KERNEL_FUNC inline bsdf::scatter_sample sample(
        const math::vector<3>& wo,
        const math::continuous_random_variable<2>& x
    ) const {
        auto wi = sampling::hemisphere_cos(x);
        if(wi.z() < 0.0f) {
            wi = {wi.x(), wi.y(), -wi.z()};
        }

        scatter_sample s;
        s.direction = wi;
        s.energy = eval(wo, wi);
        s.pdf = pdf(wo, wi);
        s.reflection = DIFFUSE;
        s.scattering = REFLECTION;
        return s;
    }

    [[nodiscard]] CGRT_KERNEL_FUNC inline math::scalar pdf(const math::vector<3>& wo, const math::vector<3>& wi) const {
        if(wo.z() * wi.z() > 0.f) {
            return detail::abs_cos_theta(wi) * CGRT_1_PI;
        }
        return 0.0f;
    }
};

struct bxdf {
    function_type func;
    reflection_type reflection;
    scattering_type scattering;
    math::spectrum alpha;
    math::spectrum beta;
};

struct bsdf {
    math::transform::matrix transform;
    math::vector<3> ng; // normal with respect to physical geometry
    bxdf functions[2];

    bsdf() = default;

    CGRT_KERNEL_FUNC bsdf(const surfaces::surface& surface, const objects::material& material) noexcept {
        functions[0] = bxdf{
            LAMBERTIAN,
            DIFFUSE,
            REFLECTION,
            material.primary,
            material.secondary
        };
        functions[1] = bxdf{};

        auto ns = surface.shading.normal;
        auto ss = math::normalize(surface.geometry.du);
        auto ts = math::cross(ns, ss);

        transform = math::matrix<4, 4>{
            {ss[0], ss[1], ss[2], 1.f},
            {ts[0], ts[1], ts[2], 1.f},
            {ns[0], ns[1], ns[2], 1.f},
            { 0.f,   0.f,   0.f,  1.f}
        };
        ng = surface.geometry.normal;
    }
};

CGRT_KERNEL_FUNC inline containers::optional<scatter_sample> sample(
    const bsdf& df,
    const math::vector<3>& wo,
    const math::continuous_random_variable<2>& x
) {
    /*
     * transform wi into a bsdf local point using the bsdf transform matrix
     */
    auto xo = df.transform(wo);
    if(xo.z() == 0.f) {
        return {};
    }


    auto bxdf = df.functions[0];

    scatter_sample s;
    switch (bxdf.func) {
    case LAMBERTIAN:
        switch(bxdf.scattering) {
        case REFLECTION: {
            auto f = lambertian{bxdf.alpha};
            s = f.sample(xo, x);
            break;
        }
        default:
            return {};
        }
        break;
    default:
        return {};
    }

    auto wi = inverse(df.transform)(s.direction);

    // todo: randomly select a matching df to use to get a direction and then re-sample all the matching ones
    // for now there's only one type, and it's reflective...so no need
    auto reflect = math::dot(wi, df.ng) * math::dot(wo, df.ng) > 0.f;
    if(!reflect) {
        return {};
    }
    if(s.pdf == 0.f) {
        return {};
    }

    /*
     * because it's a unit vector, the matrix can be used as the inverse transposed, as well.
     * use it to transform the surface sample's output vector back to world space
     */
    s.direction = inverse(df.transform)(s.direction);
    s.pos = inverse(df.transform)(s.pos);

    return s;
}

CGRT_KERNEL_FUNC inline math::spectrum eval(
    const bsdf& df,
    const math::vector<3>& wo,
    const math::vector<3>& wi
) noexcept {
    auto xo = df.transform(wo);
    auto xi = df.transform(wi);

    if(xo.z() == 0.0f) {
        return {};
    }

    bool reflect = math::dot(wi, df.ng) * math::dot(wo, df.ng) > 0;
    if(!reflect) {
        return {};
    }

    auto bxdf = df.functions[0];

    math::spectrum f;
    switch(bxdf.func) {
    case LAMBERTIAN: {
        auto func = lambertian{bxdf.alpha};
        f = func.eval(xo, xi);
        break;
    }
    default:
        return {};
    }

    return f;
}

CGRT_KERNEL_FUNC inline math::scalar pdf(const bsdf& df, const math::vector<3>& wo, const math::vector<3>& wi) noexcept {
    auto xo = (df.transform)(wo);
    auto xi = (df.transform)(wi);

    if(xo.z() == 0.f) {
        return {};
    }

    math::scalar pdf;
    int matches = 1;

    auto bxdf = df.functions[0];
    switch(bxdf.func) {
    case LAMBERTIAN: {
        auto func = lambertian{bxdf.alpha};
        pdf = func.pdf(xo, xi);
    }
    break;
    }

    return pdf / (float)(matches);
}

} // namespace cgrt::kernel::bsdf

#endif //CGRT_BSDF_HPP

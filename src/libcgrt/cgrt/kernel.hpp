//
// Created by chris on 1/23/21.
//

#ifndef CGRT_KERNEL_HPP
#define CGRT_KERNEL_HPP

#include <cgrt/math/random.hpp>
#include <cgrt/kernel/path_trace.hpp>
#include <cgrt/scene/state.hpp>
#include <cgrt/img/canvas.hpp>

namespace cgrt::kernel {

struct environment {
	const scene::state* state;
    img::canvas* canvas;

	uint32_t time{};
    math::point<2, int> restrict_pixels{};
};

template<typename RuntimeT, typename KernelT>
class executor {
    /*
     * Device side storage for scratch space, owned by host
     */
    unsigned version_{};

    std::unique_ptr<dense_state<RuntimeT>> scene_state_;
    std::unique_ptr<integrator_state<RuntimeT>> integrator_state_;
    std::unique_ptr<path_integrator> integrator_;

    KernelT kernel_;

  public:
    CGRT_HOST explicit executor(KernelT&& f) : kernel_(f) {}

    CGRT_HOST inline void operator()(cgrt::kernel::environment &global)
    {
        size_t pixels = global.canvas->w() * global.canvas->h();

        bool redraw = global.state->version != version_;
        if(redraw) {
            scene_state_ = std::make_unique<dense_state<RuntimeT>>(*global.state);
            integrator_state_ = std::make_unique<integrator_state<RuntimeT>>(pixels);
            integrator_ = std::make_unique<path_integrator>(dense_state_view{*scene_state_}, *integrator_state_);
        }
        version_ = global.state->version;

        kernel_(global, *integrator_);

        auto averages = integrator_state_->averages.to_host();
        for (int y = 0; y < global.canvas->h(); y++) {
            for (int x = 0; x < global.canvas->w(); x++) {
                auto index = y * global.canvas->w() + x;
                kernel::srgb pixel = averages[index];
                global.canvas->set(img::canvas::point_type{x, y}, cgrt::img::color(pixel.r, pixel.g, pixel.b));
            }
        }
    }
};

} // namespace cgrt::kernel

#endif //CGRT_KERNEL_HPP

//
// Created by chris on 1/3/21.
//

#ifndef CGRT_CANVAS_HPP
#define CGRT_CANVAS_HPP

#include <cgrt/math/point.hpp>
#include <cgrt/img/color.hpp>

#include <vector>

namespace cgrt::img
{

    class canvas
    {
    private:
        int width;
        int height;

        //std::vector<color> values;
        std::vector<uint8_t> values;
        struct point
        {
            int x;
            int y;
        };

    public:
        using point_type = point;

        canvas(int width, int height) : width(width), height(height)
        {
            values.resize(width * height * 4);
        }


        inline int w() const noexcept
        { return width; }

        inline int h() const noexcept
        { return height; }

        inline const color operator[](const point_type &p) const
        {
            return color(math::scalar{values[(p.y * width + p.x) * 4  + 0]},
                         math::scalar{values[(p.y * width + p.x) * 4  + 1]},
                         math::scalar{values[(p.y * width + p.x) * 4 +  2]});
        }

        inline void set(uint8_t c)
        {
            for(auto& x : values) x = c;
        }

        inline void set(const point_type &p, const color& c)
        {
            values[(p.y * width + p.x) * 4 + 0] = (uint8_t)(c[0]);
            values[(p.y * width + p.x) * 4 + 1] = (uint8_t)(c[1]);
            values[(p.y * width + p.x) * 4 + 2] = (uint8_t)(c[2]);
            values[(p.y * width + p.x) * 4 + 3] = (uint8_t)(c[3]);
        }

        inline const std::vector<uint8_t>& storage()
        {
            return values;
        }
    };
}

#endif //CGRT_CANVAS_HPP

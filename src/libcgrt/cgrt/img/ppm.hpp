//
// Created by chris on 11/17/2020.
//

#ifndef CGRT_PPM_HPP
#define CGRT_PPM_HPP

#include <cgrt/math/point.hpp>
#include <cgrt/img/canvas.hpp>

#include <ostream>
#include <cmath>

namespace cgrt::img::ppm {
    inline void write(int width, int height, const float *values, std::ostream &out) {
        out << "P3" << std::endl;
        out << width << " " << height << std::endl;
        out << 255 << std::endl;
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                auto r = int(ceil(values[0] * 255)),
                     g = int(ceil(values[1] * 255)),
                     b = int(ceil(values[2] * 255));
                out << r << " " << g << " " << b;
                values += 4;

                if (x < width) {
                    out << " ";
                }
            }
            out << "\n";
        }
        out.flush();
    }

    inline void write(int width, int height, const canvas& c, std::ostream &out) {
        out << "P3" << std::endl;
        out << width << " " << height << std::endl;
        out << 255 << std::endl;
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                auto p = canvas::point_type{x, y};
                auto& color = c[p];
                auto r = int(ceil(color[0] * 255.0f)),
                     g = int(ceil(color[1] * 255.0f)),
                     b = int(ceil(color[2] * 255.0f));
                out << r << " " << g << " " << b;

                if (x < width) {
                    out << " ";
                }
            }
            out << "\n";
        }
        out.flush();
    }
}

#endif //CGRT_PPM_HPP

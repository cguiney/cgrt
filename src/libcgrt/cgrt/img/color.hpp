//
// Created by chris on 1/3/21.
//

#ifndef CGRT_COLOR_HPP
#define CGRT_COLOR_HPP

#include <cgrt/math/array.hpp>
#include <cgrt/math/scalar.hpp>
#include <cgrt/math/radiosity.hpp>

namespace cgrt::img
{
    class color : public math::array<color, 4>
    {
        using base_type = math::array<color, 4>;

    public:
        color(math::scalar fill = 0.f) : base_type(fill, fill, fill, 255.f)
        {
        }

        color(
            math::scalar x,
            math::scalar y,
            math::scalar z
        ) : base_type(x, y, z, 255.f)
        {
        }

        explicit color(const math::spectrum &spec) : color(spec[0], spec[1], spec[2])
        {
        }
    };

    inline std::ostream &operator<<(std::ostream &os, const color &v)
    {
        os << "{ ";
        os << "r:" << v[0] << " ";
        os << "g:" << v[1] << " ";
        os << "b:" << v[2] << " ";
        os << "}";

        return os;
    }
}

#endif //CGRT_COLOR_HPP

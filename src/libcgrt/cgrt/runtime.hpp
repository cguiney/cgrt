//
// Created by chris on 10/2/2022.
//

#ifndef CGRT_RUNTIME_HPP
#define CGRT_RUNTIME_HPP

#include <type_traits>

#	ifdef __CUDACC__
#		define CGRT_CUDA 1
#	else
#		define CGRT_CUDA 0
#	endif
#	if CGRT_CUDA
#		define CGRT_KERNEL_FUNC __host__ __device__
#		define CGRT_KERNEL __global__
#		define CGRT_HOST __host__
#		define CGRT_DEVICE __device__
#	else
#		define CGRT_KERNEL_FUNC
#		define CGRT_KERNEL
#		define CGRT_HOST
#		define CGRT_DEVICE
#	endif

namespace cgrt::runtime {
    template<typename, typename>
    struct system;

    template<typename RuntimeT, typename T>
    using device_vector_t = typename system<RuntimeT, T>::device_vector_type;

    template<typename RuntimeT, typename T>
    using host_vector_t = typename system<RuntimeT, T>::host_vector_type;
};

#endif //CGRT_RUNTIME_HPP

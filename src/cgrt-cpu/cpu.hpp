//
// Created by chris on 11/13/2022.
//

#ifndef CGRT_CPU_HPP
#define CGRT_CPU_HPP

#include <vector>

namespace cgrt::runtime {

struct cpu{};

template<typename T>
struct system<cpu, T> {
    using device_vector_type = std::vector<T>;
    using host_vector_type = std::vector<T>;
};

}

#endif //CGRT_CPU_HPP

//
// Created by chris on 1/10/21.
//

#ifndef CGRT_CPU_KERNEL_HPP
#define CGRT_CPU_KERNEL_HPP

#include <memory>

#include <cgrt/runtime.hpp>
#include <cgrt/math/point.hpp>
#include <cgrt/state.hpp>
#include <cgrt/kernel/path_trace.hpp>
#include <cgrt/kernel.hpp>
#include <cgrt/img/canvas.hpp>

#include "cpu.hpp"

namespace cgrt::cpu
{
struct cpu_kernel
{
    inline void operator()(cgrt::kernel::environment &env, cgrt::kernel::path_integrator integrator)
    {
        const auto& restrict = env.restrict_pixels;
        for(int y = 0; y < env.canvas->h(); y++) {
            if(restrict.y() > 0 && restrict.y() != y) {
                continue;
            }
            for(int x = 0; x < env.canvas->w(); x++) {
                if(restrict.x() > 0 && restrict.x() != x) {
                    continue;
                }
                kernel::frame local{};
                local.shadowing = true;
                local.time = env.time;
                local.width = env.state->camera.width;
                local.height = env.state->camera.height;
                local.depth = 1;
                local.spp = 1;
                local.id = y * local.width + x;
                local.x = x;
                local.y = y;

                integrator(local);
            }
        }
    }
};
}

#endif //CGRT_CPU_KERNEL_HPP

#include <cgrt/shell.hpp>

#include "kernel.hpp"

using namespace cgrt;

static int canvas_size = 400;

int main(int argc, char **argv)
{
    auto state = spheres_example(canvas_size, canvas_size);
    auto kernel = kernel::executor<runtime::cpu, cpu::cpu_kernel>(cpu::cpu_kernel{});
    auto sh = ui::shell<decltype(kernel)>(state, std::move(kernel), canvas_size, canvas_size);
    sh.run();

    return 0;
}

//
// Created by chris on 2/6/21.
//

#include "kernel.cuh"

#include <cgrt/runtime.hpp>
#include <cgrt/math/radiosity.hpp>
#include <cgrt/kernel.hpp>
#include <cgrt/kernel/path_trace.hpp>

#include <thrust/fill.h>

#include <utility>


#define gpu(x) gpu_assert((x), __FILE__, __LINE__);

using namespace cgrt;

namespace cgrt::detail {


CGRT_KERNEL_FUNC inline math::point<2, unsigned> coordinates() {
    return math::point<2, unsigned> {
        threadIdx.x + blockIdx.x * blockDim.x,
        threadIdx.y + blockIdx.y * blockDim.y
    };
}

CGRT_KERNEL_FUNC inline unsigned index() {
    auto pos = coordinates();
    return pos.y() * (gridDim.x * blockDim.x) + pos.x();
}

}
// namespace cgrt::detail


void gpu_assert(cudaError_t code, const char *file, int line, bool do_abort = true)
{
    if (code != cudaSuccess) {
        fprintf(stderr, "gpu_assert: %s %s %d\n", cudaGetErrorString(code), file, line);
        if (do_abort) abort();
    }
}

CGRT_KERNEL void run_cuda_kernel(kernel::path_integrator integrator)
{
    auto coordinates = detail::coordinates();
    auto x = coordinates.x();
    auto y = coordinates.y();
    auto h = integrator.scene().camera().height;
    auto w = integrator.scene().camera().width;
    if(x >= w || y >= h) {
        return;
    }
    auto index = y * w + x;
    auto& frame = integrator.integration().frames[index];
    frame.height = h;
    frame.width = w;
    frame.x = coordinates.x();
    frame.y = coordinates.y();
    frame.id = index;
    auto avg = integrator.integration().averages;
    auto sum = integrator.integration().sums;

    frame.spp = 1;
    integrator(frame);
}

CGRT_HOST cuda_kernel::cuda_kernel(int width, int height)
{
    size_t free = 0, total = 0;
    size_t stackLimit = 0;

    cudaMemGetInfo(&free, &total);
    gpu(cudaDeviceSetLimit(cudaLimitStackSize, 8 * 1024));
    gpu(cudaDeviceGetLimit(&stackLimit, cudaLimitStackSize));
    std::cerr << "free: " << free << " total: " << total << std::endl;
    std::cerr << "stack size limit: " << stackLimit << std::endl;
}

CGRT_HOST void cuda_kernel::operator()(kernel::environment& env, kernel::path_integrator integrator)
{
    uint32_t nx = env.canvas->w();
    uint32_t ny = env.canvas->h();
    uint32_t tx = 8; /* n x m thread counts */
    uint32_t ty = 8;
    dim3 blocks(nx/tx+1, ny/ty+1);
    dim3 threads(tx, ty);

    run_cuda_kernel<<<blocks, threads>>>(integrator);
    gpu(cudaDeviceSynchronize());
}

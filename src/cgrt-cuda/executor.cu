//
// Created by chris on 12/30/2022.
//

#include "runtime.cuh"
#include "kernel.cuh"
#include "executor.hpp"

#include <utility>

using namespace cgrt;

class cuda_executor_impl {
    std::unique_ptr<kernel::executor<runtime::cuda, cuda_kernel>> exec_;
  public:
    CGRT_HOST cuda_executor_impl(int h, int w);
    CGRT_HOST void operator()(kernel::environment& env);
};

CGRT_HOST cuda_executor_impl::cuda_executor_impl(int h, int w) {
    exec_ = std::make_unique<kernel::executor<runtime::cuda, cuda_kernel>>(cuda_kernel{h, w});
}

void cuda_executor_impl::operator()(kernel::environment& env) {
    (*exec_)(env);
}

void cuda_executor::operator()(kernel::environment& env) {
    (*ptr_)(env);
}

cuda_executor::cuda_executor(int w, int h) {
    ptr_ = new cuda_executor_impl(h, w);
}

cuda_executor::cuda_executor(cuda_executor&& other) noexcept {
    std::swap(ptr_, other.ptr_);
}

cuda_executor& cuda_executor::operator=(cuda_executor&& other) noexcept {
    std::swap(ptr_, other.ptr_);
    return *this;
}

cuda_executor::~cuda_executor() {
    delete ptr_;
}

//
// Created by chris on 12/30/2022.
//

#ifndef CGRT_RUNTIME_CUH
#define CGRT_RUNTIME_CUH

#include <thrust/device_new.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>

#include <cgrt/runtime.hpp>
#include <cgrt/containers.hpp>

namespace cgrt::detail {
template<typename T>
class device_vector : public thrust::device_vector<T> {
  public:
    static_assert(CGRT_CUDA);
    using parent_type = typename thrust::device_vector<T>;
    using value_type = typename parent_type::value_type;
    using size_type = typename parent_type::size_type;

    template<typename... Args>
    CGRT_HOST inline device_vector(Args&&... args) noexcept: parent_type(std::forward<Args>(args)...) {
    }

    CGRT_HOST inline device_vector(device_vector<T>&& other) noexcept: parent_type(std::move(other)) {
    }

    CGRT_HOST inline operator containers::span<T>() noexcept {
        return containers::span<T>(parent_type::data().get(), parent_type::size());
    }

    CGRT_HOST inline operator containers::span<const T>() const noexcept {
        return containers::span<const T>(parent_type::data().get(), parent_type::size());
    }
};
}

namespace cgrt::runtime {

struct cuda {};

template<typename T>
struct system<cuda, T> {
    using host_vector_type = typename thrust::host_vector<T>;
    using device_vector_type = typename cgrt::detail::device_vector<T>;
};

} // namespace cgrt::runtime

#endif //CGRT_RUNTIME_CUH

//
// Created by chris on 11/11/20.
//

#include "executor.hpp"

#include <cgrt/shell.hpp>
#include <fstream>
#include <iostream>

using std::cin;
using std::cout;
using std::cerr;

using namespace cgrt;

std::string file_get_contents(const std::string &filename)
{
    auto f = std::fstream(filename);
    if (!f)
        throw std::runtime_error("failed opening file: " + filename);

    auto ss = std::stringstream();
    ss << f.rdbuf();

    return ss.str();
}

int main(int argc, char **argv)
{
    auto w = 400;
    auto h = 400;
    auto st = spheres_example(w, h);
    auto f = cuda_executor{h, w};
    std::cerr << "creating shell" << std::endl;
    auto sh = ui::shell<cuda_executor>(st, std::move(f), w, h);
    sh.run();



    return 0;
}
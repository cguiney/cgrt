//
// Created by chris on 2/6/21.
//

#ifndef CGRT_KERNEL_CUH
#define CGRT_KERNEL_CUH
#include <cuda.h>
#include <memory>

#include <cgrt/runtime.hpp>
#include <cgrt/containers.hpp>
#include <cgrt/kernel.hpp>
#include <cgrt/kernel/path_trace.hpp>
#include <cgrt/state.hpp>

namespace cgrt {

class cuda_kernel {
  public:
    CGRT_HOST cuda_kernel(int width, int height);
    CGRT_HOST void operator()(kernel::environment& env, kernel::path_integrator integrator);
};

} // namespace cgrt

#endif //CGRT_KERNEL_CUH

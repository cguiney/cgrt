//
// Created by chris on 11/21/2021.
//
#pragma once
#ifndef CGRT_MEMORY_HPP
#define CGRT_MEMORY_HPP

#include <cgrt/containers.hpp>

#include <cooperative_groups.h>
#include <cuda.h>
#include <cuda_runtime.h>

#include <functional>
#include <iostream>
#include <memory>
#include <sstream>
#include <stdexcept>
#include <utility>

namespace cgrt::detail::runtime::cuda {

CGRT_HOST void throw_if_error(cudaError err, const std::string& message) {
	if(err == cudaSuccess)
		return;

	auto ss = std::stringstream{};
	ss << message << ": cuda error code #" << err;
	throw std::runtime_error(ss.str());
}

template<typename T>
CGRT_HOST inline void device_deleter(T* ptr) {
	if(auto err = cudaFree(ptr); err != cudaSuccess)
		throw std::runtime_error("cudaFree failed");
}

template<typename T>
using device_ptr = std::unique_ptr<T, std::function<decltype(device_deleter<T>)>>;

template<typename T>
CGRT_HOST inline device_ptr<T> make_device_array(size_t count = 1) {
	if(count <= 0)
		throw std::runtime_error("device allocation size must be >= 1");

	T* ptr = nullptr;
	throw_if_error(cudaMalloc(&ptr, (sizeof(T) * count)), "failed allocating values");
	throw_if_error(cudaMemset(ptr, 0, (sizeof(T) * count)), "failed zeroing memory");

	return device_ptr<T>(ptr, device_deleter<T>);
}

template<typename T>
CGRT_HOST inline device_ptr<T> make_device(const T& v) {
	T* ptr = nullptr;
	throw_if_error(cudaMalloc(&ptr, sizeof(T)), "failed allocating value");
	throw_if_error(cudaMemcpy(ptr, &v, sizeof(T), cudaMemcpyHostToDevice), "failed copying value to device");
	return device_ptr<T>{ptr};
}

template<typename T>
class device_view {
	T* values_;
	size_t* len_;

	CGRT_KERNEL_FUNC constexpr inline device_view(T* values, size_t* len): values_(values), len_(len) {
	}

	CGRT_KERNEL_FUNC constexpr inline T* cbegin() const {
		return values_;
	}

	CGRT_KERNEL_FUNC constexpr inline T* cend() const {
		return values_ + *len_;
	}

	CGRT_KERNEL_FUNC constexpr inline const T* data() const {
		return values_;
	}
};

template<typename T>
class device_queue {
	T* values_;
	size_t* len_;
	size_t* cap_;

  public:
	CGRT_KERNEL_FUNC constexpr inline device_queue(T* values, size_t* len, size_t* cap)
	    : values_(values), len_(len), cap_(cap) {
	}

	CGRT_DEVICE inline size_t capacity() {
		return *cap_;
	}

	CGRT_DEVICE inline void push_back(const T& value) {
		auto cg = cooperative_groups::coalesced_threads();

		size_t index = 0;
		if(cg.thread_rank() == 0) {
			index = atomicAdd(len_, cg.size());
		}

		index = cg.thread_rank() + cg.shfl(index, 0);
		values_[index] = value;
	}
};

template<typename T>
class device_vector {
	cuda::device_ptr<T> dvalues_;
	cuda::device_ptr<size_t> dlen_;
	cuda::device_ptr<size_t> dcap_;

	size_t hcap_{};

  public:
	constexpr inline explicit device_vector() = default;

	CGRT_HOST constexpr inline explicit device_vector(size_t max, size_t len = 0, size_t cap = 0) {
		if(max == 0)
			return;

		dvalues_ = cuda::make_device_array<T>(max);
		dlen_ = cuda::make_device<size_t>(len);
		dcap_ = cuda::make_device<size_t>(cap);
		hcap_ = max;
	}

	CGRT_HOST inline explicit device_vector(const std::vector<T>& host): device_vector(host.size(), host.size(), host.size()) {
		if(host.empty())
			return;

		throw_if_error(cudaMemcpy(dvalues_.get(), host.data(), sizeof(T) * host.size(), cudaMemcpyHostToDevice),
		               "failed copying vector to device");
	}

	CGRT_HOST inline void copy(std::vector<T>& values) const {
		size_t len = size();
		values.resize(len);


		throw_if_error(cudaMemcpy(values.data(), dvalues_.get(), sizeof(T) * len, cudaMemcpyDeviceToHost),
		               "failed reading queue data into host vector");
	}

	[[nodiscard]] CGRT_HOST inline size_t size() const {
		size_t hlen = 0;
		throw_if_error(cudaMemcpy(&hlen, dlen_.get(), sizeof(*dlen_), cudaMemcpyDeviceToHost),
		               "failed reading queue length");
		return hlen;
	}

	[[nodiscard]] CGRT_HOST inline device_queue<T> queue() const {
		return device_queue{dvalues_.get(), 0, dcap_.get()};
	}

	[[nodiscard]] CGRT_HOST inline device_view<T> view() const {
		return device_view{dvalues_.get(), dlen_.get()};
	}

	[[nodiscard]] CGRT_HOST inline containers::span<T> span() const {
		return containers::span{dvalues_.get(), size()};
	}
};

} // namespace cgrt::cuda

#endif //CGRT_MEMORY_HPP

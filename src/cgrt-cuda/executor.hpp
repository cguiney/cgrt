//
// Created by chris on 12/30/2022.
//

#ifndef CGRT_EXECUTOR_HPP
#define CGRT_EXECUTOR_HPP

#include <cgrt/kernel.hpp>

class cuda_executor_impl;

class cuda_executor {
    cuda_executor_impl* ptr_{};
  public:
    explicit cuda_executor(int w, int h);
    cuda_executor(cuda_executor&& other) noexcept ;
    cuda_executor& operator=(cuda_executor&& other) noexcept;
    cuda_executor(cuda_executor&) = delete;
    cuda_executor& operator=(cuda_executor&) = delete;
    ~cuda_executor();

    void operator()(cgrt::kernel::environment& env);
};


#endif //CGRT_EXECUTOR_HPP

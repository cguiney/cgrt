import os
import numpy as np
import matplotlib.pyplot as plt

os.add_dll_directory(os.path.join(os.environ['CUDA_PATH'], 'bin'))

from ._pycgrt import scalar, vector, point, ray, generate_camera_paths


def vis_camera_paths(w, h):
    fig = plt.figure()
    rays = generate_camera_paths(w, h)
    x = np.array([r.origin.x().to_float() for r in rays])
    y = np.array([r.origin.y().to_float() for r in rays])
    z = np.array([r.origin.z().to_float() for r in rays])
    u = np.array([r.direction.x().to_float() for r in rays])
    v = np.array([r.direction.y().to_float() for r in rays])
    w = np.array([r.direction.z().to_float() for r in rays])
    ax = fig.add_subplot(projection='3d')
    ax.quiver(x, y, z, u, v, w)
    return fig


if __name__ == '__main__':
    vis_camera_paths(1920, 1080)
    fig.show()

//
// Created by chris on 11/20/2021.
//

#include "kernels.cuh"

#include <cgrt/cuda/memory.hpp>
#include <cgrt/kernel/wavefront.cuh>
#include <cgrt/math/random.hpp>
#include <cgrt/scene/builder.hpp>

using namespace cgrt;

std::vector<cgrt::math::ray<3>> generate_camera_rays(int w, int h) {
	int tx = 16; /* n x m thread counts */
	int ty = 16;
	dim3 blocks(w / tx + 1, h / ty + 1);
	dim3 threads(tx, ty);

	auto size = w * h;

	auto rnd = cuda::make_device_array<cgrt::math::rng>(size);
	auto q = cuda::device_vector<cgrt::kernel::segment>(size);

	auto b = cgrt::scene::builder{};
	b.set_film(blocks.x * threads.x, blocks.y * threads.y);
	b.camera().from({0.f, 2.f, -7.f}).to({0.0f, 0.0f, 1.0f}).up({0.0f, 0.0f, 1.0f}).field_of_view(CGRT_PI_2).finish();

	auto st = b.build();
	auto camera = cgrt::cuda::make_device<cgrt::objects::camera>(st.camera);


	kernel::wavefront::init_random_state<<<blocks, threads>>>(rnd.get(), (int) size);
	kernel::wavefront::generate_camera_paths<<<blocks, threads>>>(q.queue(), rnd.get(), camera.get(), size);

	auto segments = std::vector<cgrt::kernel::segment>{};
	q.copy(segments);

	auto rv = std::vector<cgrt::math::ray<3>>{};
	for(const auto& s : segments) {
		rv.push_back(s.path);
	}
	return rv;
}

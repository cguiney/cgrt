//
// Created by chris on 11/20/2021.
//

#ifndef CGRT_KERNELS_CUH
#define CGRT_KERNELS_CUH

#include <cgrt/kernel/pipeline.hpp>
#include <vector>

std::vector<cgrt::math::ray<3>> generate_camera_rays(int w, int h);

#endif //CGRT_KERNELS_CUH

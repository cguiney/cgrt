//
// Created by chris on 11/20/2021.
//
#include "kernels.cuh"

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

namespace py = pybind11;

using namespace cgrt;

int add(int i, int j) {
	return i + j;
}

PYBIND11_MODULE(_pycgrt, m) {
	m.def("add", &add, R"pbdoc(
        Add two numbers
    )pbdoc");
	// clang-format off
	py::class_<math::scalar>(m, "scalar").def(py::init<>())
		.def("to_float", [](const math::scalar& s) -> float { return s; })
		.def("__repr__", &math::scalar::repr);

	// py::implicitly_convertible<math::scalar, float>();

	py::class_<math::vector<3>>(m, "vector")
		.def(py::init<>())
		.def("x", &math::vector<3>::x)
		.def("y", &math::vector<3>::y)
		.def("z", &math::vector<3>::z)
		.def("__repr__", &math::vector<3>::repr);


	py::class_<math::point<3>>(m, "point")
		.def(py::init<>())
		.def("x", &math::point<3>::x)
		.def("y", &math::point<3>::y)
		.def("z", &math::point<3>::z)
		.def("__repr__", &math::point<3>::repr);

	py::class_<math::ray<3>>(m, "ray")
	    .def(py::init<>())
	    .def_readonly("origin", &math::ray<3>::origin)
	    .def_readonly("direction", &math::ray<3>::direction)
		.def("__repr__", &math::ray<3>::repr);

	py::class_<kernel::segment>(m, "segment")
	    .def(py::init<>())
	    .def_readonly("path", &kernel::segment::path);
	// clang-format on

	m.def("generate_camera_paths", &generate_camera_rays);
}